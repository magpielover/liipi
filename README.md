# Node-GraphQL proxy for Liipi - Park and Ride API

HSL open source project to collect and share parking capacity information.

The service can be found at <https://p.hsl.fi>

API documentation can be found at <https://p.hsl.fi/docs>

## Development

To build the project, you will need [Docker](https://www.docker.com/community-edition).

Building the project

    docker-compose build

Starting the application

    docker-compose up -d

The web application will run on <http://hsl-client.docker.localhost:5000>

GraphQL playground will run on <http://hsl-api.docker.localhost:5000/graphql>

                                                         | Docker
    https://p.hsl.fi/*                      -> |         | ->  |_Production UI
    https://p.hsl.fi/graphql/*              -> |         | ->  |_Production Backend
                                               | traefik |
    https://test.p.hsl.fi/*                 -> |         | ->  |_Test UI
    https://test.p.hsl.fi/graphql/*         -> |         | ->  |_Test Backend

a single traefik service, that:

1. Handles domain mapping and url routing
2. Provides SSL
3. Can load balances production instances

If you want to run the client and server apps separately on your local machine, environment variables should be set according to the example.

For client app, an example of environment variables is

    REACT_APP_DOCKER_API_URL=http://hsl-api.docker.localhost:5000/graphql
    REACT_APP_LOCAL_API_URL=http://localhost:4000/graphql
    REACT_APP_ENV=local
    PORT=3000

For the server api, an example of environment variables is

    API_BASE_URL=https://test.p.hsl.fi/api/v1/
    INTERNAL_API_BASE_URL=https://test.p.hsl.fi/internal/
    ENGINE_API_KEY=myenginekey
    ENV=local
