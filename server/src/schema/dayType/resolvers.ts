const Query: any = {
  dayTypes: async (_, {}, { dataSources }) => {
    return dataSources.dayTypeAPI.getAllDayTypes();
  }
};

export default { Query };
