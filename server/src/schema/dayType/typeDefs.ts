import { gql } from "apollo-server";

const typeDefs = gql`
  extend type Query {
    dayTypes: [String]
  }
`;

export default typeDefs;
