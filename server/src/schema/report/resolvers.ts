import { encode } from "base64-arraybuffer";
import isArrayBuffer from "is-array-buffer";

const Mutation: any = {
  facilityUsageReport: async (_, payload, { dataSources, token }) => {
    const response = await dataSources.reportAPI.downloadFacilityUsageReport(
      payload,
      token
    );
    return isArrayBuffer(response) ? encode(response) : response;
  },
  hubsAndFacilitiesReport: async (_, payload, { dataSources, token }) => {
    const response = await dataSources.reportAPI.downloadHubsAndFacilitiesReport(
      payload,
      token
    );
    return isArrayBuffer(response) ? encode(response) : response;
  },
  maxUtilizationReport: async (_, payload, { dataSources, token }) => {
    const response = await dataSources.reportAPI.downloadMaxUtilizationReport(
      payload,
      token
    );
    return isArrayBuffer(response) ? encode(response) : response;
  },
  requestLogReport: async (_, payload, { dataSources, token }) => {
    const response = await dataSources.reportAPI.downloadRequestLogReport(
      payload,
      token
    );
    return isArrayBuffer(response) ? encode(response) : response;
  }
};

export default { Mutation };
