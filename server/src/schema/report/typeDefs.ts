import { gql } from "apollo-server";

const typeDefs = gql`
  extend type Mutation {
    facilityUsageReport(
      capacityTypes: [String!]
      endDate: [Int!]
      facilities: [Int!]
      hubs: [Int!]
      interval: Int
      operators: [Int!]
      regions: [Int!]
      startDate: [Int!]
      usages: [Int!]
    ): String

    maxUtilizationReport(
      capacityTypes: [String!]
      endDate: [Int!]
      facilities: [Int!]
      hubs: [Int!]
      operators: [Int!]
      regions: [Int!]
      startDate: [Int!]
      usages: [Int!]
    ): String

    hubsAndFacilitiesReport: String

    requestLogReport(
      endDate: [Int!]
      requestLogInterval: String!
      startDate: [Int!]
    ): String
  }
`;

export default typeDefs;
