import capacityType from "./capacityType/typeDefs";
import contact from "./contact/typeDefs";
import dayType from "./dayType/typeDefs";
import facility from "./facility/typeDefs";
import facilityStatus from "./facilityStatus/typeDefs";
import global from "./global/typeDefs";
import hub from "./hub/typeDefs";
import login from "./login/typeDefs";
import operator from "./operator/typeDefs";
import paymentMethod from "./paymentMethod/typeDefs";
import pricingMethod from "./pricingMethod/typeDefs";
import region from "./region/typeDefs";
import report from "./report/typeDefs";
import service from "./service/typeDefs";
import usage from "./usage/typeDefs";
import user from "./user/typeDefs";
import utilization from "./utilization/typeDefs";

const typeDefs = [
  ...global,
  facility,
  utilization,
  hub,
  login,
  operator,
  contact,
  capacityType,
  dayType,
  facilityStatus,
  pricingMethod,
  region,
  report,
  service,
  usage,
  paymentMethod,
  user
];

export default typeDefs;
