import { AuthenticationError } from "apollo-server";

const Mutation: any = {
  login: async (_, { password, username }, { dataSources }) => {
    return dataSources.loginAPI.postLogin({
      password,
      username
    });
  }
};

export default { Mutation };
