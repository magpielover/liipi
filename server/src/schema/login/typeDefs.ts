import { gql } from "apollo-server";

const typeDefs = gql`
  extend type Mutation {
    login(password: String, username: String): Login
  }

  type Login {
    operatorId: String
    passwordExpireInDays: Int!
    permissions: [String]!
    role: String!
    token: String!
    userId: Int!
    username: String!
  }
`;

export default typeDefs;
