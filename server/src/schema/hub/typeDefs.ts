import { gql } from "apollo-server";

const typeDefs = gql`
  extend type Query {
    hubs(
      after: String
      pageSize: Int
      sortBy: String
      sortDir: String
      statuses: [String]
      ids: [Int]
      facilityIds: [Int]
      geometry: String
      maxDistance: Float
    ): HubResponse!
    hubDetails(id: ID!, geojson: Boolean): HubDetails!
    hubsSmmary(summary: Boolean): HubsSummary
    hubUtilization(id: ID!): [Utilization]
    hubPrediction(id: ID!, after: Int, at: String): [Prediction]
    hubsGeoJSON: HubsGeoJSONResponse
    hubGeoJSON(id: ID!): HubGeoJSON
  }
  type Hub {
    id: Int!
    name: LocalizedObject
    location: Location
    facilityIds: [Int!]!
    address: Address
    hasMore: Boolean
  }

  type HubResponse {
    cursor: String
    totalCount: Int
    hasMore: Boolean!
    hubs: [Hub!]!
  }

  type HubDetails {
    id: Int
    name: LocalizedObject
    location: HubLocation
    facilityIds: [Int!]!
    address: Address
  }

  type HubLocation {
    crs: Crs
    bbox: [Float]
    type: String
    coordinates: [Float]
  }
  type HubsSummary {
    hubCount: Int
    capacities: Capacity
  }

  type HubsGeoJSONResponse {
    hasMore: Boolean
    features: [HubGeoJSON]
    type: String
  }
  type HubGeoJSON {
    id: Int
    geometry: Geometry
    properties: Properties
    type: String
  }
`;

export default typeDefs;
