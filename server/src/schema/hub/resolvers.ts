import { paginateResults } from "../..//utils/paginateResults";

const queryBuilder = (
  after: string,
  pageSize: number | string,
  sortBy: string,
  sortDir: string,
  statuses: string[],
  ids: number[] | string[],
  facilityIds: number[] | string[],
  geometry: string,
  maxDistance: string,
  at: string
) => {
  let query = "";

  if (after) {
    query =
      query === ""
        ? query.concat("?after=", after)
        : query.concat("&after=", after);
  }
  if (pageSize) {
    query =
      query === ""
        ? query.concat("?pageSize=", pageSize.toString())
        : query.concat("&pageSize=", pageSize.toString());
  }
  if (sortBy) {
    query =
      query === ""
        ? query.concat("?sortBy=", sortBy)
        : query.concat("&sortBy=", sortBy);
  }
  if (sortDir) {
    query =
      query === ""
        ? query.concat("?sortDir=", sortDir)
        : query.concat("&sortDir=", sortDir);
  }
  if (Array.isArray(statuses) && statuses.length) {
    statuses.forEach(element => {
      query =
        query === ""
          ? query.concat("?statuses=", element)
          : query.concat("&statuses=", element);
    });
  }
  if (Array.isArray(ids) && ids.length) {
    ids.forEach(element => {
      query =
        query === ""
          ? query.concat("?ids=", element)
          : query.concat("&ids=", element);
    });
  }
  if (Array.isArray(facilityIds) && facilityIds.length) {
    facilityIds.forEach(element => {
      query =
        query === ""
          ? query.concat("?facilityIds=", element)
          : query.concat("&facilityIds=", element);
    });
  }
  if (geometry) {
    query =
      query === ""
        ? query.concat("?geometry=", geometry)
        : query.concat("&geometry=", geometry);
  }
  if (maxDistance) {
    query =
      query === ""
        ? query.concat("?maxDistance=", maxDistance)
        : query.concat("&maxDistance=", maxDistance);
  }
  if (at) {
    query = query === "" ? query.concat("?at=", at) : query.concat("&at=", at);
  }
  return query;
};
const Query: any = {
  hubs: async (
    _,
    {
      after,
      pageSize,
      sortBy,
      sortDir,
      statuses,
      ids,
      facilityIds,
      geometry,
      maxDistance
    },
    { dataSources }
  ) => {
    const query = queryBuilder(
      after,
      pageSize,
      sortBy,
      sortDir,
      statuses,
      ids,
      facilityIds,
      geometry,
      maxDistance,
      null
    );

    const result = await dataSources.hubAPI.getAllHubs(query);

    // const hubs = paginateResults({
    //   after,
    //   pageSize,
    //   results: allHubs
    // });
    // ** TODO : sorting based on the query strings
    // Sort the results based on sortBy arguments

    return {
      totalCount: result.results.length,
      hubs: result.results,
      //   cursor: hubs.length
      //     ? hubs[hubs.length - 1].cursor
      //     : null,
      // if the cursor of the end of the paginated results is the same as the
      // last item in _all_ results, then there are no more results after this
      hasMore: result.hasMore
    };
  },
  hubDetails: async (_, { id }, { dataSources }) => {
    const hubDetails = await dataSources.hubAPI.getHubById(id);
    return hubDetails;
  },
  hubUtilization: async (_, { id }, { dataSources }) => {
    const hubUtilization = await dataSources.hubAPI.getHubUtilization(id);
    return hubUtilization;
  },
  hubPrediction: async (_, { id, at, after }, { dataSources }) => {
    const query = queryBuilder(
      after,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      at
    );
    return dataSources.hubAPI.getHubPrediction(id, query);
  },
  hubsGeoJSON: async (_, {}, { dataSources }) => {
    const allHubs = await dataSources.hubAPI.getAllHubs("", true);
    return JSON.parse(allHubs);
  },
  hubGeoJSON: async (_, { id }, { dataSources }) => {
    const hubDetailsGeoJSON = await dataSources.hubAPI.getHubGeoJSON(id);
    return JSON.parse(hubDetailsGeoJSON);
  },
  hubsSmmary: async (_, { summary }, { dataSources }) => {
    const hubsSmmary = await dataSources.hubAPI.getHubsSummary(summary);
    return hubsSmmary;
  }
};

export default { Query };
