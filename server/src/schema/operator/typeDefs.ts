import { gql } from "apollo-server";

const typeDefs = gql`
  extend type Query {
    operators: OperatorResponse!
    operatorDetails(id: ID!): Operator!
  }
  type OperatorResponse {
    hasMore: Boolean
    results: [Operator!]!
  }
  type Operator {
    id: ID!
    name: LocalizedObject!
  }
`;

export default typeDefs;
