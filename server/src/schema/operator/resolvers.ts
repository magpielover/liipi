const Query: any = {
  operators: async (_, {}, { dataSources }) => {
    return dataSources.operatorAPI.getAllOperators();
  },
  operatorDetails: async (_, { id }, { dataSources }) => {
    return dataSources.operatorAPI.getOperatorById(id);
  }
};

export default { Query };
