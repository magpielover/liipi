const Query: any = {
  utilizations: async (_, {}, { dataSources }) => {
    console.log("Girdi mi");
    const utilizations = await dataSources.utilizationAPI.getUtilizations();
    return utilizations;
  }
};

export default { Query };
