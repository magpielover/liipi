import { gql } from "apollo-server";

const typeDefs = gql`
  extend type Query {
    utilizations: [Utilization]
  }
`;

export default typeDefs;
