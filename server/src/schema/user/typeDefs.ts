import { gql } from "apollo-server";

const typeDefs = gql`
  extend type Query {
    users: [User]
  }
  type User {
    id: Int
    username: String
    role: String
    operatorId: Int
  }
  input UserInput {
    role: String
    username: String
    password: String
    operatorId: Int
  }
  extend type Mutation {
    createUser(user: UserInput): User
    changePassword(id: Int, newPassword: String): String
    updateToken(id: Int): String
    deleteUser(id: Int!): String
  }
`;

export default typeDefs;
