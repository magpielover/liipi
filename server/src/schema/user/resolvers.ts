const Query: any = {
  users: async (_, {}, { dataSources }) => {
    const response = await dataSources.userAPI.getAllUsers();
    return response ? response.results : [];
  }
};
const Mutation: any = {
  createUser: async (_, { user }, { dataSources }) => {
    return dataSources.userAPI.createUser(user);
  },
  changePassword: async (_, { id, newPassword }, { dataSources }) => {
    return dataSources.userAPI.changePassword(id, newPassword);
  },
  updateToken: async (_, { id }, { dataSources }) => {
    const res = await dataSources.userAPI.updateToken(id);
    return res.value;
  },
  deleteUser: async (_, { id }, { dataSources }) => {
    // TODO: return meaningful responses
    return dataSources.userAPI.deleteUser(id);
  }
};

export default { Query, Mutation };
