const Query: any = {
  facilityStatuses: async (_, {}, { dataSources }) => {
    return dataSources.facilityStatusAPI.getAllFacilityStatuses();
  }
};

export default { Query };
