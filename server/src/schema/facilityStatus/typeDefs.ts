import { gql } from "apollo-server";

const typeDefs = gql`
  extend type Query {
    facilityStatuses: [String]
  }
`;

export default typeDefs;
