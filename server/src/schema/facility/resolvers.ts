const queryBuilder = (
  after: string,
  pageSize: number | string,
  sortBy: string,
  sortDir: string,
  statuses: string[],
  ids: number[] | string[],
  geometry: string,
  maxDistance: string,
  at: string
) => {
  let query = "";
  if (after) {
    query =
      query === ""
        ? query.concat("?after=", after)
        : query.concat("&after=", after);
  }
  if (pageSize) {
    query =
      query === ""
        ? query.concat("?pageSize=", pageSize.toString())
        : query.concat("&pageSize=", pageSize.toString());
  }
  if (sortBy) {
    query =
      query === ""
        ? query.concat("?sortBy=", sortBy)
        : query.concat("&sortBy=", sortBy);
  }
  if (sortDir) {
    query =
      query === ""
        ? query.concat("?sortDir=", sortDir)
        : query.concat("&sortDir=", sortDir);
  }
  if (Array.isArray(statuses) && statuses.length) {
    statuses.forEach(element => {
      query =
        query === ""
          ? query.concat("?statuses=", element)
          : query.concat("&statuses=", element);
    });
  }
  if (Array.isArray(ids) && ids.length) {
    ids.forEach(element => {
      query =
        query === ""
          ? query.concat("?ids=", element)
          : query.concat("&ids=", element);
    });
  }
  if (geometry) {
    query =
      query === ""
        ? query.concat("?geometry=", geometry)
        : query.concat("&geometry=", geometry);
  }
  if (maxDistance) {
    query =
      query === ""
        ? query.concat("?maxDistance=", maxDistance)
        : query.concat("&maxDistance=", maxDistance);
  }
  if (at) {
    query = query === "" ? query.concat("?at=", at) : query.concat("&at=", at);
  }
  return query;
};
const Query: any = {
  facilities: async (
    _,
    { after, pageSize, sortBy, sortDir, statuses, ids, geometry, maxDistance },
    { dataSources }
  ) => {
    const query = queryBuilder(
      after,
      pageSize,
      sortBy,
      sortDir,
      statuses,
      ids,
      geometry,
      maxDistance,
      null
    );

    const response = await dataSources.facilityAPI.getAllFacilities(query);

    // const facilities = paginateResults({
    //   after,
    //   pageSize,
    //   results: allFacilities
    // });
    // ** TODO : sorting based on the query strings
    // Sort the results based on sortBy arguments

    return {
      totalCount: response.results.length,
      facilities: response.results,
      // cursor: facilities.length
      //   ? facilities[facilities.length - 1].cursor
      //   : null,
      // if the cursor of the end of the paginated results is the same as the
      // last item in _all_ results, then there are no more results after this
      hasMore: response.hasMore
      // facilities.length
      //   ? facilities[facilities.length - 1].cursor !==
      //     allFacilities[allFacilities.length - 1].cursor
      //   : false
    };
  },
  facilityDetails: async (_, { id }, { dataSources }) => {
    const facilityDetails = await dataSources.facilityAPI.getFacilityById(id);
    return facilityDetails;
  },
  facilityUtilization: async (_, { id }, { dataSources }) => {
    const facilityUtilization = await dataSources.facilityAPI.getFacilityUtilization(
      id
    );
    return facilityUtilization;
  },
  facilityPrediction: async (_, { id, at, after }, { dataSources }) => {
    const query = queryBuilder(
      after,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      at
    );
    return dataSources.facilityAPI.getFacilityPrediction(id, query);
  },
  facilitiesGeoJSON: async (_, {}, { dataSources }) => {
    const allFacilities = await dataSources.facilityAPI.getAllFacilities(
      "",
      true
    );
    return JSON.parse(allFacilities);
  },
  facilityGeoJSON: async (_, { id }, { dataSources }) => {
    const facilityDetailsGeoJSON = await dataSources.facilityAPI.getFacilityGeoJSON(
      id
    );
    return JSON.parse(facilityDetailsGeoJSON);
  },
  facilitiesSummary: async (_, { summary }, { dataSources }) => {
    const facilitiesSummary = await dataSources.facilityAPI.getFacilitiesSummary(
      summary
    );
    return facilitiesSummary;
  }
};

const Mutation: any = {
  updateUtilization: async (
    _,
    { facilityId, newUtilizations },
    { dataSources, token }
  ) => {
    return dataSources.facilityAPI.updateFacilitiyUtilization(
      facilityId,
      token,
      newUtilizations
    );
  }
};

export default { Query, Mutation };
