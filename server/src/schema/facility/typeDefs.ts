import { gql } from "apollo-server";

const typeDefs = gql`
  extend type Query {
    facilities(
      after: String
      pageSize: Int
      sortBy: String
      sortDir: String
      statuses: [String]
      ids: [Int]
      geometry: String
      maxDistance: Float
    ): FacilityResponse!
    facilityDetails(id: ID!, geojson: Boolean): FacilityDetails!
    facilitiesSummary(summary: Boolean): FacilitiesSummary
    facilityUtilization(id: ID!): [Utilization!]!
    facilityPrediction(id: ID!, after: Int, at: String): [Prediction]
    facilitiesGeoJSON: FacilitiesGeoJSONResponse
    facilityGeoJSON(id: ID!): FacilityGeoJSON
  }

  type Facility {
    id: Int!
    name: LocalizedObject
    status: String
    landings: Int
    type: String
    reuse_count: Int
    operatorId: Int
    builtCapacity: Capacity
    usages: [String]
    location: Location
  }
  type FacilityDetails {
    id: Int!
    name: LocalizedObject!
    location: Location
    operatorId: Int!
    status: String
    pricingMethod: String!
    statusDescription: LocalizedObject
    builtCapacity: Capacity
    usages: [String]
    pricing: [Pricing!]!
    unavailableCapacities: [UnavailableCapacity!]!
    aliases: [String!]!
    ports: [Port!]!
    services: [String!]!
    contacts: ContactOptions!
    paymentInfo: PaymentInfo!
    openingHours: OpeningHour!
  }
  type OpeningHour {
    openNow: Boolean
    byDayType: Day
    info: LocalizedObject
    url: LocalizedObject
  }
  type Day {
    BUSINESS_DAY: FromUntil
    SATURDAY: FromUntil
    SUNDAY: FromUntil
  }
  type FromUntil {
    from: String
    until: String
  }
  type PaymentInfo {
    detail: LocalizedObject
    url: LocalizedObject
    paymentMethods: [String!]!
  }
  type ContactOptions {
    emergency: Int
    operator: Int
    service: Int
  }
  type PortLocation {
    crs: Crs
    bbox: [Float]
    type: String
    coordinates: [Float]!
  }
  type Port {
    location: PortLocation
    entry: Boolean
    exit: Boolean
    pedestrian: Boolean
    bicycle: Boolean
    address: Address
    info: LocalizedObject
  }
  type Address {
    streetAddress: LocalizedObject
    postalCode: String
    city: LocalizedObject
  }

  type Pricing {
    usage: String!
    capacityType: String!
    maxCapacity: Int!
    dayType: String!
    time: PricingTimeRange!
    price: LocalizedObject
  }

  type PricingTimeRange {
    from: String!
    until: String!
  }
  type LocalizedObject {
    fi: String
    sv: String
    en: String
  }
  type FacilityResponse {
    cursor: String
    totalCount: Int
    hasMore: Boolean!
    facilities: [Facility!]!
  }

  type Utilization {
    facilityId: Int!
    capacityType: String!
    usage: String!
    timestamp: String
    spacesAvailable: Int
    capacity: Int
    openNow: Boolean
  }

  type Prediction {
    capacityType: String
    usage: String
    timestamp: String
    spacesAvailable: Int
    facilityId: Int
    hubId: Int
  }

  type Properties {
    name: LocalizedObject
    status: String
    operatorId: Int
    builtCapacity: Capacity
    usages: [String]
    facilityIds: [Int]
  }
  type FacilitiesSummary {
    facilityCount: Int
    capacities: Capacity
  }
  type Capacity {
    CAR: Int
    ELECTRIC_CAR: Int
    BICYCLE_SECURE_SPACE: Int
    DISABLED: Int
    MOTORCYCLE: Int
    BICYCLE: Int
  }

  type FacilitiesGeoJSONResponse {
    hasMore: Boolean
    features: [FacilityGeoJSON]
    type: String
  }
  type FacilityGeoJSON {
    id: Int
    geometry: Geometry
    properties: Properties
    type: String
  }
  type Geometry {
    crs: Crs
    bbox: [Float]
    type: String
  }
  type UnavailableCapacity {
    capacityType: String!
    usage: String!
    capacity: Int!
  }

  type Crs {
    type: String
    properties: Name
    bbox: [Float]
  }
  type Name {
    name: String
  }

  type Location {
    crs: Crs
    bbox: [Float]
    type: String
    coordinates: [[[Float]]]
  }

  input UtilizationInput {
    capacityType: String
    usage: String
    timestamp: String
    spacesAvailable: Int
    capacity: Int
  }
  type UtilizationMutationResponse {
    facilityId: Int
    capacityType: String
    usage: String
    timestamp: String
    spacesAvailable: Int
    capacity: Int
  }
  extend type Mutation {
    updateUtilization(
      facilityId: ID!
      newUtilizations: [UtilizationInput]
    ): [UtilizationMutationResponse]
  }
`;

export default typeDefs;
