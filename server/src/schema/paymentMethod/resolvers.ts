const Query: any = {
  paymentMethods: async (_, {}, { dataSources }) => {
    return dataSources.paymentMethodAPI.getAllPaymentMethods();
  }
};

export default { Query };
