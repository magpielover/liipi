import { gql } from "apollo-server";

const typeDefs = gql`
  extend type Query {
    paymentMethods: [String]
  }
`;

export default typeDefs;
