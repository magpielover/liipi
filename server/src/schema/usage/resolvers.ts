const Query: any = {
  usages: async (_, {}, { dataSources }) => {
    return dataSources.usageAPI.getAllUsages();
  }
};

export default { Query };
