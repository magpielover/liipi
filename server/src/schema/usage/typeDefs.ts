import { gql } from "apollo-server";

const typeDefs = gql`
  extend type Query {
    usages: [String!]
  }
`;

export default typeDefs;
