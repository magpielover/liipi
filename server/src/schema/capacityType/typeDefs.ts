import { gql } from "apollo-server";

const typeDefs = gql`
  extend type Query {
    capacityTypes: [String!]
  }
`;

export default typeDefs;
