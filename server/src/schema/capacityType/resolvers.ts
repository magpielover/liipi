const Query: any = {
  capacityTypes: async (_, {}, { dataSources }) => {
    return dataSources.capacityTypeAPI.getAllCapacityTypes();
  }
};

export default { Query };
