import { merge } from "lodash";
import capacityType from "./capacityType/resolvers";
import contact from "./contact/resolvers";
import dayType from "./dayType/resolvers";
import facility from "./facility/resolvers";
import facilityStatus from "./facilityStatus/resolvers";
import hub from "./hub/resolvers";
import login from "./login/resolvers";
import operator from "./operator/resolvers";
import paymentMethod from "./paymentMethod/resolvers";
import pricingMethod from "./pricingMethod/resolvers";
import region from "./region/resolvers";
import report from "./report/resolvers";
import service from "./service/resolvers";
import usage from "./usage/resolvers";
import user from "./user/resolvers";
import utilization from "./utilization/resolvers";
const resolvers = merge(
  facility,
  utilization,
  hub,
  login,
  operator,
  capacityType,
  dayType,
  facilityStatus,
  pricingMethod,
  contact,
  region,
  report,
  usage,
  service,
  paymentMethod,
  user
);

export default resolvers;
