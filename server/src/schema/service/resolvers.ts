const Query: any = {
  services: async (_, {}, { dataSources }) => {
    return dataSources.serviceAPI.getAllServices();
  }
};

export default { Query };
