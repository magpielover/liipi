import { gql } from "apollo-server";

const typeDefs = gql`
  extend type Query {
    services: [String]
  }
`;

export default typeDefs;
