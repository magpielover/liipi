const Query: any = {
  pricingMethods: async (_, {}, { dataSources }) => {
    return dataSources.pricingMethodAPI.getAllPricingMethods();
  }
};

export default { Query };
