import { gql } from "apollo-server";

const typeDefs = gql`
  extend type Query {
    pricingMethods: [String]
  }
`;

export default typeDefs;
