import { gql } from "apollo-server";

const typeDefs = gql`
  extend type Query {
    regions: [Region]!
    regionsWithHubs: [RegionWithHubs!]!
  }

  type Region {
    id: ID!
    name: LocalizedObject
  }
  type RegionWithHubs {
    id: ID!
    name: LocalizedObject
    hubIds: [ID!]!
  }
`;

export default typeDefs;
