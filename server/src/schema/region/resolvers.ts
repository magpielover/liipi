const Query: any = {
  regions: async (_, {}, { dataSources }) => {
    return dataSources.regionAPI.getAllRegions();
  },
  regionsWithHubs: async (_, {}, { dataSources }) => {
    return dataSources.regionAPI.getRegionsWithHubs();
  }
};

export default { Query };
