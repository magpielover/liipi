const Query: any = {
  contacts: async (_, { ids }, { dataSources }) => {
    return dataSources.contactAPI.getAllContacts(ids);
  },
  contactDetails: async (_, { id }, { dataSources }) => {
    return dataSources.contactAPI.getContactById(id);
  }
};

export default { Query };
