import { gql } from "apollo-server";

const typeDefs = gql`
  extend type Query {
    contacts(ids: String): ContactResponse!
    contactDetails(id: ID!): Contact!
  }
  type ContactResponse {
    results: [Contact!]!
  }
  type Contact {
    id: Int!
    name: LocalizedObject!
    operatorId: Int
    phone: String
    email: String
    address: Address
    openingHours: LocalizedObject
    info: LocalizedObject
    hasMore: Boolean
  }
`;

export default typeDefs;
