import { ApolloServer } from "apollo-server-express";
import cors from "cors";
import dotenv from "dotenv";
import express from "express";
import depthLimit from "graphql-depth-limit";
import CapacityTypeAPI from "./datasources/capacityType";
import ContactAPI from "./datasources/contact";
import DayTypeAPI from "./datasources/dayType";
import FacilityAPI from "./datasources/facility";
import FacilityStatusAPI from "./datasources/facilityStatus";
import HubAPI from "./datasources/hub";
import LoginAPI from "./datasources/login";
import OperatorAPI from "./datasources/operator";
import PaymentMethodAPI from "./datasources/paymentMethod";
import PricingMethodAPI from "./datasources/pricingMethod";
import RegionAPI from "./datasources/region";
import ReportAPI from "./datasources/report";
import ServiceAPI from "./datasources/service";
import UsageAPI from "./datasources/usage";
import UserAPI from "./datasources/user";
import UtilizationAPI from "./datasources/utilization";
import schema from "./schema";
dotenv.config();

const dataSources = () => ({
    facilityAPI: new FacilityAPI(),
    utilizationAPI: new UtilizationAPI(),
    hubAPI: new HubAPI(),
    loginAPI: new LoginAPI(),
    operatorAPI: new OperatorAPI(),
    contactAPI: new ContactAPI(),
    pricingMethodAPI: new PricingMethodAPI(),
    facilityStatusAPI: new FacilityStatusAPI(),
    capacityTypeAPI: new CapacityTypeAPI(),
    regionAPI: new RegionAPI(),
    reportAPI: new ReportAPI(),
    usageAPI: new UsageAPI(),
    serviceAPI: new ServiceAPI(),
    paymentMethodAPI: new PaymentMethodAPI(),
    dayTypeAPI: new DayTypeAPI(),
    userAPI: new UserAPI()
  });

  /* if we have a local memcache server, apollo can be configured this way */
  // Class extended because MemcachedCache is missing the implementation of delete method
  // class MemcachedCacheCustom extends MemcachedCache {
  //   constructor(serverLocation: Memcached.Location, options?: Memcached.options) {
  //     super(serverLocation, options);
  //     this.client.delete = promisify(this.client.delete).bind(this.client);
  //   }
  //   public async delete(key: string): Promise<boolean | undefined> {
  //     return this.client.del(key);
  //   }
  // }
  // const cache = new MemcachedCacheCustom(
  //   ["memcached-server-1", "memcached-server-2", "memcached-server-3"],
  //   { retries: 10, retry: 10000 } // Options
  // );
(async () => {
  const server = new ApolloServer({
    schema,
    debug: process.env.DEBUG === "debug" || process.env.ENV !== "production",
    dataSources,
    formatError: err => {
      return err;
    },
    context: ({ req }) => {
      const token = req.headers.authorization || "";
      return { token };
    },
    engine: {
      apiKey: process.env.ENGINE_API_KEY
    },
    validationRules: [depthLimit(10)]
  });
  const app = express();
  app.use(cors());
  server.applyMiddleware({ app });
  app.listen({ port: 4000 }, () =>
    console.log(`🚀 Server ready at http://localhost:4000${server.graphqlPath}`)
  );
})();
