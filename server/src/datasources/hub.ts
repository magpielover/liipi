import DataSource from "./DataSource";

class HubAPI extends DataSource {
  // willSendRequest(request) {
  //   request.headers.set('Authorization', this.context.token);
  // }

  // if we do the sorting and pagination on the graphql side
  public hubReducer(hub) {
    return {
      id: hub.id,
      cursor: new Buffer(hub.id.toString(), "base64").toString("ascii"),
      name: hub.name,
      location: hub.location,
      operatorId: hub.operatorId,
      status: hub.status,
      pricingMethod: hub.pricingMethod,
      statusDescription: hub.statusDescription,
      builtCapacity: hub.builtCapacity,
      usages: hub.usages
    };
  }

  public async getAllHubs(query, geojson) {
    if (geojson) {
      return this.get("hubs.geojson");
      // const results = response.results
      // return Array.isArray(results)
      // ? results.map(hub => this.hubReducer(hub)) : [];
    } else {
      return this.get("hubs" + query);
      // const results = response.results
      // return Array.isArray(results)
      // ? results.map(hub => this.hubReducer(hub)) : [];
    }
  }

  public async getHubById(hubId: any, geojson: true) {
    if (geojson) {
      return this.get("hubs/" + hubId + ".geojson");
    }
    return this.get("hubs/" + hubId);
  }
  public async getHubUtilization(hubId: any) {
    return this.get("hubs/" + hubId + "/utilization");
  }
  public async getHubPrediction(hubId: any, query: string) {
    return this.get("hubs/" + hubId + "/prediction" + query);
  }

  public async getHubsByIds(hubIds, geojson) {
    return Promise.all(hubIds.map(hubId => this.getHubById(hubId, geojson)));
  }
  public async getHubsSummary(summary: boolean) {
    return this.get("hubs?summary=" + summary);
  }
  public async getHubGeoJSON(hubId: any) {
    return this.get("hubs/" + hubId + ".geojson");
  }
}

export default HubAPI;
