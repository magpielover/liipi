import DataSource, { XLSXContentType } from "./DataSource";

class ReportAPI extends DataSource {
  public willSendRequest(request) {
    if (this.context.token) {
      request.headers.set("Authorization", this.context.token);
    }
    request.headers.set("Accept", XLSXContentType);
    request.headers.set("Content-Type", "application/json");
  }

  public async downloadFacilityUsageReport(payload) {
    return this.post("reports/FacilityUsage", payload);
  }

  public async downloadHubsAndFacilitiesReport(payload) {
    return this.post("reports/HubsAndFacilities", payload);
  }

  public async downloadMaxUtilizationReport(payload) {
    return this.post("reports/MaxUtilization", payload);
  }

  public async downloadRequestLogReport(payload) {
    return this.post("reports/RequestLog", payload);
  }
}

export default ReportAPI;
