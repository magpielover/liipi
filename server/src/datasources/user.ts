import DataSource from "./DataSource";

class UserAPI extends DataSource {
  constructor() {
    super();
    this.baseURL = process.env.INTERNAL_API_BASE_URL;
  }

  public willSendRequest(request) {
    request.headers.set("Authorization", this.context.token);
    request.headers.set("Content-Type", "application/json");
  }

  public async getAllUsers(query) {
    return this.get("users");
  }
  public async getUserById(userId: string | number) {
    return this.get("users/" + userId);
  }
  public async changePassword(userId, value) {
    const payload = { value };
    return this.put("users/" + userId + "/password", JSON.stringify(payload));
  }
  public async createUser(user) {
    return this.post("users", JSON.stringify(user));
  }
  public async updateToken(userId) {
    return this.put("users/" + userId + "/token");
  }
  public async deleteUser(userId) {
    return this.delete("users/" + userId);
  }
}

export default UserAPI;
