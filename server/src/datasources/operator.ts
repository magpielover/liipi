import DataSource from "./DataSource";

class OperatorAPI extends DataSource {
  public async getAllOperators() {
    return this.get("operators");
  }

  public async getOperatorById(operatorId: any) {
    return this.get("operators/" + operatorId);
  }
}

export default OperatorAPI;
