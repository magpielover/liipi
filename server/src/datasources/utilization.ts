import DataSource from "./DataSource";

class UtilizationAPI extends DataSource {
  /**
   *
   * Get the latest utilization for all facilities.
   */
  public async getUtilizations() {
    return this.get("utilizations");
  }
}

export default UtilizationAPI;
