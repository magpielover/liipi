import DataSource from "./DataSource";

class UsageAPI extends DataSource {
  /**
   *
   * Get the usages. Usage specifies the purpose of the capacity.
   */
  public async getAllUsages() {
    return this.get("usages");
  }
}

export default UsageAPI;
