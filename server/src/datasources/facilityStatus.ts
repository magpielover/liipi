import DataSource from "./DataSource";

class FacilityStatusAPI extends DataSource {
  /**
   *
   * Facility status indicates whether the facility is operating normally or is exceptionally closed
   */
  public async getAllFacilityStatuses() {
    return this.get("facility-statuses");
  }
}

export default FacilityStatusAPI;
