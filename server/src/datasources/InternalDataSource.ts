import { RESTDataSource } from "apollo-datasource-rest";

class InternalDataSource extends RESTDataSource {
  public baseURL = process.env.INTERNAL_API_BASE_URL;
}

export default InternalDataSource;
