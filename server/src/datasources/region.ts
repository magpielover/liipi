import DataSource from "./DataSource";

class RegionAPI extends DataSource {
  public willSendRequest(request) {
    if (this.context.token) {
      request.headers.set("Authorization", this.context.token);
    }
  }

  public async getAllRegions() {
    return this.get("regions");
  }

  public async getRegionsWithHubs() {
    return this.get("regions/withHubs");
  }
}

export default RegionAPI;
