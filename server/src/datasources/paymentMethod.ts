import DataSource from "./DataSource";

class PaymentMethodAPI extends DataSource {
  /**
   * Payment methods accepted by a facility
   */
  public async getAllPaymentMethods() {
    return this.get("payment-methods");
  }
}

export default PaymentMethodAPI;
