import DataSource from "./DataSource";

class FacilityAPI extends DataSource {
  public willSendRequest(request) {
    request.headers.set("Authorization", this.context.token);
  }

  // if we do the sorting and pagination on the graphql side
  public facilityReducer(facility) {
    return {
      id: facility.id,
      cursor: new Buffer(facility.id.toString(), "base64").toString("ascii"),
      name: facility.name,
      location: facility.location,
      operatorId: facility.operatorId,
      status: facility.status,
      pricingMethod: facility.pricingMethod,
      statusDescription: facility.statusDescription,
      builtCapacity: facility.builtCapacity,
      usages: facility.usages
    };
  }

  public async getAllFacilities(query, geojson) {
    if (geojson) {
      return this.get("facilities.geojson");
      // const results = response.results
      // return Array.isArray(results)
      // ? results.map(facility => this.facilityReducer(facility)) : [];
    } else {
      return this.get("facilities" + query);
      // const results = response.results
      // return Array.isArray(results)
      // ? results.map(facility => this.facilityReducer(facility)) : [];
    }

    // transform the raw facilities to a more friendly
  }

  public async getFacilityById(facilityId: any, geojson: true) {
    if (geojson) {
      return this.get("facilities/" + facilityId + ".geojson");
    }
    return this.get("facilities/" + facilityId);
  }
  public async getFacilityUtilization(facilityId: any) {
    return this.get("facilities/" + facilityId + "/utilization");
  }
  public async getFacilityPrediction(facilityId: any, query: string) {
    return this.get("facilities/" + facilityId + "/prediction" + query);
  }

  public async getFacilitiesByIds(facilityIds, geojson) {
    return Promise.all(
      facilityIds.map(facilityId => this.getFacilityById(facilityId, geojson))
    );
  }
  public async getFacilitiesSummary(summary: boolean) {
    return this.get("facilities?summary=" + summary);
  }
  public async getFacilityGeoJSON(facilityId: any) {
    return this.get("facilities/" + facilityId + ".geojson");
  }
  public async updateFacilitiyUtilization(facilityId, token, payload) {
    const init = {
      headers: {
        "Content-Type": "application/json",
        Authorisation: token
      }
    };

    return this.put("facilities/" + facilityId + "/utilization", payload, init);
  }
}

export default FacilityAPI;
