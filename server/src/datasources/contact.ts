import DataSource from "./DataSource";

class ContactAPI extends DataSource {
  // willSendRequest(request) {
  //   request.headers.set('Authorization', this.context.token);
  // }

  // if we do the sorting and pagination on the graphql side
  public contactReducer(contact) {
    return {
      id: contact.id,
      cursor: new Buffer(contact.id.toString(), "base64").toString("ascii"),
      name: contact.name,
      location: contact.location,
      contactId: contact.contactId,
      status: contact.status,
      pricingMethod: contact.pricingMethod,
      statusDescription: contact.statusDescription,
      builtCapacity: contact.builtCapacity,
      usages: contact.usages
    };
  }

  public async getAllContacts(ids) {
    if (ids) {
      return this.get("contacts?ids=" + ids);
    }
    return this.get("contacts");
  }

  public async getContactById(contactId: any) {
    return this.get("contacts/" + contactId);
  }
}

export default ContactAPI;
