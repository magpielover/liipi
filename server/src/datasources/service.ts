import DataSource from "./DataSource";

class ServiceAPI extends DataSource {
  /**
   * Services provided by a facility:
   */
  public async getAllServices() {
    return this.get("services");
  }
}

export default ServiceAPI;
