import DataSource from "./DataSource";

class PricingMethodsAPI extends DataSource {
  /**
   * Pricing method is a shortcut for maintaining actual facility pricing information.
   */
  public async getAllPricingMethods() {
    return this.get("pricing-methods");
  }
}

export default PricingMethodsAPI;
