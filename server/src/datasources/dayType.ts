import DataSource from "./DataSource";

class DayTypeAPI extends DataSource {
  /**
   *  Day type is a rough categorization of day types with different opening hours and/or pricing
   */
  public async getAllDayTypes() {
    return this.get("day-types");
  }
}

export default DayTypeAPI;
