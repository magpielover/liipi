import InternalDataSource from "./InternalDataSource";

class LoginAPI extends InternalDataSource {
  public async postLogin(credentials) {
    return this.post("login", credentials);
  }
}

export default LoginAPI;
