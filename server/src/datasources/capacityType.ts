import DataSource from "./DataSource";

class CapacityTypeAPI extends DataSource {
  /**
   * Capacity type defines different kind of parking slot types.
   */
  public async getAllCapacityTypes() {
    return this.get("capacity-types");
  }
}

export default CapacityTypeAPI;
