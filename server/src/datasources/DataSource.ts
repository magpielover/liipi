import { RESTDataSource } from "apollo-datasource-rest";

export const XLSXContentType =
  "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;";
class DataSource extends RESTDataSource {
  public baseURL = process.env.API_BASE_URL;

  // Override parseBody function to support arrayBuffer for application/vnd.openxmlformats-officedocument.spreadsheetml.sheet mime type
  public parseBody(response): Promise<object | string> {
    const contentType = response.headers.get("Content-Type");
    const contentLength = response.headers.get("Content-Length");

    if (
      response.status !== 204 &&
      contentLength !== "0" &&
      contentType &&
      contentType.startsWith(XLSXContentType)
    ) {
      return response.arrayBuffer();
    } else if (
      response.status !== 204 &&
      contentLength !== "0" &&
      contentType &&
      (contentType.startsWith("application/json") ||
        contentType.startsWith("application/hal+json"))
    ) {
      return response.json();
    } else {
      return response.text();
    }
  }
}

export default DataSource;
