export type Maybe<T> = T | null;

export interface UtilizationInput {
  capacityType?: Maybe<string>;

  usage?: Maybe<string>;

  timestamp?: Maybe<string>;

  spacesAvailable?: Maybe<number>;

  capacity?: Maybe<number>;
}

export interface UserInput {
  role?: Maybe<string>;

  username?: Maybe<string>;

  password?: Maybe<string>;

  operatorId?: Maybe<number>;
}

// ====================================================
// Types
// ====================================================

export interface Query {
  capacityTypes?: Maybe<string[]>;

  contacts: ContactResponse;

  contactDetails: Contact;

  dayTypes?: Maybe<(Maybe<string>)[]>;

  facilities: FacilityResponse;

  facilityDetails: FacilityDetails;

  facilitiesSummary?: Maybe<FacilitiesSummary>;

  facilityUtilization: Utilization[];

  facilityPrediction?: Maybe<(Maybe<Prediction>)[]>;

  facilitiesGeoJSON?: Maybe<FacilitiesGeoJsonResponse>;

  facilityGeoJSON?: Maybe<FacilityGeoJson>;

  facilityStatuses?: Maybe<(Maybe<string>)[]>;

  _empty?: Maybe<string>;

  hubs: HubResponse;

  hubDetails: HubDetails;

  hubsSmmary?: Maybe<HubsSummary>;

  hubUtilization?: Maybe<(Maybe<Utilization>)[]>;

  hubPrediction?: Maybe<(Maybe<Prediction>)[]>;

  hubsGeoJSON?: Maybe<HubsGeoJsonResponse>;

  hubGeoJSON?: Maybe<HubGeoJson>;

  operators: OperatorResponse;

  operatorDetails: Operator;

  paymentMethods?: Maybe<(Maybe<string>)[]>;

  pricingMethods?: Maybe<(Maybe<string>)[]>;

  regions: (Maybe<Region>)[];

  regionsWithHubs: RegionWithHubs[];

  services?: Maybe<(Maybe<string>)[]>;

  usages?: Maybe<string[]>;

  users?: Maybe<(Maybe<User>)[]>;

  utilizations?: Maybe<(Maybe<Utilization>)[]>;
}

export interface ContactResponse {
  results: Contact[];
}

export interface Contact {
  id: number;

  name: LocalizedObject;

  operatorId?: Maybe<number>;

  phone?: Maybe<string>;

  email?: Maybe<string>;

  address?: Maybe<Address>;

  openingHours?: Maybe<LocalizedObject>;

  info?: Maybe<LocalizedObject>;

  hasMore?: Maybe<boolean>;
}

export interface LocalizedObject {
  fi?: Maybe<string>;

  sv?: Maybe<string>;

  en?: Maybe<string>;
}

export interface Address {
  streetAddress?: Maybe<LocalizedObject>;

  postalCode?: Maybe<string>;

  city?: Maybe<LocalizedObject>;
}

export interface FacilityResponse {
  cursor?: Maybe<string>;

  totalCount?: Maybe<number>;

  hasMore: boolean;

  facilities: Facility[];
}

export interface Facility {
  id: number;

  name?: Maybe<LocalizedObject>;

  status?: Maybe<string>;

  landings?: Maybe<number>;

  type?: Maybe<string>;

  reuse_count?: Maybe<number>;

  operatorId?: Maybe<number>;

  builtCapacity?: Maybe<Capacity>;

  usages?: Maybe<(Maybe<string>)[]>;

  location?: Maybe<Location>;
}

export interface Capacity {
  CAR?: Maybe<number>;

  ELECTRIC_CAR?: Maybe<number>;

  BICYCLE_SECURE_SPACE?: Maybe<number>;

  DISABLED?: Maybe<number>;

  MOTORCYCLE?: Maybe<number>;

  BICYCLE?: Maybe<number>;
}

export interface Location {
  crs?: Maybe<Crs>;

  bbox?: Maybe<(Maybe<number>)[]>;

  type?: Maybe<string>;

  coordinates?: Maybe<(Maybe<number>)[][][]>;
}

export interface Crs {
  type?: Maybe<string>;

  properties?: Maybe<Name>;

  bbox?: Maybe<(Maybe<number>)[]>;
}

export interface Name {
  name?: Maybe<string>;
}

export interface FacilityDetails {
  id: number;

  name: LocalizedObject;

  location?: Maybe<Location>;

  operatorId: number;

  status?: Maybe<string>;

  pricingMethod: string;

  statusDescription?: Maybe<LocalizedObject>;

  builtCapacity?: Maybe<Capacity>;

  usages?: Maybe<(Maybe<string>)[]>;

  pricing: Pricing[];

  unavailableCapacities: UnavailableCapacity[];

  aliases: string[];

  ports: Port[];

  services: string[];

  contacts: ContactOptions;

  paymentInfo: PaymentInfo;

  openingHours: OpeningHour;
}

export interface Pricing {
  usage: string;

  capacityType: string;

  maxCapacity: number;

  dayType: string;

  time: PricingTimeRange;

  price?: Maybe<LocalizedObject>;
}

export interface PricingTimeRange {
  from: string;

  until: string;
}

export interface UnavailableCapacity {
  capacityType: string;

  usage: string;

  capacity: number;
}

export interface Port {
  location?: Maybe<PortLocation>;

  entry?: Maybe<boolean>;

  exit?: Maybe<boolean>;

  pedestrian?: Maybe<boolean>;

  bicycle?: Maybe<boolean>;

  address?: Maybe<Address>;

  info?: Maybe<LocalizedObject>;
}

export interface PortLocation {
  crs?: Maybe<Crs>;

  bbox?: Maybe<(Maybe<number>)[]>;

  type?: Maybe<string>;

  coordinates: (Maybe<number>)[];
}

export interface ContactOptions {
  emergency?: Maybe<number>;

  operator?: Maybe<number>;

  service?: Maybe<number>;
}

export interface PaymentInfo {
  detail?: Maybe<LocalizedObject>;

  url?: Maybe<LocalizedObject>;

  paymentMethods: string[];
}

export interface OpeningHour {
  openNow?: Maybe<boolean>;

  byDayType?: Maybe<Day>;

  info?: Maybe<LocalizedObject>;

  url?: Maybe<LocalizedObject>;
}

export interface Day {
  BUSINESS_DAY?: Maybe<FromUntil>;

  SATURDAY?: Maybe<FromUntil>;

  SUNDAY?: Maybe<FromUntil>;
}

export interface FromUntil {
  from?: Maybe<string>;

  until?: Maybe<string>;
}

export interface FacilitiesSummary {
  facilityCount?: Maybe<number>;

  capacities?: Maybe<Capacity>;
}

export interface Utilization {
  facilityId: number;

  capacityType: string;

  usage: string;

  timestamp?: Maybe<string>;

  spacesAvailable?: Maybe<number>;

  capacity?: Maybe<number>;

  openNow?: Maybe<boolean>;
}

export interface Prediction {
  capacityType?: Maybe<string>;

  usage?: Maybe<string>;

  timestamp?: Maybe<string>;

  spacesAvailable?: Maybe<number>;

  facilityId?: Maybe<number>;

  hubId?: Maybe<number>;
}

export interface FacilitiesGeoJsonResponse {
  hasMore?: Maybe<boolean>;

  features?: Maybe<(Maybe<FacilityGeoJson>)[]>;

  type?: Maybe<string>;
}

export interface FacilityGeoJson {
  id?: Maybe<number>;

  geometry?: Maybe<Geometry>;

  properties?: Maybe<Properties>;

  type?: Maybe<string>;
}

export interface Geometry {
  crs?: Maybe<Crs>;

  bbox?: Maybe<(Maybe<number>)[]>;

  type?: Maybe<string>;
}

export interface Properties {
  name?: Maybe<LocalizedObject>;

  status?: Maybe<string>;

  operatorId?: Maybe<number>;

  builtCapacity?: Maybe<Capacity>;

  usages?: Maybe<(Maybe<string>)[]>;

  facilityIds?: Maybe<(Maybe<number>)[]>;
}

export interface HubResponse {
  cursor?: Maybe<string>;

  totalCount?: Maybe<number>;

  hasMore: boolean;

  hubs: Hub[];
}

export interface Hub {
  id: number;

  name?: Maybe<LocalizedObject>;

  location?: Maybe<Location>;

  facilityIds: number[];

  address?: Maybe<Address>;

  hasMore?: Maybe<boolean>;
}

export interface HubDetails {
  id?: Maybe<number>;

  name?: Maybe<LocalizedObject>;

  location?: Maybe<HubLocation>;

  facilityIds: number[];

  address?: Maybe<Address>;
}

export interface HubLocation {
  crs?: Maybe<Crs>;

  bbox?: Maybe<(Maybe<number>)[]>;

  type?: Maybe<string>;

  coordinates?: Maybe<(Maybe<number>)[]>;
}

export interface HubsSummary {
  hubCount?: Maybe<number>;

  capacities?: Maybe<Capacity>;
}

export interface HubsGeoJsonResponse {
  hasMore?: Maybe<boolean>;

  features?: Maybe<(Maybe<HubGeoJson>)[]>;

  type?: Maybe<string>;
}

export interface HubGeoJson {
  id?: Maybe<number>;

  geometry?: Maybe<Geometry>;

  properties?: Maybe<Properties>;

  type?: Maybe<string>;
}

export interface OperatorResponse {
  hasMore?: Maybe<boolean>;

  results: Operator[];
}

export interface Operator {
  id: string;

  name: LocalizedObject;
}

export interface Region {
  id: string;

  name?: Maybe<LocalizedObject>;
}

export interface RegionWithHubs {
  id: string;

  name?: Maybe<LocalizedObject>;

  hubIds: string[];
}

export interface User {
  id?: Maybe<number>;

  username?: Maybe<string>;

  role?: Maybe<string>;

  operatorId?: Maybe<number>;
}

export interface Mutation {
  updateUtilization?: Maybe<(Maybe<UtilizationMutationResponse>)[]>;

  _empty?: Maybe<string>;

  login?: Maybe<Login>;

  facilityUsageReport?: Maybe<string>;

  maxUtilizationReport?: Maybe<string>;

  hubsAndFacilitiesReport?: Maybe<string>;

  requestLogReport?: Maybe<string>;

  createUser?: Maybe<User>;

  changePassword?: Maybe<string>;

  updateToken?: Maybe<string>;

  deleteUser?: Maybe<string>;
}

export interface UtilizationMutationResponse {
  facilityId?: Maybe<number>;

  capacityType?: Maybe<string>;

  usage?: Maybe<string>;

  timestamp?: Maybe<string>;

  spacesAvailable?: Maybe<number>;

  capacity?: Maybe<number>;
}

export interface Login {
  operatorId?: Maybe<string>;

  passwordExpireInDays: number;

  permissions: (Maybe<string>)[];

  role: string;

  token: string;

  userId: number;

  username: string;
}

export interface Subscription {
  _empty?: Maybe<string>;
}

// ====================================================
// Arguments
// ====================================================

export interface ContactsQueryArgs {
  ids?: Maybe<string>;
}
export interface ContactDetailsQueryArgs {
  id: string;
}
export interface FacilitiesQueryArgs {
  after?: Maybe<string>;

  pageSize?: Maybe<number>;

  sortBy?: Maybe<string>;

  sortDir?: Maybe<string>;

  statuses?: Maybe<(Maybe<string>)[]>;

  ids?: Maybe<(Maybe<number>)[]>;

  geometry?: Maybe<string>;

  maxDistance?: Maybe<number>;
}
export interface FacilityDetailsQueryArgs {
  id: string;

  geojson?: Maybe<boolean>;
}
export interface FacilitiesSummaryQueryArgs {
  summary?: Maybe<boolean>;
}
export interface FacilityUtilizationQueryArgs {
  id: string;
}
export interface FacilityPredictionQueryArgs {
  id: string;

  after?: Maybe<number>;

  at?: Maybe<string>;
}
export interface FacilityGeoJsonQueryArgs {
  id: string;
}
export interface HubsQueryArgs {
  after?: Maybe<string>;

  pageSize?: Maybe<number>;

  sortBy?: Maybe<string>;

  sortDir?: Maybe<string>;

  statuses?: Maybe<(Maybe<string>)[]>;

  ids?: Maybe<(Maybe<number>)[]>;

  facilityIds?: Maybe<(Maybe<number>)[]>;

  geometry?: Maybe<string>;

  maxDistance?: Maybe<number>;
}
export interface HubDetailsQueryArgs {
  id: string;

  geojson?: Maybe<boolean>;
}
export interface HubsSmmaryQueryArgs {
  summary?: Maybe<boolean>;
}
export interface HubUtilizationQueryArgs {
  id: string;
}
export interface HubPredictionQueryArgs {
  id: string;

  after?: Maybe<number>;

  at?: Maybe<string>;
}
export interface HubGeoJsonQueryArgs {
  id: string;
}
export interface OperatorDetailsQueryArgs {
  id: string;
}
export interface UpdateUtilizationMutationArgs {
  facilityId: string;

  newUtilizations?: Maybe<(Maybe<UtilizationInput>)[]>;
}
export interface LoginMutationArgs {
  password?: Maybe<string>;

  username?: Maybe<string>;
}
export interface FacilityUsageReportMutationArgs {
  capacityTypes?: Maybe<string[]>;

  endDate?: Maybe<number[]>;

  facilities?: Maybe<number[]>;

  hubs?: Maybe<number[]>;

  interval?: Maybe<number>;

  operators?: Maybe<number[]>;

  regions?: Maybe<number[]>;

  startDate?: Maybe<number[]>;

  usages?: Maybe<number[]>;
}
export interface MaxUtilizationReportMutationArgs {
  capacityTypes?: Maybe<string[]>;

  endDate?: Maybe<number[]>;

  facilities?: Maybe<number[]>;

  hubs?: Maybe<number[]>;

  operators?: Maybe<number[]>;

  regions?: Maybe<number[]>;

  startDate?: Maybe<number[]>;

  usages?: Maybe<number[]>;
}
export interface RequestLogReportMutationArgs {
  endDate?: Maybe<number[]>;

  requestLogInterval: string;

  startDate?: Maybe<number[]>;
}
export interface CreateUserMutationArgs {
  user?: Maybe<UserInput>;
}
export interface ChangePasswordMutationArgs {
  id?: Maybe<number>;

  newPassword?: Maybe<string>;
}
export interface UpdateTokenMutationArgs {
  id?: Maybe<number>;
}
export interface DeleteUserMutationArgs {
  id: number;
}

import { GraphQLResolveInfo } from "graphql";

import { MyContext } from "./context";

export type Resolver<Result, Parent = {}, Context = {}, Args = {}> = (
  parent: Parent,
  args: Args,
  context: Context,
  info: GraphQLResolveInfo
) => Promise<Result> | Result;

export interface ISubscriptionResolverObject<Result, Parent, Context, Args> {
  subscribe<R = Result, P = Parent>(
    parent: P,
    args: Args,
    context: Context,
    info: GraphQLResolveInfo
  ): AsyncIterator<R | Result> | Promise<AsyncIterator<R | Result>>;
  resolve?<R = Result, P = Parent>(
    parent: P,
    args: Args,
    context: Context,
    info: GraphQLResolveInfo
  ): R | Result | Promise<R | Result>;
}

export type SubscriptionResolver<
  Result,
  Parent = {},
  Context = {},
  Args = {}
> =
  | ((
      ...args: any[]
    ) => ISubscriptionResolverObject<Result, Parent, Context, Args>)
  | ISubscriptionResolverObject<Result, Parent, Context, Args>;

export type TypeResolveFn<Types, Parent = {}, Context = {}> = (
  parent: Parent,
  context: Context,
  info: GraphQLResolveInfo
) => Maybe<Types>;

export type NextResolverFn<T> = () => Promise<T>;

export type DirectiveResolverFn<TResult, TArgs = {}, TContext = {}> = (
  next: NextResolverFn<TResult>,
  source: any,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => TResult | Promise<TResult>;

export namespace QueryResolvers {
  export interface Resolvers<Context = MyContext, TypeParent = {}> {
    capacityTypes?: CapacityTypesResolver<Maybe<string[]>, TypeParent, Context>;

    contacts?: ContactsResolver<ContactResponse, TypeParent, Context>;

    contactDetails?: ContactDetailsResolver<Contact, TypeParent, Context>;

    dayTypes?: DayTypesResolver<Maybe<(Maybe<string>)[]>, TypeParent, Context>;

    facilities?: FacilitiesResolver<FacilityResponse, TypeParent, Context>;

    facilityDetails?: FacilityDetailsResolver<
      FacilityDetails,
      TypeParent,
      Context
    >;

    facilitiesSummary?: FacilitiesSummaryResolver<
      Maybe<FacilitiesSummary>,
      TypeParent,
      Context
    >;

    facilityUtilization?: FacilityUtilizationResolver<
      Utilization[],
      TypeParent,
      Context
    >;

    facilityPrediction?: FacilityPredictionResolver<
      Maybe<(Maybe<Prediction>)[]>,
      TypeParent,
      Context
    >;

    facilitiesGeoJSON?: FacilitiesGeoJsonResolver<
      Maybe<FacilitiesGeoJsonResponse>,
      TypeParent,
      Context
    >;

    facilityGeoJSON?: FacilityGeoJsonResolver<
      Maybe<FacilityGeoJson>,
      TypeParent,
      Context
    >;

    facilityStatuses?: FacilityStatusesResolver<
      Maybe<(Maybe<string>)[]>,
      TypeParent,
      Context
    >;

    _empty?: _EmptyResolver<Maybe<string>, TypeParent, Context>;

    hubs?: HubsResolver<HubResponse, TypeParent, Context>;

    hubDetails?: HubDetailsResolver<HubDetails, TypeParent, Context>;

    hubsSmmary?: HubsSmmaryResolver<Maybe<HubsSummary>, TypeParent, Context>;

    hubUtilization?: HubUtilizationResolver<
      Maybe<(Maybe<Utilization>)[]>,
      TypeParent,
      Context
    >;

    hubPrediction?: HubPredictionResolver<
      Maybe<(Maybe<Prediction>)[]>,
      TypeParent,
      Context
    >;

    hubsGeoJSON?: HubsGeoJsonResolver<
      Maybe<HubsGeoJsonResponse>,
      TypeParent,
      Context
    >;

    hubGeoJSON?: HubGeoJsonResolver<Maybe<HubGeoJson>, TypeParent, Context>;

    operators?: OperatorsResolver<OperatorResponse, TypeParent, Context>;

    operatorDetails?: OperatorDetailsResolver<Operator, TypeParent, Context>;

    paymentMethods?: PaymentMethodsResolver<
      Maybe<(Maybe<string>)[]>,
      TypeParent,
      Context
    >;

    pricingMethods?: PricingMethodsResolver<
      Maybe<(Maybe<string>)[]>,
      TypeParent,
      Context
    >;

    regions?: RegionsResolver<(Maybe<Region>)[], TypeParent, Context>;

    regionsWithHubs?: RegionsWithHubsResolver<
      RegionWithHubs[],
      TypeParent,
      Context
    >;

    services?: ServicesResolver<Maybe<(Maybe<string>)[]>, TypeParent, Context>;

    usages?: UsagesResolver<Maybe<string[]>, TypeParent, Context>;

    users?: UsersResolver<Maybe<(Maybe<User>)[]>, TypeParent, Context>;

    utilizations?: UtilizationsResolver<
      Maybe<(Maybe<Utilization>)[]>,
      TypeParent,
      Context
    >;
  }

  export type CapacityTypesResolver<
    R = Maybe<string[]>,
    Parent = {},
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type ContactsResolver<
    R = ContactResponse,
    Parent = {},
    Context = MyContext
  > = Resolver<R, Parent, Context, ContactsArgs>;
  export interface ContactsArgs {
    ids?: Maybe<string>;
  }

  export type ContactDetailsResolver<
    R = Contact,
    Parent = {},
    Context = MyContext
  > = Resolver<R, Parent, Context, ContactDetailsArgs>;
  export interface ContactDetailsArgs {
    id: string;
  }

  export type DayTypesResolver<
    R = Maybe<(Maybe<string>)[]>,
    Parent = {},
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type FacilitiesResolver<
    R = FacilityResponse,
    Parent = {},
    Context = MyContext
  > = Resolver<R, Parent, Context, FacilitiesArgs>;
  export interface FacilitiesArgs {
    after?: Maybe<string>;

    pageSize?: Maybe<number>;

    sortBy?: Maybe<string>;

    sortDir?: Maybe<string>;

    statuses?: Maybe<(Maybe<string>)[]>;

    ids?: Maybe<(Maybe<number>)[]>;

    geometry?: Maybe<string>;

    maxDistance?: Maybe<number>;
  }

  export type FacilityDetailsResolver<
    R = FacilityDetails,
    Parent = {},
    Context = MyContext
  > = Resolver<R, Parent, Context, FacilityDetailsArgs>;
  export interface FacilityDetailsArgs {
    id: string;

    geojson?: Maybe<boolean>;
  }

  export type FacilitiesSummaryResolver<
    R = Maybe<FacilitiesSummary>,
    Parent = {},
    Context = MyContext
  > = Resolver<R, Parent, Context, FacilitiesSummaryArgs>;
  export interface FacilitiesSummaryArgs {
    summary?: Maybe<boolean>;
  }

  export type FacilityUtilizationResolver<
    R = Utilization[],
    Parent = {},
    Context = MyContext
  > = Resolver<R, Parent, Context, FacilityUtilizationArgs>;
  export interface FacilityUtilizationArgs {
    id: string;
  }

  export type FacilityPredictionResolver<
    R = Maybe<(Maybe<Prediction>)[]>,
    Parent = {},
    Context = MyContext
  > = Resolver<R, Parent, Context, FacilityPredictionArgs>;
  export interface FacilityPredictionArgs {
    id: string;

    after?: Maybe<number>;

    at?: Maybe<string>;
  }

  export type FacilitiesGeoJsonResolver<
    R = Maybe<FacilitiesGeoJsonResponse>,
    Parent = {},
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type FacilityGeoJsonResolver<
    R = Maybe<FacilityGeoJson>,
    Parent = {},
    Context = MyContext
  > = Resolver<R, Parent, Context, FacilityGeoJsonArgs>;
  export interface FacilityGeoJsonArgs {
    id: string;
  }

  export type FacilityStatusesResolver<
    R = Maybe<(Maybe<string>)[]>,
    Parent = {},
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type _EmptyResolver<
    R = Maybe<string>,
    Parent = {},
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type HubsResolver<
    R = HubResponse,
    Parent = {},
    Context = MyContext
  > = Resolver<R, Parent, Context, HubsArgs>;
  export interface HubsArgs {
    after?: Maybe<string>;

    pageSize?: Maybe<number>;

    sortBy?: Maybe<string>;

    sortDir?: Maybe<string>;

    statuses?: Maybe<(Maybe<string>)[]>;

    ids?: Maybe<(Maybe<number>)[]>;

    facilityIds?: Maybe<(Maybe<number>)[]>;

    geometry?: Maybe<string>;

    maxDistance?: Maybe<number>;
  }

  export type HubDetailsResolver<
    R = HubDetails,
    Parent = {},
    Context = MyContext
  > = Resolver<R, Parent, Context, HubDetailsArgs>;
  export interface HubDetailsArgs {
    id: string;

    geojson?: Maybe<boolean>;
  }

  export type HubsSmmaryResolver<
    R = Maybe<HubsSummary>,
    Parent = {},
    Context = MyContext
  > = Resolver<R, Parent, Context, HubsSmmaryArgs>;
  export interface HubsSmmaryArgs {
    summary?: Maybe<boolean>;
  }

  export type HubUtilizationResolver<
    R = Maybe<(Maybe<Utilization>)[]>,
    Parent = {},
    Context = MyContext
  > = Resolver<R, Parent, Context, HubUtilizationArgs>;
  export interface HubUtilizationArgs {
    id: string;
  }

  export type HubPredictionResolver<
    R = Maybe<(Maybe<Prediction>)[]>,
    Parent = {},
    Context = MyContext
  > = Resolver<R, Parent, Context, HubPredictionArgs>;
  export interface HubPredictionArgs {
    id: string;

    after?: Maybe<number>;

    at?: Maybe<string>;
  }

  export type HubsGeoJsonResolver<
    R = Maybe<HubsGeoJsonResponse>,
    Parent = {},
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type HubGeoJsonResolver<
    R = Maybe<HubGeoJson>,
    Parent = {},
    Context = MyContext
  > = Resolver<R, Parent, Context, HubGeoJsonArgs>;
  export interface HubGeoJsonArgs {
    id: string;
  }

  export type OperatorsResolver<
    R = OperatorResponse,
    Parent = {},
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type OperatorDetailsResolver<
    R = Operator,
    Parent = {},
    Context = MyContext
  > = Resolver<R, Parent, Context, OperatorDetailsArgs>;
  export interface OperatorDetailsArgs {
    id: string;
  }

  export type PaymentMethodsResolver<
    R = Maybe<(Maybe<string>)[]>,
    Parent = {},
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type PricingMethodsResolver<
    R = Maybe<(Maybe<string>)[]>,
    Parent = {},
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type RegionsResolver<
    R = (Maybe<Region>)[],
    Parent = {},
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type RegionsWithHubsResolver<
    R = RegionWithHubs[],
    Parent = {},
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type ServicesResolver<
    R = Maybe<(Maybe<string>)[]>,
    Parent = {},
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type UsagesResolver<
    R = Maybe<string[]>,
    Parent = {},
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type UsersResolver<
    R = Maybe<(Maybe<User>)[]>,
    Parent = {},
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type UtilizationsResolver<
    R = Maybe<(Maybe<Utilization>)[]>,
    Parent = {},
    Context = MyContext
  > = Resolver<R, Parent, Context>;
}

export namespace ContactResponseResolvers {
  export interface Resolvers<
    Context = MyContext,
    TypeParent = ContactResponse
  > {
    results?: ResultsResolver<Contact[], TypeParent, Context>;
  }

  export type ResultsResolver<
    R = Contact[],
    Parent = ContactResponse,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
}

export namespace ContactResolvers {
  export interface Resolvers<Context = MyContext, TypeParent = Contact> {
    id?: IdResolver<number, TypeParent, Context>;

    name?: NameResolver<LocalizedObject, TypeParent, Context>;

    operatorId?: OperatorIdResolver<Maybe<number>, TypeParent, Context>;

    phone?: PhoneResolver<Maybe<string>, TypeParent, Context>;

    email?: EmailResolver<Maybe<string>, TypeParent, Context>;

    address?: AddressResolver<Maybe<Address>, TypeParent, Context>;

    openingHours?: OpeningHoursResolver<
      Maybe<LocalizedObject>,
      TypeParent,
      Context
    >;

    info?: InfoResolver<Maybe<LocalizedObject>, TypeParent, Context>;

    hasMore?: HasMoreResolver<Maybe<boolean>, TypeParent, Context>;
  }

  export type IdResolver<
    R = number,
    Parent = Contact,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type NameResolver<
    R = LocalizedObject,
    Parent = Contact,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type OperatorIdResolver<
    R = Maybe<number>,
    Parent = Contact,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type PhoneResolver<
    R = Maybe<string>,
    Parent = Contact,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type EmailResolver<
    R = Maybe<string>,
    Parent = Contact,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type AddressResolver<
    R = Maybe<Address>,
    Parent = Contact,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type OpeningHoursResolver<
    R = Maybe<LocalizedObject>,
    Parent = Contact,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type InfoResolver<
    R = Maybe<LocalizedObject>,
    Parent = Contact,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type HasMoreResolver<
    R = Maybe<boolean>,
    Parent = Contact,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
}

export namespace LocalizedObjectResolvers {
  export interface Resolvers<
    Context = MyContext,
    TypeParent = LocalizedObject
  > {
    fi?: FiResolver<Maybe<string>, TypeParent, Context>;

    sv?: SvResolver<Maybe<string>, TypeParent, Context>;

    en?: EnResolver<Maybe<string>, TypeParent, Context>;
  }

  export type FiResolver<
    R = Maybe<string>,
    Parent = LocalizedObject,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type SvResolver<
    R = Maybe<string>,
    Parent = LocalizedObject,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type EnResolver<
    R = Maybe<string>,
    Parent = LocalizedObject,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
}

export namespace AddressResolvers {
  export interface Resolvers<Context = MyContext, TypeParent = Address> {
    streetAddress?: StreetAddressResolver<
      Maybe<LocalizedObject>,
      TypeParent,
      Context
    >;

    postalCode?: PostalCodeResolver<Maybe<string>, TypeParent, Context>;

    city?: CityResolver<Maybe<LocalizedObject>, TypeParent, Context>;
  }

  export type StreetAddressResolver<
    R = Maybe<LocalizedObject>,
    Parent = Address,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type PostalCodeResolver<
    R = Maybe<string>,
    Parent = Address,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type CityResolver<
    R = Maybe<LocalizedObject>,
    Parent = Address,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
}

export namespace FacilityResponseResolvers {
  export interface Resolvers<
    Context = MyContext,
    TypeParent = FacilityResponse
  > {
    cursor?: CursorResolver<Maybe<string>, TypeParent, Context>;

    totalCount?: TotalCountResolver<Maybe<number>, TypeParent, Context>;

    hasMore?: HasMoreResolver<boolean, TypeParent, Context>;

    facilities?: FacilitiesResolver<Facility[], TypeParent, Context>;
  }

  export type CursorResolver<
    R = Maybe<string>,
    Parent = FacilityResponse,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type TotalCountResolver<
    R = Maybe<number>,
    Parent = FacilityResponse,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type HasMoreResolver<
    R = boolean,
    Parent = FacilityResponse,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type FacilitiesResolver<
    R = Facility[],
    Parent = FacilityResponse,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
}

export namespace FacilityResolvers {
  export interface Resolvers<Context = MyContext, TypeParent = Facility> {
    id?: IdResolver<number, TypeParent, Context>;

    name?: NameResolver<Maybe<LocalizedObject>, TypeParent, Context>;

    status?: StatusResolver<Maybe<string>, TypeParent, Context>;

    landings?: LandingsResolver<Maybe<number>, TypeParent, Context>;

    type?: TypeResolver<Maybe<string>, TypeParent, Context>;

    reuse_count?: ReuseCountResolver<Maybe<number>, TypeParent, Context>;

    operatorId?: OperatorIdResolver<Maybe<number>, TypeParent, Context>;

    builtCapacity?: BuiltCapacityResolver<Maybe<Capacity>, TypeParent, Context>;

    usages?: UsagesResolver<Maybe<(Maybe<string>)[]>, TypeParent, Context>;

    location?: LocationResolver<Maybe<Location>, TypeParent, Context>;
  }

  export type IdResolver<
    R = number,
    Parent = Facility,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type NameResolver<
    R = Maybe<LocalizedObject>,
    Parent = Facility,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type StatusResolver<
    R = Maybe<string>,
    Parent = Facility,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type LandingsResolver<
    R = Maybe<number>,
    Parent = Facility,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type TypeResolver<
    R = Maybe<string>,
    Parent = Facility,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type ReuseCountResolver<
    R = Maybe<number>,
    Parent = Facility,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type OperatorIdResolver<
    R = Maybe<number>,
    Parent = Facility,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type BuiltCapacityResolver<
    R = Maybe<Capacity>,
    Parent = Facility,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type UsagesResolver<
    R = Maybe<(Maybe<string>)[]>,
    Parent = Facility,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type LocationResolver<
    R = Maybe<Location>,
    Parent = Facility,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
}

export namespace CapacityResolvers {
  export interface Resolvers<Context = MyContext, TypeParent = Capacity> {
    CAR?: CarResolver<Maybe<number>, TypeParent, Context>;

    ELECTRIC_CAR?: ElectricCarResolver<Maybe<number>, TypeParent, Context>;

    BICYCLE_SECURE_SPACE?: BicycleSecureSpaceResolver<
      Maybe<number>,
      TypeParent,
      Context
    >;

    DISABLED?: DisabledResolver<Maybe<number>, TypeParent, Context>;

    MOTORCYCLE?: MotorcycleResolver<Maybe<number>, TypeParent, Context>;

    BICYCLE?: BicycleResolver<Maybe<number>, TypeParent, Context>;
  }

  export type CarResolver<
    R = Maybe<number>,
    Parent = Capacity,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type ElectricCarResolver<
    R = Maybe<number>,
    Parent = Capacity,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type BicycleSecureSpaceResolver<
    R = Maybe<number>,
    Parent = Capacity,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type DisabledResolver<
    R = Maybe<number>,
    Parent = Capacity,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type MotorcycleResolver<
    R = Maybe<number>,
    Parent = Capacity,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type BicycleResolver<
    R = Maybe<number>,
    Parent = Capacity,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
}

export namespace LocationResolvers {
  export interface Resolvers<Context = MyContext, TypeParent = Location> {
    crs?: CrsResolver<Maybe<Crs>, TypeParent, Context>;

    bbox?: BboxResolver<Maybe<(Maybe<number>)[]>, TypeParent, Context>;

    type?: TypeResolver<Maybe<string>, TypeParent, Context>;

    coordinates?: CoordinatesResolver<
      Maybe<(Maybe<number>)[][][]>,
      TypeParent,
      Context
    >;
  }

  export type CrsResolver<
    R = Maybe<Crs>,
    Parent = Location,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type BboxResolver<
    R = Maybe<(Maybe<number>)[]>,
    Parent = Location,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type TypeResolver<
    R = Maybe<string>,
    Parent = Location,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type CoordinatesResolver<
    R = Maybe<(Maybe<number>)[][][]>,
    Parent = Location,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
}

export namespace CrsResolvers {
  export interface Resolvers<Context = MyContext, TypeParent = Crs> {
    type?: TypeResolver<Maybe<string>, TypeParent, Context>;

    properties?: PropertiesResolver<Maybe<Name>, TypeParent, Context>;

    bbox?: BboxResolver<Maybe<(Maybe<number>)[]>, TypeParent, Context>;
  }

  export type TypeResolver<
    R = Maybe<string>,
    Parent = Crs,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type PropertiesResolver<
    R = Maybe<Name>,
    Parent = Crs,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type BboxResolver<
    R = Maybe<(Maybe<number>)[]>,
    Parent = Crs,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
}

export namespace NameResolvers {
  export interface Resolvers<Context = MyContext, TypeParent = Name> {
    name?: NameResolver<Maybe<string>, TypeParent, Context>;
  }

  export type NameResolver<
    R = Maybe<string>,
    Parent = Name,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
}

export namespace FacilityDetailsResolvers {
  export interface Resolvers<
    Context = MyContext,
    TypeParent = FacilityDetails
  > {
    id?: IdResolver<number, TypeParent, Context>;

    name?: NameResolver<LocalizedObject, TypeParent, Context>;

    location?: LocationResolver<Maybe<Location>, TypeParent, Context>;

    operatorId?: OperatorIdResolver<number, TypeParent, Context>;

    status?: StatusResolver<Maybe<string>, TypeParent, Context>;

    pricingMethod?: PricingMethodResolver<string, TypeParent, Context>;

    statusDescription?: StatusDescriptionResolver<
      Maybe<LocalizedObject>,
      TypeParent,
      Context
    >;

    builtCapacity?: BuiltCapacityResolver<Maybe<Capacity>, TypeParent, Context>;

    usages?: UsagesResolver<Maybe<(Maybe<string>)[]>, TypeParent, Context>;

    pricing?: PricingResolver<Pricing[], TypeParent, Context>;

    unavailableCapacities?: UnavailableCapacitiesResolver<
      UnavailableCapacity[],
      TypeParent,
      Context
    >;

    aliases?: AliasesResolver<string[], TypeParent, Context>;

    ports?: PortsResolver<Port[], TypeParent, Context>;

    services?: ServicesResolver<string[], TypeParent, Context>;

    contacts?: ContactsResolver<ContactOptions, TypeParent, Context>;

    paymentInfo?: PaymentInfoResolver<PaymentInfo, TypeParent, Context>;

    openingHours?: OpeningHoursResolver<OpeningHour, TypeParent, Context>;
  }

  export type IdResolver<
    R = number,
    Parent = FacilityDetails,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type NameResolver<
    R = LocalizedObject,
    Parent = FacilityDetails,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type LocationResolver<
    R = Maybe<Location>,
    Parent = FacilityDetails,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type OperatorIdResolver<
    R = number,
    Parent = FacilityDetails,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type StatusResolver<
    R = Maybe<string>,
    Parent = FacilityDetails,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type PricingMethodResolver<
    R = string,
    Parent = FacilityDetails,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type StatusDescriptionResolver<
    R = Maybe<LocalizedObject>,
    Parent = FacilityDetails,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type BuiltCapacityResolver<
    R = Maybe<Capacity>,
    Parent = FacilityDetails,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type UsagesResolver<
    R = Maybe<(Maybe<string>)[]>,
    Parent = FacilityDetails,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type PricingResolver<
    R = Pricing[],
    Parent = FacilityDetails,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type UnavailableCapacitiesResolver<
    R = UnavailableCapacity[],
    Parent = FacilityDetails,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type AliasesResolver<
    R = string[],
    Parent = FacilityDetails,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type PortsResolver<
    R = Port[],
    Parent = FacilityDetails,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type ServicesResolver<
    R = string[],
    Parent = FacilityDetails,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type ContactsResolver<
    R = ContactOptions,
    Parent = FacilityDetails,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type PaymentInfoResolver<
    R = PaymentInfo,
    Parent = FacilityDetails,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type OpeningHoursResolver<
    R = OpeningHour,
    Parent = FacilityDetails,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
}

export namespace PricingResolvers {
  export interface Resolvers<Context = MyContext, TypeParent = Pricing> {
    usage?: UsageResolver<string, TypeParent, Context>;

    capacityType?: CapacityTypeResolver<string, TypeParent, Context>;

    maxCapacity?: MaxCapacityResolver<number, TypeParent, Context>;

    dayType?: DayTypeResolver<string, TypeParent, Context>;

    time?: TimeResolver<PricingTimeRange, TypeParent, Context>;

    price?: PriceResolver<Maybe<LocalizedObject>, TypeParent, Context>;
  }

  export type UsageResolver<
    R = string,
    Parent = Pricing,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type CapacityTypeResolver<
    R = string,
    Parent = Pricing,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type MaxCapacityResolver<
    R = number,
    Parent = Pricing,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type DayTypeResolver<
    R = string,
    Parent = Pricing,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type TimeResolver<
    R = PricingTimeRange,
    Parent = Pricing,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type PriceResolver<
    R = Maybe<LocalizedObject>,
    Parent = Pricing,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
}

export namespace PricingTimeRangeResolvers {
  export interface Resolvers<
    Context = MyContext,
    TypeParent = PricingTimeRange
  > {
    from?: FromResolver<string, TypeParent, Context>;

    until?: UntilResolver<string, TypeParent, Context>;
  }

  export type FromResolver<
    R = string,
    Parent = PricingTimeRange,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type UntilResolver<
    R = string,
    Parent = PricingTimeRange,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
}

export namespace UnavailableCapacityResolvers {
  export interface Resolvers<
    Context = MyContext,
    TypeParent = UnavailableCapacity
  > {
    capacityType?: CapacityTypeResolver<string, TypeParent, Context>;

    usage?: UsageResolver<string, TypeParent, Context>;

    capacity?: CapacityResolver<number, TypeParent, Context>;
  }

  export type CapacityTypeResolver<
    R = string,
    Parent = UnavailableCapacity,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type UsageResolver<
    R = string,
    Parent = UnavailableCapacity,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type CapacityResolver<
    R = number,
    Parent = UnavailableCapacity,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
}

export namespace PortResolvers {
  export interface Resolvers<Context = MyContext, TypeParent = Port> {
    location?: LocationResolver<Maybe<PortLocation>, TypeParent, Context>;

    entry?: EntryResolver<Maybe<boolean>, TypeParent, Context>;

    exit?: ExitResolver<Maybe<boolean>, TypeParent, Context>;

    pedestrian?: PedestrianResolver<Maybe<boolean>, TypeParent, Context>;

    bicycle?: BicycleResolver<Maybe<boolean>, TypeParent, Context>;

    address?: AddressResolver<Maybe<Address>, TypeParent, Context>;

    info?: InfoResolver<Maybe<LocalizedObject>, TypeParent, Context>;
  }

  export type LocationResolver<
    R = Maybe<PortLocation>,
    Parent = Port,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type EntryResolver<
    R = Maybe<boolean>,
    Parent = Port,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type ExitResolver<
    R = Maybe<boolean>,
    Parent = Port,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type PedestrianResolver<
    R = Maybe<boolean>,
    Parent = Port,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type BicycleResolver<
    R = Maybe<boolean>,
    Parent = Port,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type AddressResolver<
    R = Maybe<Address>,
    Parent = Port,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type InfoResolver<
    R = Maybe<LocalizedObject>,
    Parent = Port,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
}

export namespace PortLocationResolvers {
  export interface Resolvers<Context = MyContext, TypeParent = PortLocation> {
    crs?: CrsResolver<Maybe<Crs>, TypeParent, Context>;

    bbox?: BboxResolver<Maybe<(Maybe<number>)[]>, TypeParent, Context>;

    type?: TypeResolver<Maybe<string>, TypeParent, Context>;

    coordinates?: CoordinatesResolver<(Maybe<number>)[], TypeParent, Context>;
  }

  export type CrsResolver<
    R = Maybe<Crs>,
    Parent = PortLocation,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type BboxResolver<
    R = Maybe<(Maybe<number>)[]>,
    Parent = PortLocation,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type TypeResolver<
    R = Maybe<string>,
    Parent = PortLocation,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type CoordinatesResolver<
    R = (Maybe<number>)[],
    Parent = PortLocation,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
}

export namespace ContactOptionsResolvers {
  export interface Resolvers<Context = MyContext, TypeParent = ContactOptions> {
    emergency?: EmergencyResolver<Maybe<number>, TypeParent, Context>;

    operator?: OperatorResolver<Maybe<number>, TypeParent, Context>;

    service?: ServiceResolver<Maybe<number>, TypeParent, Context>;
  }

  export type EmergencyResolver<
    R = Maybe<number>,
    Parent = ContactOptions,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type OperatorResolver<
    R = Maybe<number>,
    Parent = ContactOptions,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type ServiceResolver<
    R = Maybe<number>,
    Parent = ContactOptions,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
}

export namespace PaymentInfoResolvers {
  export interface Resolvers<Context = MyContext, TypeParent = PaymentInfo> {
    detail?: DetailResolver<Maybe<LocalizedObject>, TypeParent, Context>;

    url?: UrlResolver<Maybe<LocalizedObject>, TypeParent, Context>;

    paymentMethods?: PaymentMethodsResolver<string[], TypeParent, Context>;
  }

  export type DetailResolver<
    R = Maybe<LocalizedObject>,
    Parent = PaymentInfo,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type UrlResolver<
    R = Maybe<LocalizedObject>,
    Parent = PaymentInfo,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type PaymentMethodsResolver<
    R = string[],
    Parent = PaymentInfo,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
}

export namespace OpeningHourResolvers {
  export interface Resolvers<Context = MyContext, TypeParent = OpeningHour> {
    openNow?: OpenNowResolver<Maybe<boolean>, TypeParent, Context>;

    byDayType?: ByDayTypeResolver<Maybe<Day>, TypeParent, Context>;

    info?: InfoResolver<Maybe<LocalizedObject>, TypeParent, Context>;

    url?: UrlResolver<Maybe<LocalizedObject>, TypeParent, Context>;
  }

  export type OpenNowResolver<
    R = Maybe<boolean>,
    Parent = OpeningHour,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type ByDayTypeResolver<
    R = Maybe<Day>,
    Parent = OpeningHour,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type InfoResolver<
    R = Maybe<LocalizedObject>,
    Parent = OpeningHour,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type UrlResolver<
    R = Maybe<LocalizedObject>,
    Parent = OpeningHour,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
}

export namespace DayResolvers {
  export interface Resolvers<Context = MyContext, TypeParent = Day> {
    BUSINESS_DAY?: BusinessDayResolver<Maybe<FromUntil>, TypeParent, Context>;

    SATURDAY?: SaturdayResolver<Maybe<FromUntil>, TypeParent, Context>;

    SUNDAY?: SundayResolver<Maybe<FromUntil>, TypeParent, Context>;
  }

  export type BusinessDayResolver<
    R = Maybe<FromUntil>,
    Parent = Day,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type SaturdayResolver<
    R = Maybe<FromUntil>,
    Parent = Day,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type SundayResolver<
    R = Maybe<FromUntil>,
    Parent = Day,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
}

export namespace FromUntilResolvers {
  export interface Resolvers<Context = MyContext, TypeParent = FromUntil> {
    from?: FromResolver<Maybe<string>, TypeParent, Context>;

    until?: UntilResolver<Maybe<string>, TypeParent, Context>;
  }

  export type FromResolver<
    R = Maybe<string>,
    Parent = FromUntil,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type UntilResolver<
    R = Maybe<string>,
    Parent = FromUntil,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
}

export namespace FacilitiesSummaryResolvers {
  export interface Resolvers<
    Context = MyContext,
    TypeParent = FacilitiesSummary
  > {
    facilityCount?: FacilityCountResolver<Maybe<number>, TypeParent, Context>;

    capacities?: CapacitiesResolver<Maybe<Capacity>, TypeParent, Context>;
  }

  export type FacilityCountResolver<
    R = Maybe<number>,
    Parent = FacilitiesSummary,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type CapacitiesResolver<
    R = Maybe<Capacity>,
    Parent = FacilitiesSummary,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
}

export namespace UtilizationResolvers {
  export interface Resolvers<Context = MyContext, TypeParent = Utilization> {
    facilityId?: FacilityIdResolver<number, TypeParent, Context>;

    capacityType?: CapacityTypeResolver<string, TypeParent, Context>;

    usage?: UsageResolver<string, TypeParent, Context>;

    timestamp?: TimestampResolver<Maybe<string>, TypeParent, Context>;

    spacesAvailable?: SpacesAvailableResolver<
      Maybe<number>,
      TypeParent,
      Context
    >;

    capacity?: CapacityResolver<Maybe<number>, TypeParent, Context>;

    openNow?: OpenNowResolver<Maybe<boolean>, TypeParent, Context>;
  }

  export type FacilityIdResolver<
    R = number,
    Parent = Utilization,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type CapacityTypeResolver<
    R = string,
    Parent = Utilization,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type UsageResolver<
    R = string,
    Parent = Utilization,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type TimestampResolver<
    R = Maybe<string>,
    Parent = Utilization,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type SpacesAvailableResolver<
    R = Maybe<number>,
    Parent = Utilization,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type CapacityResolver<
    R = Maybe<number>,
    Parent = Utilization,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type OpenNowResolver<
    R = Maybe<boolean>,
    Parent = Utilization,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
}

export namespace PredictionResolvers {
  export interface Resolvers<Context = MyContext, TypeParent = Prediction> {
    capacityType?: CapacityTypeResolver<Maybe<string>, TypeParent, Context>;

    usage?: UsageResolver<Maybe<string>, TypeParent, Context>;

    timestamp?: TimestampResolver<Maybe<string>, TypeParent, Context>;

    spacesAvailable?: SpacesAvailableResolver<
      Maybe<number>,
      TypeParent,
      Context
    >;

    facilityId?: FacilityIdResolver<Maybe<number>, TypeParent, Context>;

    hubId?: HubIdResolver<Maybe<number>, TypeParent, Context>;
  }

  export type CapacityTypeResolver<
    R = Maybe<string>,
    Parent = Prediction,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type UsageResolver<
    R = Maybe<string>,
    Parent = Prediction,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type TimestampResolver<
    R = Maybe<string>,
    Parent = Prediction,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type SpacesAvailableResolver<
    R = Maybe<number>,
    Parent = Prediction,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type FacilityIdResolver<
    R = Maybe<number>,
    Parent = Prediction,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type HubIdResolver<
    R = Maybe<number>,
    Parent = Prediction,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
}

export namespace FacilitiesGeoJsonResponseResolvers {
  export interface Resolvers<
    Context = MyContext,
    TypeParent = FacilitiesGeoJsonResponse
  > {
    hasMore?: HasMoreResolver<Maybe<boolean>, TypeParent, Context>;

    features?: FeaturesResolver<
      Maybe<(Maybe<FacilityGeoJson>)[]>,
      TypeParent,
      Context
    >;

    type?: TypeResolver<Maybe<string>, TypeParent, Context>;
  }

  export type HasMoreResolver<
    R = Maybe<boolean>,
    Parent = FacilitiesGeoJsonResponse,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type FeaturesResolver<
    R = Maybe<(Maybe<FacilityGeoJson>)[]>,
    Parent = FacilitiesGeoJsonResponse,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type TypeResolver<
    R = Maybe<string>,
    Parent = FacilitiesGeoJsonResponse,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
}

export namespace FacilityGeoJsonResolvers {
  export interface Resolvers<
    Context = MyContext,
    TypeParent = FacilityGeoJson
  > {
    id?: IdResolver<Maybe<number>, TypeParent, Context>;

    geometry?: GeometryResolver<Maybe<Geometry>, TypeParent, Context>;

    properties?: PropertiesResolver<Maybe<Properties>, TypeParent, Context>;

    type?: TypeResolver<Maybe<string>, TypeParent, Context>;
  }

  export type IdResolver<
    R = Maybe<number>,
    Parent = FacilityGeoJson,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type GeometryResolver<
    R = Maybe<Geometry>,
    Parent = FacilityGeoJson,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type PropertiesResolver<
    R = Maybe<Properties>,
    Parent = FacilityGeoJson,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type TypeResolver<
    R = Maybe<string>,
    Parent = FacilityGeoJson,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
}

export namespace GeometryResolvers {
  export interface Resolvers<Context = MyContext, TypeParent = Geometry> {
    crs?: CrsResolver<Maybe<Crs>, TypeParent, Context>;

    bbox?: BboxResolver<Maybe<(Maybe<number>)[]>, TypeParent, Context>;

    type?: TypeResolver<Maybe<string>, TypeParent, Context>;
  }

  export type CrsResolver<
    R = Maybe<Crs>,
    Parent = Geometry,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type BboxResolver<
    R = Maybe<(Maybe<number>)[]>,
    Parent = Geometry,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type TypeResolver<
    R = Maybe<string>,
    Parent = Geometry,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
}

export namespace PropertiesResolvers {
  export interface Resolvers<Context = MyContext, TypeParent = Properties> {
    name?: NameResolver<Maybe<LocalizedObject>, TypeParent, Context>;

    status?: StatusResolver<Maybe<string>, TypeParent, Context>;

    operatorId?: OperatorIdResolver<Maybe<number>, TypeParent, Context>;

    builtCapacity?: BuiltCapacityResolver<Maybe<Capacity>, TypeParent, Context>;

    usages?: UsagesResolver<Maybe<(Maybe<string>)[]>, TypeParent, Context>;

    facilityIds?: FacilityIdsResolver<
      Maybe<(Maybe<number>)[]>,
      TypeParent,
      Context
    >;
  }

  export type NameResolver<
    R = Maybe<LocalizedObject>,
    Parent = Properties,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type StatusResolver<
    R = Maybe<string>,
    Parent = Properties,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type OperatorIdResolver<
    R = Maybe<number>,
    Parent = Properties,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type BuiltCapacityResolver<
    R = Maybe<Capacity>,
    Parent = Properties,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type UsagesResolver<
    R = Maybe<(Maybe<string>)[]>,
    Parent = Properties,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type FacilityIdsResolver<
    R = Maybe<(Maybe<number>)[]>,
    Parent = Properties,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
}

export namespace HubResponseResolvers {
  export interface Resolvers<Context = MyContext, TypeParent = HubResponse> {
    cursor?: CursorResolver<Maybe<string>, TypeParent, Context>;

    totalCount?: TotalCountResolver<Maybe<number>, TypeParent, Context>;

    hasMore?: HasMoreResolver<boolean, TypeParent, Context>;

    hubs?: HubsResolver<Hub[], TypeParent, Context>;
  }

  export type CursorResolver<
    R = Maybe<string>,
    Parent = HubResponse,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type TotalCountResolver<
    R = Maybe<number>,
    Parent = HubResponse,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type HasMoreResolver<
    R = boolean,
    Parent = HubResponse,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type HubsResolver<
    R = Hub[],
    Parent = HubResponse,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
}

export namespace HubResolvers {
  export interface Resolvers<Context = MyContext, TypeParent = Hub> {
    id?: IdResolver<number, TypeParent, Context>;

    name?: NameResolver<Maybe<LocalizedObject>, TypeParent, Context>;

    location?: LocationResolver<Maybe<Location>, TypeParent, Context>;

    facilityIds?: FacilityIdsResolver<number[], TypeParent, Context>;

    address?: AddressResolver<Maybe<Address>, TypeParent, Context>;

    hasMore?: HasMoreResolver<Maybe<boolean>, TypeParent, Context>;
  }

  export type IdResolver<
    R = number,
    Parent = Hub,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type NameResolver<
    R = Maybe<LocalizedObject>,
    Parent = Hub,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type LocationResolver<
    R = Maybe<Location>,
    Parent = Hub,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type FacilityIdsResolver<
    R = number[],
    Parent = Hub,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type AddressResolver<
    R = Maybe<Address>,
    Parent = Hub,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type HasMoreResolver<
    R = Maybe<boolean>,
    Parent = Hub,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
}

export namespace HubDetailsResolvers {
  export interface Resolvers<Context = MyContext, TypeParent = HubDetails> {
    id?: IdResolver<Maybe<number>, TypeParent, Context>;

    name?: NameResolver<Maybe<LocalizedObject>, TypeParent, Context>;

    location?: LocationResolver<Maybe<HubLocation>, TypeParent, Context>;

    facilityIds?: FacilityIdsResolver<number[], TypeParent, Context>;

    address?: AddressResolver<Maybe<Address>, TypeParent, Context>;
  }

  export type IdResolver<
    R = Maybe<number>,
    Parent = HubDetails,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type NameResolver<
    R = Maybe<LocalizedObject>,
    Parent = HubDetails,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type LocationResolver<
    R = Maybe<HubLocation>,
    Parent = HubDetails,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type FacilityIdsResolver<
    R = number[],
    Parent = HubDetails,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type AddressResolver<
    R = Maybe<Address>,
    Parent = HubDetails,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
}

export namespace HubLocationResolvers {
  export interface Resolvers<Context = MyContext, TypeParent = HubLocation> {
    crs?: CrsResolver<Maybe<Crs>, TypeParent, Context>;

    bbox?: BboxResolver<Maybe<(Maybe<number>)[]>, TypeParent, Context>;

    type?: TypeResolver<Maybe<string>, TypeParent, Context>;

    coordinates?: CoordinatesResolver<
      Maybe<(Maybe<number>)[]>,
      TypeParent,
      Context
    >;
  }

  export type CrsResolver<
    R = Maybe<Crs>,
    Parent = HubLocation,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type BboxResolver<
    R = Maybe<(Maybe<number>)[]>,
    Parent = HubLocation,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type TypeResolver<
    R = Maybe<string>,
    Parent = HubLocation,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type CoordinatesResolver<
    R = Maybe<(Maybe<number>)[]>,
    Parent = HubLocation,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
}

export namespace HubsSummaryResolvers {
  export interface Resolvers<Context = MyContext, TypeParent = HubsSummary> {
    hubCount?: HubCountResolver<Maybe<number>, TypeParent, Context>;

    capacities?: CapacitiesResolver<Maybe<Capacity>, TypeParent, Context>;
  }

  export type HubCountResolver<
    R = Maybe<number>,
    Parent = HubsSummary,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type CapacitiesResolver<
    R = Maybe<Capacity>,
    Parent = HubsSummary,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
}

export namespace HubsGeoJsonResponseResolvers {
  export interface Resolvers<
    Context = MyContext,
    TypeParent = HubsGeoJsonResponse
  > {
    hasMore?: HasMoreResolver<Maybe<boolean>, TypeParent, Context>;

    features?: FeaturesResolver<
      Maybe<(Maybe<HubGeoJson>)[]>,
      TypeParent,
      Context
    >;

    type?: TypeResolver<Maybe<string>, TypeParent, Context>;
  }

  export type HasMoreResolver<
    R = Maybe<boolean>,
    Parent = HubsGeoJsonResponse,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type FeaturesResolver<
    R = Maybe<(Maybe<HubGeoJson>)[]>,
    Parent = HubsGeoJsonResponse,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type TypeResolver<
    R = Maybe<string>,
    Parent = HubsGeoJsonResponse,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
}

export namespace HubGeoJsonResolvers {
  export interface Resolvers<Context = MyContext, TypeParent = HubGeoJson> {
    id?: IdResolver<Maybe<number>, TypeParent, Context>;

    geometry?: GeometryResolver<Maybe<Geometry>, TypeParent, Context>;

    properties?: PropertiesResolver<Maybe<Properties>, TypeParent, Context>;

    type?: TypeResolver<Maybe<string>, TypeParent, Context>;
  }

  export type IdResolver<
    R = Maybe<number>,
    Parent = HubGeoJson,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type GeometryResolver<
    R = Maybe<Geometry>,
    Parent = HubGeoJson,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type PropertiesResolver<
    R = Maybe<Properties>,
    Parent = HubGeoJson,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type TypeResolver<
    R = Maybe<string>,
    Parent = HubGeoJson,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
}

export namespace OperatorResponseResolvers {
  export interface Resolvers<
    Context = MyContext,
    TypeParent = OperatorResponse
  > {
    hasMore?: HasMoreResolver<Maybe<boolean>, TypeParent, Context>;

    results?: ResultsResolver<Operator[], TypeParent, Context>;
  }

  export type HasMoreResolver<
    R = Maybe<boolean>,
    Parent = OperatorResponse,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type ResultsResolver<
    R = Operator[],
    Parent = OperatorResponse,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
}

export namespace OperatorResolvers {
  export interface Resolvers<Context = MyContext, TypeParent = Operator> {
    id?: IdResolver<string, TypeParent, Context>;

    name?: NameResolver<LocalizedObject, TypeParent, Context>;
  }

  export type IdResolver<
    R = string,
    Parent = Operator,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type NameResolver<
    R = LocalizedObject,
    Parent = Operator,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
}

export namespace RegionResolvers {
  export interface Resolvers<Context = MyContext, TypeParent = Region> {
    id?: IdResolver<string, TypeParent, Context>;

    name?: NameResolver<Maybe<LocalizedObject>, TypeParent, Context>;
  }

  export type IdResolver<
    R = string,
    Parent = Region,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type NameResolver<
    R = Maybe<LocalizedObject>,
    Parent = Region,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
}

export namespace RegionWithHubsResolvers {
  export interface Resolvers<Context = MyContext, TypeParent = RegionWithHubs> {
    id?: IdResolver<string, TypeParent, Context>;

    name?: NameResolver<Maybe<LocalizedObject>, TypeParent, Context>;

    hubIds?: HubIdsResolver<string[], TypeParent, Context>;
  }

  export type IdResolver<
    R = string,
    Parent = RegionWithHubs,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type NameResolver<
    R = Maybe<LocalizedObject>,
    Parent = RegionWithHubs,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type HubIdsResolver<
    R = string[],
    Parent = RegionWithHubs,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
}

export namespace UserResolvers {
  export interface Resolvers<Context = MyContext, TypeParent = User> {
    id?: IdResolver<Maybe<number>, TypeParent, Context>;

    username?: UsernameResolver<Maybe<string>, TypeParent, Context>;

    role?: RoleResolver<Maybe<string>, TypeParent, Context>;

    operatorId?: OperatorIdResolver<Maybe<number>, TypeParent, Context>;
  }

  export type IdResolver<
    R = Maybe<number>,
    Parent = User,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type UsernameResolver<
    R = Maybe<string>,
    Parent = User,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type RoleResolver<
    R = Maybe<string>,
    Parent = User,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type OperatorIdResolver<
    R = Maybe<number>,
    Parent = User,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
}

export namespace MutationResolvers {
  export interface Resolvers<Context = MyContext, TypeParent = {}> {
    updateUtilization?: UpdateUtilizationResolver<
      Maybe<(Maybe<UtilizationMutationResponse>)[]>,
      TypeParent,
      Context
    >;

    _empty?: _EmptyResolver<Maybe<string>, TypeParent, Context>;

    login?: LoginResolver<Maybe<Login>, TypeParent, Context>;

    facilityUsageReport?: FacilityUsageReportResolver<
      Maybe<string>,
      TypeParent,
      Context
    >;

    maxUtilizationReport?: MaxUtilizationReportResolver<
      Maybe<string>,
      TypeParent,
      Context
    >;

    hubsAndFacilitiesReport?: HubsAndFacilitiesReportResolver<
      Maybe<string>,
      TypeParent,
      Context
    >;

    requestLogReport?: RequestLogReportResolver<
      Maybe<string>,
      TypeParent,
      Context
    >;

    createUser?: CreateUserResolver<Maybe<User>, TypeParent, Context>;

    changePassword?: ChangePasswordResolver<Maybe<string>, TypeParent, Context>;

    updateToken?: UpdateTokenResolver<Maybe<string>, TypeParent, Context>;

    deleteUser?: DeleteUserResolver<Maybe<string>, TypeParent, Context>;
  }

  export type UpdateUtilizationResolver<
    R = Maybe<(Maybe<UtilizationMutationResponse>)[]>,
    Parent = {},
    Context = MyContext
  > = Resolver<R, Parent, Context, UpdateUtilizationArgs>;
  export interface UpdateUtilizationArgs {
    facilityId: string;

    newUtilizations?: Maybe<(Maybe<UtilizationInput>)[]>;
  }

  export type _EmptyResolver<
    R = Maybe<string>,
    Parent = {},
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type LoginResolver<
    R = Maybe<Login>,
    Parent = {},
    Context = MyContext
  > = Resolver<R, Parent, Context, LoginArgs>;
  export interface LoginArgs {
    password?: Maybe<string>;

    username?: Maybe<string>;
  }

  export type FacilityUsageReportResolver<
    R = Maybe<string>,
    Parent = {},
    Context = MyContext
  > = Resolver<R, Parent, Context, FacilityUsageReportArgs>;
  export interface FacilityUsageReportArgs {
    capacityTypes?: Maybe<string[]>;

    endDate?: Maybe<number[]>;

    facilities?: Maybe<number[]>;

    hubs?: Maybe<number[]>;

    interval?: Maybe<number>;

    operators?: Maybe<number[]>;

    regions?: Maybe<number[]>;

    startDate?: Maybe<number[]>;

    usages?: Maybe<number[]>;
  }

  export type MaxUtilizationReportResolver<
    R = Maybe<string>,
    Parent = {},
    Context = MyContext
  > = Resolver<R, Parent, Context, MaxUtilizationReportArgs>;
  export interface MaxUtilizationReportArgs {
    capacityTypes?: Maybe<string[]>;

    endDate?: Maybe<number[]>;

    facilities?: Maybe<number[]>;

    hubs?: Maybe<number[]>;

    operators?: Maybe<number[]>;

    regions?: Maybe<number[]>;

    startDate?: Maybe<number[]>;

    usages?: Maybe<number[]>;
  }

  export type HubsAndFacilitiesReportResolver<
    R = Maybe<string>,
    Parent = {},
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type RequestLogReportResolver<
    R = Maybe<string>,
    Parent = {},
    Context = MyContext
  > = Resolver<R, Parent, Context, RequestLogReportArgs>;
  export interface RequestLogReportArgs {
    endDate?: Maybe<number[]>;

    requestLogInterval: string;

    startDate?: Maybe<number[]>;
  }

  export type CreateUserResolver<
    R = Maybe<User>,
    Parent = {},
    Context = MyContext
  > = Resolver<R, Parent, Context, CreateUserArgs>;
  export interface CreateUserArgs {
    user?: Maybe<UserInput>;
  }

  export type ChangePasswordResolver<
    R = Maybe<string>,
    Parent = {},
    Context = MyContext
  > = Resolver<R, Parent, Context, ChangePasswordArgs>;
  export interface ChangePasswordArgs {
    id?: Maybe<number>;

    newPassword?: Maybe<string>;
  }

  export type UpdateTokenResolver<
    R = Maybe<string>,
    Parent = {},
    Context = MyContext
  > = Resolver<R, Parent, Context, UpdateTokenArgs>;
  export interface UpdateTokenArgs {
    id?: Maybe<number>;
  }

  export type DeleteUserResolver<
    R = Maybe<string>,
    Parent = {},
    Context = MyContext
  > = Resolver<R, Parent, Context, DeleteUserArgs>;
  export interface DeleteUserArgs {
    id: number;
  }
}

export namespace UtilizationMutationResponseResolvers {
  export interface Resolvers<
    Context = MyContext,
    TypeParent = UtilizationMutationResponse
  > {
    facilityId?: FacilityIdResolver<Maybe<number>, TypeParent, Context>;

    capacityType?: CapacityTypeResolver<Maybe<string>, TypeParent, Context>;

    usage?: UsageResolver<Maybe<string>, TypeParent, Context>;

    timestamp?: TimestampResolver<Maybe<string>, TypeParent, Context>;

    spacesAvailable?: SpacesAvailableResolver<
      Maybe<number>,
      TypeParent,
      Context
    >;

    capacity?: CapacityResolver<Maybe<number>, TypeParent, Context>;
  }

  export type FacilityIdResolver<
    R = Maybe<number>,
    Parent = UtilizationMutationResponse,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type CapacityTypeResolver<
    R = Maybe<string>,
    Parent = UtilizationMutationResponse,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type UsageResolver<
    R = Maybe<string>,
    Parent = UtilizationMutationResponse,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type TimestampResolver<
    R = Maybe<string>,
    Parent = UtilizationMutationResponse,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type SpacesAvailableResolver<
    R = Maybe<number>,
    Parent = UtilizationMutationResponse,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type CapacityResolver<
    R = Maybe<number>,
    Parent = UtilizationMutationResponse,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
}

export namespace LoginResolvers {
  export interface Resolvers<Context = MyContext, TypeParent = Login> {
    operatorId?: OperatorIdResolver<Maybe<string>, TypeParent, Context>;

    passwordExpireInDays?: PasswordExpireInDaysResolver<
      number,
      TypeParent,
      Context
    >;

    permissions?: PermissionsResolver<(Maybe<string>)[], TypeParent, Context>;

    role?: RoleResolver<string, TypeParent, Context>;

    token?: TokenResolver<string, TypeParent, Context>;

    userId?: UserIdResolver<number, TypeParent, Context>;

    username?: UsernameResolver<string, TypeParent, Context>;
  }

  export type OperatorIdResolver<
    R = Maybe<string>,
    Parent = Login,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type PasswordExpireInDaysResolver<
    R = number,
    Parent = Login,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type PermissionsResolver<
    R = (Maybe<string>)[],
    Parent = Login,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type RoleResolver<
    R = string,
    Parent = Login,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type TokenResolver<
    R = string,
    Parent = Login,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type UserIdResolver<
    R = number,
    Parent = Login,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
  export type UsernameResolver<
    R = string,
    Parent = Login,
    Context = MyContext
  > = Resolver<R, Parent, Context>;
}

export namespace SubscriptionResolvers {
  export interface Resolvers<Context = MyContext, TypeParent = {}> {
    _empty?: _EmptyResolver<Maybe<string>, TypeParent, Context>;
  }

  export type _EmptyResolver<
    R = Maybe<string>,
    Parent = {},
    Context = MyContext
  > = SubscriptionResolver<R, Parent, Context>;
}

/** Directs the executor to skip this field or fragment when the `if` argument is true. */
export type SkipDirectiveResolver<Result> = DirectiveResolverFn<
  Result,
  SkipDirectiveArgs,
  MyContext
>;
export interface SkipDirectiveArgs {
  /** Skipped when true. */
  if: boolean;
}

/** Directs the executor to include this field or fragment only when the `if` argument is true. */
export type IncludeDirectiveResolver<Result> = DirectiveResolverFn<
  Result,
  IncludeDirectiveArgs,
  MyContext
>;
export interface IncludeDirectiveArgs {
  /** Included when true. */
  if: boolean;
}

/** Marks an element of a GraphQL schema as no longer supported. */
export type DeprecatedDirectiveResolver<Result> = DirectiveResolverFn<
  Result,
  DeprecatedDirectiveArgs,
  MyContext
>;
export interface DeprecatedDirectiveArgs {
  /** Explains why this element was deprecated, usually also including a suggestion for how to access supported similar data. Formatted using the Markdown syntax (as specified by [CommonMark](https://commonmark.org/). */
  reason?: string;
}

export interface IResolvers {
  Query?: QueryResolvers.Resolvers;
  ContactResponse?: ContactResponseResolvers.Resolvers;
  Contact?: ContactResolvers.Resolvers;
  LocalizedObject?: LocalizedObjectResolvers.Resolvers;
  Address?: AddressResolvers.Resolvers;
  FacilityResponse?: FacilityResponseResolvers.Resolvers;
  Facility?: FacilityResolvers.Resolvers;
  Capacity?: CapacityResolvers.Resolvers;
  Location?: LocationResolvers.Resolvers;
  Crs?: CrsResolvers.Resolvers;
  Name?: NameResolvers.Resolvers;
  FacilityDetails?: FacilityDetailsResolvers.Resolvers;
  Pricing?: PricingResolvers.Resolvers;
  PricingTimeRange?: PricingTimeRangeResolvers.Resolvers;
  UnavailableCapacity?: UnavailableCapacityResolvers.Resolvers;
  Port?: PortResolvers.Resolvers;
  PortLocation?: PortLocationResolvers.Resolvers;
  ContactOptions?: ContactOptionsResolvers.Resolvers;
  PaymentInfo?: PaymentInfoResolvers.Resolvers;
  OpeningHour?: OpeningHourResolvers.Resolvers;
  Day?: DayResolvers.Resolvers;
  FromUntil?: FromUntilResolvers.Resolvers;
  FacilitiesSummary?: FacilitiesSummaryResolvers.Resolvers;
  Utilization?: UtilizationResolvers.Resolvers;
  Prediction?: PredictionResolvers.Resolvers;
  FacilitiesGeoJsonResponse?: FacilitiesGeoJsonResponseResolvers.Resolvers;
  FacilityGeoJson?: FacilityGeoJsonResolvers.Resolvers;
  Geometry?: GeometryResolvers.Resolvers;
  Properties?: PropertiesResolvers.Resolvers;
  HubResponse?: HubResponseResolvers.Resolvers;
  Hub?: HubResolvers.Resolvers;
  HubDetails?: HubDetailsResolvers.Resolvers;
  HubLocation?: HubLocationResolvers.Resolvers;
  HubsSummary?: HubsSummaryResolvers.Resolvers;
  HubsGeoJsonResponse?: HubsGeoJsonResponseResolvers.Resolvers;
  HubGeoJson?: HubGeoJsonResolvers.Resolvers;
  OperatorResponse?: OperatorResponseResolvers.Resolvers;
  Operator?: OperatorResolvers.Resolvers;
  Region?: RegionResolvers.Resolvers;
  RegionWithHubs?: RegionWithHubsResolvers.Resolvers;
  User?: UserResolvers.Resolvers;
  Mutation?: MutationResolvers.Resolvers;
  UtilizationMutationResponse?: UtilizationMutationResponseResolvers.Resolvers;
  Login?: LoginResolvers.Resolvers;
  Subscription?: SubscriptionResolvers.Resolvers;
}

export interface IDirectiveResolvers<Result> {
  skip?: SkipDirectiveResolver<Result>;
  include?: IncludeDirectiveResolver<Result>;
  deprecated?: DeprecatedDirectiveResolver<Result>;
}
