readonly RESOURCE_GROUP="liipiResearchGroup"
readonly FRONTEND_POOL="liipiFrontEndPool"
readonly BACKEND_POOL="liipiBackEndPool"
readonly LOAD_BALANCER="liipiLoadBalancer"
readonly LOAD_BALANCER_RULE="liipiLoadBalancerRule"
readonly HEALTH_PROBE="liipiHealthProbe"
readonly PUBLIC_IP="liipiPublicIP"
readonly NETWORK_SECURITY_GROUP="liipiNetworkSecurityGroup"
readonly SUBNET="liipiSubnet"
readonly NETWORK_SECURITY_GROUP_ROLE="liipiNetworkSecurityGroupRule"
readonly AVAILABILITY_SET="liipiAvailabilitySet"
readonly USERNAME="azureuser"
readonly CUSTOM_DATA="cloud-init.txt"
readonly NIC="liipiNic"
readonly VNET="liipiVnet"
readonly VM_NAME="liipiVMUgur"


az group create --name liipiResearchGroup --location northeurope
az network public-ip create --resource-group liipiResearchGroup --name liipiPublicIP
az network lb create \
    --resource-group $RESOURCE_GROUP \
    --name $LOAD_BALANCER \
    --frontend-ip-name $FRONTEND_POOL \
    --backend-pool-name $BACKEND_POOL \
    --public-ip-address $PUBLIC_IP
az network lb probe create \
    --resource-group $RESOURCE_GROUP \
    --lb-name $LOAD_BALANCER \
    --name $HEALTH_PROBE \
    --protocol tcp \
    --port 80
  az network lb rule create \
    --resource-group $RESOURCE_GROUP \
    --lb-name $LOAD_BALANCER \
    --name $LOAD_BALANCER_RULE \
    --protocol tcp \
    --frontend-port 80 \
    --backend-port 80 \
    --frontend-ip-name $FRONTEND_POOL \
    --backend-pool-name $BACKEND_POOL \
    --probe-name $HEALTH_PROBE
az network vnet create \
    --resource-group $RESOURCE_GROUP \
    --name liipiVnet \
    --subnet-name $SUBNET
az network nsg create \
    --resource-group $RESOURCE_GROUP \
    --name $NETWORK_SECURITY_GROUP
az network nsg rule create \
    --resource-group $RESOURCE_GROUP \
    --nsg-name $NETWORK_SECURITY_GROUP \
    --name $NETWORK_SECURITY_GROUP_ROLE \
    --priority 1001 \
    --protocol tcp \
    --destination-port-range 80
for i in `seq 1 1`; do
    az network nic create \
        --resource-group $RESOURCE_GROUP \
        --name $NIC$i \
        --vnet-name $VNET \
        --subnet $SUBNET \
        --network-security-group $NETWORK_SECURITY_GROUP \
        --lb-name $LOAD_BALANCER \
        --lb-address-pools $BACKEND_POOL
done
az vm availability-set create \
    --resource-group $RESOURCE_GROUP \
    --name $AVAILABILITY_SET
for i in `seq 1 1`; do
    az vm create \
        --resource-group $RESOURCE_GROUP \
        --name $VM_NAME$i \
        --availability-set $AVAILABILITY_SET \
        --nics $NIC$i \
        --image UbuntuLTS \
        --admin-username $USERNAME \
        --generate-ssh-keys \
        --custom-data $CUSTOM_DATA \
        --no-wait
done
az network public-ip show \
    --resource-group $RESOURCE_GROUP \
    --name $PUBLIC_IP \
    --query [ipAddress] \
    --output tsv


