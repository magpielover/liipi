import gql from "graphql-tag";

export const QUERY_FACILITY_LIST = gql`
  query FacilityList($ids: [Int]) {
    facilities(ids: $ids) {
      totalCount
      facilities {
        id
        name {
          fi
        }
        status
        location {
          crs {
            type
            properties {
              name
            }
          }
          bbox
          type
          coordinates
        }
        builtCapacity {
          CAR
          ELECTRIC_CAR
          BICYCLE_SECURE_SPACE
          DISABLED
          MOTORCYCLE
          BICYCLE
        }
        usages
      }
      hasMore
    }
  }
`;
