import * as React from "react";
import { useTranslation } from "react-i18next";
import { useFacilityListQuery } from "../../generated/graphql";
import FacilityList from "./FacilityList";

interface Props {
  ids: any;
}

const FacilityListContainer = (props: Props) => {
  const { t } = useTranslation("commons");
  const { data, error, loading } = useFacilityListQuery({
    variables: { ids: props.ids }
  });

  if (loading) {
    return (
      <tr>
        <td colSpan={4}>{t("loading")}</td>
      </tr>
    );
  }

  if (error || !data) {
    return <tr />;
  }
  const facilities = (data.facilities && data.facilities.facilities) || [];

  return <FacilityList facilities={facilities} />;
};
export default FacilityListContainer;
