import * as React from "react";
import { useTranslation } from "react-i18next";
import { Link } from "react-router-dom";
import { Facility } from "../../generated/graphql";
import {
  toPascalCase,
  translateList,
  translateValue
} from "../../util/translateUtils";
import styles from "./FacilityList.module.css";
import "./style.css";

interface Props {
  facilities: Facility[];
}

const className = "FacilityList";

const FacilityList: React.FC<Props> = ({ facilities }) => {
  const { t } = useTranslation(["commons", "facilityList"]);

  return (
    <>
      {facilities.map(
        (facility: any, i: any) =>
          !!facility && (
            <tr key={facility.id} className={`${className}__item`}>
              <td style={{ width: "25%" }}>
                <Link
                  to={{
                    pathname: `/facility/${facility.id}`
                  }}
                >
                  {facility.name.fi}
                </Link>
              </td>
              <td style={{ width: "25%" }}>
                <span>
                  {facility.status
                    ? translateValue("facilityList:status", facility.status, t)
                    : ""}
                </span>
              </td>
              <td style={{ width: "25%" }}>
                <span>
                  {facility.usages && facility.usages.length
                    ? translateList("usage", facility.usages, t)
                    : ""}
                </span>
              </td>
              <td style={{ width: "25%" }}>
                {facility.builtCapacity && (
                  <div className={styles.facilityIconsContainer}>
                    {Object.keys(facility.builtCapacity)
                      .filter(key => facility.builtCapacity[key])
                      .map(type => {
                        return (
                          <span
                            title={translateValue("capacityType", type, t)}
                            key={type}
                            className={
                              styles["facilityIcon" + toPascalCase(type)]
                            }
                          />
                        );
                      })}
                  </div>
                )}
              </td>
            </tr>
          )
      )}
    </>
  );
};

export default FacilityList;
