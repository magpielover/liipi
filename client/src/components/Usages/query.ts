import gql from "graphql-tag";

export const QUERY_USAGE_LIST = gql`
  query UsageList {
    usages
  }
`;
