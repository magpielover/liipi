import React, { useState } from "react";
import useForm from "react-hook-form";
import { useTranslation } from "react-i18next";
import Modal from "react-modal";
import Select from "react-select";
import { ValueType } from "react-select/src/types";
import { useCreateUserMutation } from "../../generated/graphql";
import { validatePassword } from "../../util/helpers";
import FormError from "../Commons/FormError";
import ModalBody from "../Commons/ModalBody";
import ModalContent from "../Commons/ModalContent";
import ModalFooter from "../Commons/ModalFooter";
import ModalHeader from "../Commons/ModalHeader";
import "./styles.css";
interface Props {
  isOpen: boolean;
  onClose: any;
  operators: any;
}

const NewUserModal: React.FC<Props> = (props: Props) => {
  const [error, setError] = useState();
  const [roleSelected, setRole] = useState();
  const [operatorSelected, setOperator] = useState();
  const [createUser] = useCreateUserMutation();
  const USER_ROLES = [
    { label: "Ylläpitäjä", value: "ADMIN" },
    { label: "Api", value: "OPERATOR_API" },
    { label: "Operaattori", value: "OPERATOR" }
  ];
  const { register, handleSubmit, errors } = useForm();
  const onSubmit = async (data: any) => {
    if (roleSelected && validatePassword(data.password)) {
      if (
        (roleSelected === "OPERATOR" || roleSelected === "OPERATOR_API") &&
        !operatorSelected
      ) {
        setError("Select Operator");
        return;
      }
      try {
        await createUser({
          variables: {
            user: {
              role: roleSelected,
              username: data.username,
              password: data.password,
              operatorId: +operatorSelected
            }
          }
        });
        props.onClose();
      } catch (err) {
        setError(err.message);
      }
    }
  };

  const { t } = useTranslation(["userList", "login"]);

  const handleRoleChange = (inputValue: ValueType<string>) => {
    setRole(inputValue ? JSON.parse(JSON.stringify(inputValue)).value : "");
  };
  const handleOperatorChange = (inputValue: ValueType<string>) => {
    setOperator(inputValue ? JSON.parse(JSON.stringify(inputValue)).value : "");
  };
  const handleCloseModal = () => {
    props.onClose();
  };

  return (
    <React.Fragment>
      <Modal isOpen={props.isOpen} className="modal-dialog">
        <ModalContent>
          <div id="newUserFormModal">
            <form
              name="userForm"
              className="form-horizontal"
              role="form"
              onSubmit={handleSubmit(onSubmit)}
            >
              <ModalHeader title={t("addNewUser")} />

              <ModalBody>
                {error ? <FormError errorMessage={error} /> : null}

                <div className="row">
                  <div className="column3 first-column">
                    <label>{t("userRole")} *</label>
                  </div>
                </div>
                <div className="row">
                  <div className="column3 first-column">
                    <div className="form-group">
                      <Select
                        placeholder="Select"
                        onChange={(input: any) => handleRoleChange(input)}
                        options={USER_ROLES}
                        name="role"
                        className={
                          roleSelected === "" ? "validation-error" : ""
                        }
                      />
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="column3 first-column">
                    <label> {t("login:username")}</label>
                  </div>
                </div>
                <div className="row">
                  <div className="column3 first-column">
                    <div className="form-group">
                      <input
                        name="username"
                        ref={register({ required: true })}
                        className={
                          "form-control " +
                          (errors.username ? "validation-error" : "")
                        }
                      />
                    </div>
                  </div>
                </div>
                {roleSelected !== "OPERATOR_API" ? (
                  <>
                    <div className="row">
                      <div className="column3 first-column">
                        <label> {t("login:password")}</label>
                      </div>
                    </div>
                    <div className="row">
                      <div className="column3 first-column">
                        <div className="form-group">
                          <input
                            name="password"
                            type="password"
                            className={
                              "form-control " +
                              (errors.password ? "validation-error" : "")
                            }
                            ref={register({ required: true, maxLength: 100 })}
                          />
                        </div>
                      </div>
                    </div>
                  </>
                ) : null}
                <div>
                  {roleSelected === "OPERATOR" ||
                  roleSelected === "OPERATOR_API" ? (
                    <>
                      <div className="row">
                        <div className="column3 first-column">
                          <label> {t("userList:userRoleOperator")} *</label>
                        </div>
                      </div>
                      <div className="row">
                        <div className="column3 first-column">
                          <div className="form-group">
                            <Select
                              name="operator"
                              className={
                                operatorSelected === ""
                                  ? "validation-error"
                                  : ""
                              }
                              options={props.operators}
                              placeholder={"Valitse"}
                              onChange={(operator: ValueType<string>) =>
                                handleOperatorChange(operator)
                              }
                            />
                          </div>
                        </div>
                      </div>
                    </>
                  ) : null}
                </div>
              </ModalBody>
              <ModalFooter>
                <div className="last-column">
                  <button
                    type="button"
                    id="wdUserModalCancel"
                    className="btn btn-xs-secondary link-button"
                    onClick={handleCloseModal}
                  >
                    {t("login:cancel")}
                  </button>
                  <button
                    type="submit"
                    id="wdUserModalOk"
                    className="btn btn-xs link-button"
                  >
                    {t("login:ready")}
                  </button>
                </div>
              </ModalFooter>
            </form>
          </div>
        </ModalContent>
      </Modal>
    </React.Fragment>
  );
};
export default NewUserModal;
