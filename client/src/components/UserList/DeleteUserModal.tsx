import React, { useState } from "react";
import { useTranslation } from "react-i18next";
import Modal from "react-modal";
import { useDeleteUserMutation } from "../../generated/graphql";
import FormError from "../Commons/FormError";
import ModalBody from "../Commons/ModalBody";
import ModalContent from "../Commons/ModalContent";
import ModalFooter from "../Commons/ModalFooter";
import "./styles.css";
interface User {
  username: string;
  id: number;
  role: string;
}
interface Props {
  isOpen: boolean;
  onClose: any;
  user: User;
}

const DeleteUserModal: React.FC<Props> = ({ user, onClose, isOpen }) => {
  const [error, setError] = useState();
  const [deleteUser] = useDeleteUserMutation();

  const { t } = useTranslation(["userList", "login"]);

  const handleDeleteUser = async (e: any) => {
    e.preventDefault();
    try {
      // TODO: Check the success message
      const res = await deleteUser({ variables: { id: user.id } });
      onClose();
    } catch (err) {
      setError("Not successful!");
    }
  };
  const handleCloseModal = () => {
    onClose();
  };

  return (
    <React.Fragment>
      <Modal isOpen={isOpen} className="modal-dialog">
        <ModalContent>
          <div id="newUserFormModal">
            <form
              name="editOperatorForm"
              className="form-horizontal"
              role="form"
            >
              <ModalBody>
                {error ? <FormError errorMessage={error} /> : null}
                <span style={{ textAlign: "center" }}>
                  {t("doYouWantToDeleteUser")} "{user ? user.username : ""}
                  "?
                </span>
              </ModalBody>

              <ModalFooter>
                <div style={{ width: "70%", margin: "auto" }}>
                  <button
                    id="wdUserModalCancel"
                    className="btn btn-xs link-button"
                    style={{ float: "left" }}
                    onClick={handleDeleteUser}
                  >
                    {t("confirmDelete")}
                  </button>
                  <button
                    type="button"
                    id="wdUserModalOk"
                    className="btn  btn-xs-secondary link-button"
                    style={{ float: "left" }}
                    onClick={handleCloseModal}
                  >
                    {t("login:cancel")}
                  </button>
                </div>
              </ModalFooter>
            </form>
          </div>
        </ModalContent>
      </Modal>
    </React.Fragment>
  );
};
export default DeleteUserModal;
