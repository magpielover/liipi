import React, { useState } from "react";
import { useTranslation } from "react-i18next";

import {
  useOperatorNamesQuery,
  UserListQuery,
  useUpdateTokenMutation
} from "../../generated/graphql";
import { toPascalCase, translateValue } from "../../util/translateUtils";
import Breadcrumb from "../Commons/Breadcrumb";
import ChangePasswordModal from "./ChangePasswordModal";
import DeleteUserModal from "./DeleteUserModal";
import Info from "./Info";
import NewUserModal from "./NewUserModal";
import UpdateTokenModal from "./UpdateTokenModal";
interface Props {
  usersData: UserListQuery;
}
const className = "UserList";
const UserList: React.FC<Props> = ({ usersData }) => {
  const [modalType, setModalType] = useState();
  const [selectedUser, setUser] = useState();
  const [updateToken, { data: tokenData }] = useUpdateTokenMutation();
  const { t } = useTranslation(["userList", "navbar"]);
  const handleCloseModal = () => {
    setModalType("");
  };
  const handleChangePassword = (user: object) => {
    setUser(user);
    setModalType("changePassword");
  };
  const handleUserDelete = (user: object) => {
    setUser(user);
    setModalType("deleteUser");
  };
  const handleUpdateToken = async (id: number) => {
    const res = await updateToken({ variables: { id } });
    setModalType("updateToken");
  };
  const renderModal = () => {
    switch (modalType) {
      case "newUser":
        return (
          <NewUserModal
            isOpen={true}
            onClose={handleCloseModal}
            operators={allOperators}
          />
        );

      case "changePassword":
        return (
          <ChangePasswordModal
            isOpen={true}
            onClose={handleCloseModal}
            user={selectedUser}
          />
        );

      case "deleteUser":
        return (
          <DeleteUserModal
            isOpen={true}
            onClose={handleCloseModal}
            user={selectedUser}
          />
        );

      case "updateToken":
        return (
          <UpdateTokenModal
            isOpen={true}
            onClose={handleCloseModal}
            token={
              tokenData && tokenData.updateToken ? tokenData.updateToken : ""
            }
          />
        );
    }
  };

  const { data, loading, error } = useOperatorNamesQuery();
  const allOperators =
    data && data.operators && data.operators.results
      ? data.operators.results.map(item => {
          const name = !!item && !!item.name ? item.name.fi : "";
          const id = !!item && !!item.id ? item.id : null;
          return {
            label: name,
            value: id
          };
        })
      : [];

  const getOperatorName = (id: string) => {
    const operators = data && data.operators ? data.operators.results : [];
    const operator = operators
      ? operators.find(item => {
          return item && item.id === id;
        })
      : null;
    return operator && operator.name ? operator.name.fi : "";
  };

  return (
    <React.Fragment>
      <Breadcrumb
        name={t("navbar:users")}
        canGoback={false}
        canAddUser={true}
        onClickNewUser={() => setModalType("newUser")}
      />
      <div className="container content">
        <Info />
        <div className="panel panel-default wdUsersView">
          <table className="table table-bordered table-condensed">
            <thead>
              <tr>
                <th>{t("userUsername")}</th>
                <th>{t("userOperator")}</th>
                <th>{t("userRole")}</th>
                <th>{t("userActions")}</th>
              </tr>
            </thead>
            <tbody>
              {!!usersData.users &&
                !!usersData.users &&
                usersData.users.map(
                  (user: any, i: any) =>
                    !!user && (
                      <tr key={user.id} className={`${className}__item`}>
                        <td>{user.username}</td>
                        <td>{getOperatorName(user.operatorId)}</td>
                        <td>{translateValue("userRole", user.role, t)}</td>
                        <td>
                          {user.role === "OPERATOR_API" ? (
                            <button
                              className="btn btn-xs link-button"
                              onClick={() => handleUpdateToken(user.id)}
                            >
                              <span>{t("updateToken")}</span>
                            </button>
                          ) : (
                            <button
                              className="btn btn-xs link-button"
                              onClick={() => handleChangePassword(user)}
                            >
                              <span>{t("changePassword")}</span>
                            </button>
                          )}

                          <button
                            className="btn btn-xs wdDeleteUser link-button"
                            onClick={() => handleUserDelete(user)}
                          >
                            <span>{t("delete")}</span>
                          </button>
                        </td>
                      </tr>
                    )
                )}
            </tbody>
          </table>
        </div>
      </div>
      {renderModal()}
    </React.Fragment>
  );
};
export default UserList;
