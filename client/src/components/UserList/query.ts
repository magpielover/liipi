import gql from "graphql-tag";

export const QUERY_USER_LIST = gql`
  query UserList {
    users {
      id
      username
      operatorId
      role
    }
  }
`;
export const QUERY_OPERATOR_NAMES = gql`
  query OperatorNames {
    operators {
      results {
        id
        name {
          fi
        }
      }
    }
  }
`;

export const MUTATION_USER_MUTATIONS = gql`
  input UserInput {
    role: String
    username: String
    password: String
    operatorId: Int
  }
  mutation ChangePassword($id: Int!, $newPassword: String!) {
    changePassword(id: $id, newPassword: $newPassword)
  }
  mutation DeleteUser($id: Int!) {
    deleteUser(id: $id)
  }
  mutation CreateUser($user: UserInput!) {
    createUser(user: $user) {
      id
      username
      role
      operatorId
    }
  }
  mutation UpdateToken($id: Int!) {
    updateToken(id: $id)
  }
`;
