import * as React from "react";
import { useTranslation } from "react-i18next";

const Info: React.FC<any> = () => {
  const { t } = useTranslation("userList");

  return (
    <div>
      <div className="row smallMarginBottom">
        <p>{t("infoLead")}</p>
        <ul>
          <li>{t("infoBullet1")}</li>
          <li>{t("infoBullet2")}</li>
          <li>{t("infoBullet3")}</li>
          <li>{t("infoBullet4")}</li>
        </ul>
      </div>
    </div>
  );
};

export default Info;
