import React, { useState } from "react";
import useForm from "react-hook-form";
import { useTranslation } from "react-i18next";
import Modal from "react-modal";
import { useChangePasswordMutation } from "../../generated/graphql";
import { validatePassword } from "../../util/helpers";
import FormError from "../Commons/FormError";
import ModalBody from "../Commons/ModalBody";
import ModalContent from "../Commons/ModalContent";
import ModalFooter from "../Commons/ModalFooter";
import ModalHeader from "../Commons/ModalHeader";
import "./styles.css";
interface User {
  username: string;
  id: number;
  role: string;
}
interface Props {
  isOpen: boolean;
  onClose: any;
  user: User;
}

const ChangePasswordModal: React.FC<Props> = (props: Props) => {
  const [error, setError] = useState();
  const [changePassword] = useChangePasswordMutation();
  const { register, handleSubmit, errors } = useForm();
  const onSubmit = async (data: any) => {
    if (isPasswordValid(data)) {
      try {
        const { data: passwordSuccess } = await changePassword({
          variables: { id: props.user.id, newPassword: data.password }
        });
        props.onClose();
      } catch (err) {
        setError("Not successful");
      }
    }
  };

  const isPasswordValid = (data: any) => {
    if (!data.password && !data.passwordConfirm) {
      setError(t("passwordInvalid"));
      return false;
    } else if (data.password !== data.passwordConfirm) {
      setError(t("passwordDontMatch"));
      return false;
    } else if (!validatePassword(data.password)) {
      setError(t("passwordInvalid"));
      return false;
    }
    return true;
  };

  const { t } = useTranslation(["userList", "login"]);

  const handleCloseModal = () => {
    props.onClose();
  };

  return (
    <React.Fragment>
      <Modal isOpen={props.isOpen} className="modal-dialog">
        <ModalContent>
          <div id="newUserFormModal">
            <form
              name="editOperatorForm"
              className="form-horizontal"
              role="form"
              onSubmit={handleSubmit(onSubmit)}
            >
              <ModalHeader title={t("changePassword")} />
              <ModalBody>
                {error ? <FormError errorMessage={error} /> : null}

                <div className="row">
                  <div className="column3 first-column">
                    <label>{t("newPassword")} *</label>
                  </div>
                </div>
                <div className="row">
                  <div className="column3 first-column">
                    <div className="form-group">
                      <input
                        type="password"
                        name="password"
                        ref={register({ required: true })}
                        className={
                          "form-control " +
                          (errors.username ? "validation-error" : "")
                        }
                      />
                    </div>
                  </div>
                </div>
                <div>
                  <div className="row">
                    <div className="column3 first-column">
                      <label>{t("confirmNewPassword")} *</label>
                    </div>
                  </div>
                  <div className="row">
                    <div className="column3 first-column">
                      <div className="form-group">
                        <input
                          name="passwordConfirm"
                          type="password"
                          className={
                            "form-control " +
                            (errors.password ? "validation-error" : "")
                          }
                          ref={register({ required: true })}
                        />
                      </div>
                    </div>
                  </div>
                </div>
                <div />
              </ModalBody>

              <ModalFooter>
                <div className="last-column">
                  <button
                    type="button"
                    id="wdUserModalCancel"
                    className="btn btn-xs-secondary link-button"
                    onClick={handleCloseModal}
                  >
                    {t("login:cancel")}
                  </button>
                  <button
                    type="submit"
                    id="wdUserModalOk"
                    className="btn btn-xs link-button"
                  >
                    {t("login:ready")}
                  </button>
                </div>
              </ModalFooter>
            </form>
          </div>
        </ModalContent>
      </Modal>
    </React.Fragment>
  );
};
export default ChangePasswordModal;
