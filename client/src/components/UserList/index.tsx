import * as React from "react";
import { useTranslation } from "react-i18next";
import { useUserListQuery } from "../../generated/graphql";
import UserList from "./UserList";

const UserListContainer = () => {
  const { t } = useTranslation("commons");
  const { data, error, loading } = useUserListQuery();

  if (loading) {
    return <div>{t("loading")}</div>;
  }

  if (error || !data) {
    return <div />;
  }

  return (
    <React.Fragment>
      <UserList usersData={data} />
    </React.Fragment>
  );
};
export default UserListContainer;
