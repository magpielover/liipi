import React, { useState } from "react";
import { useTranslation } from "react-i18next";
import Modal from "react-modal";
import ModalBody from "../Commons/ModalBody";
import ModalContent from "../Commons/ModalContent";
import ModalFooter from "../Commons/ModalFooter";
import "./styles.css";

interface Props {
  isOpen: boolean;
  onClose: any;
  token: string;
}

const UpdateTokenModal: React.FC<Props> = ({ token, onClose, isOpen }) => {
  const { t } = useTranslation(["userList", "login"]);
  const handleCloseModal = () => {
    onClose();
  };

  return (
    <React.Fragment>
      <Modal isOpen={isOpen} className="modal-dialog">
        <ModalContent>
          <div id="newUserFormModal">
            <form
              name="editOperatorForm"
              className="form-horizontal"
              role="form"
            >
              <ModalBody>
                <span style={{ textAlign: "center" }}>
                  {t("tokenReceived") + "  " + token}
                </span>
              </ModalBody>

              <ModalFooter>
                <div style={{ width: "15%", margin: "auto" }}>
                  <button
                    id="wdUserModalCancel"
                    className="btn btn-xs link-button"
                    style={{ width: "100%" }}
                    onClick={handleCloseModal}
                  >
                    {t("login:ready")}
                  </button>
                </div>
              </ModalFooter>
            </form>
          </div>
        </ModalContent>
      </Modal>
    </React.Fragment>
  );
};
export default UpdateTokenModal;
