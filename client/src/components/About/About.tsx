import * as React from "react";
import { useTranslation } from "react-i18next";
import Breadcrumb from "../Commons/Breadcrumb";

const About: React.FC<any> = () => {
  const { t } = useTranslation("about");
  return (
    <>
      <Breadcrumb name={t("about")} />
      <div className="container content">
        <div className="row smallMarginBottom">
          <p>{t("aboutInfo")}</p>
          <ul>
            <li>
              <a href="https://www.hsl.fi/kayttoehdot">
                {" "}
                {t("aboutTermsOfService")}
              </a>
            </li>
            <li>
              <a href="https://test.p.hsl.fi/docs/">{t("aboutAPIdocs")}</a>
              {t("aboutBullet1")}
              <a href="https://github.com/HSLdevcom/parkandrideAPI">
                GitHub repository
              </a>
            </li>
            <li>{t("aboutBullet2")}</li>
            <li>
              {t("aboutLicense")}{" "}
              <a href="http://creativecommons.org/licenses/by/4.0/deed.fi">
                Creative Commons Nimeä 4.0 Kansainvälinen{" "}
              </a>
              {t("aboutWithLicense")}
            </li>
          </ul>
        </div>
      </div>
    </>
  );
};

export default About;
