import * as React from "react";
import { useTranslation } from "react-i18next";
import Breadcrumb from "../Commons/Breadcrumb";

const AboutEn: React.FC<any> = () => {
  const { t } = useTranslation("aboutEn");
  return (
    <>
      <Breadcrumb name={t("about")} />
      <div className="container content">
        <div className="row smallMarginBottom">
          <p>{t("aboutInfo")}</p>
          <ul>
            <li>
              <a href="https://www.hsl.fi/kayttoehdot">Terms of Service</a>
            </li>
            <li>
              <a href="https://test.p.hsl.fi/docs/">
                Documentation for developers (API-description).
              </a>{" "}
              The open source service also has it's own
              <a href="https://github.com/HSLdevcom/parkandrideAPI">
                {" "}
                GitHub repository
              </a>
            </li>
            <li>
              This UI supports the newest browser versions (IE 11+, Firefox 29+,
              Chrome 35+, Safari 7+)
            </li>
            <li>
              HSL’s open data is licensed under the
              <a href="http://creativecommons.org/licenses/by/4.0/deed.fi">
                {" "}
                Creative Commons BY 4.0 International -license
              </a>
            </li>
          </ul>
        </div>
      </div>
    </>
  );
};

export default AboutEn;
