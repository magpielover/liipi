import gql from "graphql-tag";

export const QUERY_REGION_LIST = gql`
  query RegionList {
    regions {
      id
      name {
        fi
        sv
        en
      }
    }
  }
  query RegionsWithHubs {
    regionsWithHubs {
      id
      name {
        fi
        sv
        en
      }
      hubIds
    }
  }
`;
