import gql from "graphql-tag";

export const QUERY_CAPACITY_TYPE_LIST = gql`
  query CapacityTypeList {
    capacityTypes
  }
`;
