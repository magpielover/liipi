import * as React from "react";
import { useTranslation } from "react-i18next";
import { HubProfileQuery } from "../../generated/graphql";

interface Props {
  data: HubProfileQuery;
}

const HubBasicInfo: React.FC<Props> = ({ data }) => {
  const { t } = useTranslation(["hubProfile", "commons"]);
  const details = data.hubDetails;

  const shouldShowStreetAddress = () => {
    return (
      !!data.hubDetails &&
      !!data.hubDetails.address &&
      !!data.hubDetails.address.streetAddress
    );
  };

  const shouldShowPostalCode = () => {
    return (
      !!data.hubDetails &&
      !!data.hubDetails.address &&
      !!data.hubDetails.address.postalCode
    );
  };

  const shouldShowCity = () => {
    return (
      !!data.hubDetails &&
      !!data.hubDetails.address &&
      !!data.hubDetails.address.city
    );
  };

  const shouldShowAddress = () => {
    return (
      shouldShowStreetAddress() && shouldShowPostalCode() && shouldShowCity()
    );
  };

  return (
    <div>
      <div className="row">
        <h3 className="h3-first">{t("titleBasicInfo")}</h3>
        <h4>{t("labelName")}</h4>
        <div className="column3 first-column">
          <p>
            <span className="lang-fi ">
              {details.name ? details.name.fi : "-"}
            </span>{" "}
            (fi)
          </p>
        </div>
        <div className="column3">
          <p>
            <span className="lang-sv ">
              {details.name ? details.name.sv : "-"}
            </span>{" "}
            (sv)
          </p>
        </div>
        <div className="column3 last-column">
          <p>
            <span className="lang-en ">
              {details.name ? details.name.en : "-"}
            </span>{" "}
            (en)
          </p>
        </div>
      </div>
      <div className="row">
        <h4>Käyntiosoite</h4>
      </div>
      {shouldShowAddress() && (
        <>
          {shouldShowStreetAddress() && (
            <div className="row streetAddress">
              <div className="column3 first-column">
                <p>
                  <span className="lang-fi ">
                    {details.address && details.address.streetAddress
                      ? details.address.streetAddress.fi
                      : "-"}
                  </span>{" "}
                  (fi)
                </p>
              </div>
              <div className="column3">
                <p>
                  <span className="lang-sv ">
                    {details.address && details.address.streetAddress
                      ? details.address.streetAddress.sv
                      : "-"}
                  </span>{" "}
                  (sv)
                </p>
              </div>
              <div className="column3 last-column">
                <p>
                  <span className="lang-en ">
                    {details.address && details.address.streetAddress
                      ? details.address.streetAddress.en
                      : "-"}
                  </span>{" "}
                  (en)
                </p>
              </div>
            </div>
          )}
          {shouldShowCity() && (
            <div className="row city">
              <div className="column3 first-column">
                <p>
                  <span className="lang-fi ">
                    {details.address && details.address.city
                      ? details.address.city.fi
                      : "-"}
                  </span>{" "}
                  (fi)
                </p>
              </div>
              <div className="column3">
                <p>
                  <span className="lang-sv ">
                    {details.address && details.address.city
                      ? details.address.city.sv
                      : "-"}
                  </span>{" "}
                  (sv)
                </p>
              </div>
              <div className="column3 last-column">
                <p>
                  <span className="lang-en ">
                    {details.address && details.address.city
                      ? details.address.city.en
                      : "-"}
                  </span>{" "}
                  (en)
                </p>
              </div>
            </div>
          )}
        </>
      )}
      <div className="row city">
        <div className="column3 first-column">
          <p>
            <span className="lang-fi ">
              {details.address && details.address.city
                ? details.address.city.fi
                : "-"}
            </span>{" "}
            (fi)
          </p>
        </div>
        <div className="column3">
          <p>
            <span className="lang-sv ">
              {details.address && details.address.city
                ? details.address.city.sv
                : "-"}
            </span>{" "}
            (sv)
          </p>
        </div>
        <div className="column3 last-column">
          <p>
            <span className="lang-en ">
              {details.address && details.address.city
                ? details.address.city.en
                : "-"}
            </span>{" "}
            (en)
          </p>
        </div>
      </div>
    </div>
  );
};

export default HubBasicInfo;
