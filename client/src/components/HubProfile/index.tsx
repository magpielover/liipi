import * as React from "react";
import { useTranslation } from "react-i18next";
import { useHubProfileQuery } from "../../generated/graphql";
import HubProfile from "./HubProfile";

interface Params {
  id: string;
  url: string;
  isExact: boolean;
}
interface Match {
  params: Params;
}

interface Props {
  match: Match;
}

const HubProfileContainer = ({ match }: Props) => {
  const { t } = useTranslation("commons");
  const { data, error, loading } = useHubProfileQuery({
    variables: { id: match.params.id }
  });

  if (loading) {
    return <div>{t("loading")}</div>;
  }

  if (error || !data) {
    return <div />;
  }

  return <HubProfile hub={data} />;
};

export default HubProfileContainer;
