import gql from "graphql-tag";

export const QUERY_HUB_PROFILE = gql`
  query HubProfile($id: ID!) {
    hubDetails(id: $id) {
      id
      name {
        fi
        sv
        en
      }
      location {
        crs {
          type
          properties {
            name
          }
        }
        type
        coordinates
      }
      address {
        streetAddress {
          fi
          en
          sv
        }
        postalCode
        city {
          fi
          en
          sv
        }
      }
      facilityIds
    }
  }
`;
