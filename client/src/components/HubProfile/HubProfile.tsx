import * as React from "react";
import { useTranslation } from "react-i18next";
import { Link } from "react-router-dom";
import { HubProfileQuery, useFacilityListQuery } from "../../generated/graphql";
import { getBounds, getCenter, getMaxBbox } from "../../util/mapUtils";
import Breadcrumb from "../Commons/Breadcrumb";

import BuiltCapacity from "../Commons/BuiltCapacity";
import Location from "../Commons/Location";
import FacilityList from "../FacilityList/FacilityList";
import HubBasicInfo from "./BasicInfo";
import "./styles.css";

interface Props {
  hub: HubProfileQuery;
}

const getFeatures: any = (locations: any) => {
  return Array.isArray(locations)
    ? locations.map(location => ({
        type: "Feature",
        geometry: {
          coordinates: location.coordinates,
          type: location.type
        }
      }))
    : [];
};

const HubProfile: React.FC<Props> = ({ hub }) => {
  const { t } = useTranslation(["hubProfile", "commons"]);

  const ids = hub.hubDetails ? hub.hubDetails.facilityIds : [];

  const hubName =
    hub.hubDetails && hub.hubDetails.name ? hub.hubDetails.name.fi : "";
  const { data: facilitiesData, error, loading } = useFacilityListQuery({
    skip: !ids.length,
    variables: { ids }
  });

  if (loading) {
    return <div>{t("commons:loading")}</div>;
  }

  if (error) {
    return <div>{t("commons:error")}</div>;
  }

  const facilities: any =
    facilitiesData && facilitiesData.facilities
      ? facilitiesData.facilities.facilities
      : [];

  const locations: any = facilities.map((facility: any) => facility.location);
  const hubLocation: any = hub.hubDetails ? hub.hubDetails.location : undefined;
  locations.push(hubLocation);

  const features = getFeatures(locations);
  const geojson: any = {
    type: "FeatureCollection",
    features
  };
  const bboxes: any = [];

  locations.forEach((location: any) => {
    const tempBbox = location ? location.bbox : undefined;

    if (Array.isArray(tempBbox) && tempBbox.length === 4) {
      bboxes.push(tempBbox);
    }
  });

  const bbox = getMaxBbox(bboxes);
  const bounds = getBounds(bbox);
  const center = getCenter(bbox);
  const zoom = 15;

  return (
    <>
      <Breadcrumb name={hubName} canGoback={true} />
      <div className="container content">
        <div className="row wdHubView">
          <HubBasicInfo data={hub} />
          <Location
            bounds={bounds}
            center={center}
            geojson={geojson}
            hubs={undefined}
            zoom={zoom}
          />
          <div className="row">
            <h3>{t("titleFacilities")}</h3>
            {!facilities.length ? (
              <p className="wdNoFacilitiesMsg ">{t("messageNoFacilities")}</p>
            ) : (
              <div className="panel panel-default wdFacilitiesTable">
                <table className="table table-bordered table-striped table-condensed">
                  <thead>
                    <tr>
                      <th>
                        <span>{t("labelName")}</span>
                      </th>
                      <th>
                        <span>{t("labelStatus")}</span>
                      </th>
                      <th>
                        <span>{t("labelUsage")}</span>
                      </th>
                      <th>
                        <span>{t("labelCapacityTypes")}</span>
                      </th>
                    </tr>
                  </thead>
                  <tbody>
                    <FacilityList facilities={facilities} />
                  </tbody>
                </table>
              </div>
            )}
          </div>

          {!!facilities.length && (
            <div className="row">
              <h3 className="h3-view wdFacilitiesTotal ">
                {t("titleBuiltCapacity")}
              </h3>
              <div className="panel panel-default">
                <BuiltCapacity facilities={facilities} />
              </div>
            </div>
          )}
        </div>
      </div>

      <div className="row navi-below">
        <div className="container">
          <div className="secondary-navi-column first-column" />
          <div className="secondary-navi-column text-right last-column">
            <Link className="btn btn-s btn-responsive ng-binding" to="/hubs">
              {t("buttonBack")}
            </Link>
          </div>
        </div>
      </div>
    </>
  );
};

export default HubProfile;
