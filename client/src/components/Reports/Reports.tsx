import { decode } from "base64-arraybuffer";
import { saveAs } from "file-saver";
import { Form, Formik } from "formik";
import { capitalize } from "lodash";
import * as React from "react";
import { useTranslation } from "react-i18next";
import {
  CapacityTypeListQuery,
  Facility,
  FacilityListQuery,
  Hub,
  HubListQuery,
  Operator,
  OperatorListQuery,
  RegionsWithHubsQuery,
  RegionWithHubs,
  UsageListQuery,
  useFacilityUsageReportMutation,
  useHubsAndFacilitiesReportMutation,
  useMaxUtilizationReportMutation,
  useRequestLogReportMutation
} from "../../generated/graphql";
import { dateToArray } from "../../util/dateUtils";
import { translateValue } from "../../util/translateUtils";
import Breadcrumb from "../Commons/Breadcrumb";
import DatePickerField from "../Commons/DatePickerFields";
import { useLoginModalContext } from "../Commons/LoginModal";
import RadioButtonField from "../Commons/RadioButtonField";
import SelectField, { Option } from "../Commons/SelectField";
import Info from "./Info";

interface Fields {
  [key: string]: string[];
}

const sortOptions = (a: Option, b: Option): number => {
  if (a.label < b.label) {
    return -1;
  }
  if (a.label > b.label) {
    return 1;
  }
  return 0;
};

const XLSXContentType =
  "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;";

const FIELDS = {
  RANGE: "range",
  INTERVAL: "interval",
  OPERATOR: "operator",
  USAGES: "usages",
  CAPACITY_TYPES: "capacityTypes",
  REGIONS: "regions",
  HUBS: "hubs",
  FACILITIES: "facilities",
  REGUEST_LOG_INTERVAL: "requestLogInterval"
};

const Fields: Fields = {
  facilityUsage: [
    FIELDS.RANGE,
    FIELDS.INTERVAL,
    FIELDS.OPERATOR,
    FIELDS.USAGES,
    FIELDS.CAPACITY_TYPES,
    FIELDS.REGIONS,
    FIELDS.HUBS,
    FIELDS.FACILITIES
  ],
  hubsAndFacilities: [],
  maxUtilization: [
    FIELDS.RANGE,
    FIELDS.OPERATOR,
    FIELDS.USAGES,
    FIELDS.CAPACITY_TYPES,
    FIELDS.REGIONS,
    FIELDS.HUBS,
    FIELDS.FACILITIES
  ],
  requestLog: [FIELDS.RANGE, FIELDS.REGUEST_LOG_INTERVAL]
};

const REPORT_TYPE = {
  FACILITY_USAGE: "facilityUsage",
  MAX_UTILIZATION: "maxUtilization",
  HUBS_AND_FACILITIES: "hubsAndFacilities",
  REQUEST_LOG: "requestLog"
};

interface Props {
  capacityTypesData: CapacityTypeListQuery | undefined;
  facilitiesData: FacilityListQuery | undefined;
  hubsData: HubListQuery | undefined;
  operatorsData: OperatorListQuery | undefined;
  regionsData: RegionsWithHubsQuery | undefined;
  usagesData: UsageListQuery | undefined;
}

const Reports: React.FC<Props> = ({
  capacityTypesData,
  facilitiesData,
  hubsData,
  operatorsData,
  regionsData,
  usagesData
}) => {
  const { openLoginModal } = useLoginModalContext();
  const { t } = useTranslation(["reports", "commons"]);
  const [
    downloadFacilityUsageReport,
    { error: facilityUsageReportError }
  ] = useFacilityUsageReportMutation();
  const [
    downloadHubsAndFacilitiesReport,
    { error: hubsAndFacilitiesReportError }
  ] = useHubsAndFacilitiesReportMutation();
  const [
    downloadMaxUtilizationReport,
    { error: maxUtilizationReportError }
  ] = useMaxUtilizationReportMutation();
  const [
    downloadRequestLogReport,
    { error: requestLogReportError }
  ] = useRequestLogReportMutation();
  const now = new Date();
  const year = now.getFullYear();
  const month = now.getMonth();

  const intervalOptions: Option[] = ["5", "10", "15", "30", "60"].map(val => {
    return {
      label: `${val} min`,
      value: val
    };
  });

  const requestLogIntervalOptions: Option[] = ["HOUR", "DAY", "MONTH"].map(
    val => {
      return {
        label: t(`requestLogInterval${capitalize(val)}`),
        value: val
      };
    }
  );

  const allOperators: Operator[] =
    operatorsData && operatorsData.operators && operatorsData.operators.results
      ? operatorsData.operators.results
      : [];
  const operatorOptions: Option[] = allOperators.map(operator => {
    return {
      value: operator.id.toString(),
      label: operator.name ? operator.name.fi || "" : ""
    };
  });

  const allUsages: string[] =
    usagesData && usagesData.usages ? usagesData.usages : [];
  const usageOptions: Option[] = allUsages
    .map(usage => ({
      value: usage,
      label: translateValue("commons:usage", usage, t)
    }))
    .sort(sortOptions);

  const allCapacityTypes: string[] =
    capacityTypesData && capacityTypesData.capacityTypes
      ? capacityTypesData.capacityTypes
      : [];
  const capacityTypeOptions: Option[] = allCapacityTypes
    .map(capacityType => ({
      value: capacityType,
      label: translateValue("commons:capacityType", capacityType, t)
    }))
    .sort(sortOptions);

  const allRegions: RegionWithHubs[] =
    regionsData && regionsData.regionsWithHubs
      ? regionsData.regionsWithHubs
      : [];
  const regionOptions: Option[] = allRegions
    .map(region => ({
      value: region.id,
      label: region.name ? region.name.fi || "" : ""
    }))
    .sort(sortOptions);

  const allHubs: Hub[] =
    hubsData && hubsData.hubs && hubsData.hubs ? hubsData.hubs.hubs : [];

  const getFilteredHubs = (regionIds: string[]): Hub[] => {
    const hubIds: string[] = [];
    const newHubs: Hub[] = [];
    allRegions
      .filter(region =>
        regionIds.length ? regionIds.indexOf(region.id) !== -1 : true
      )
      .forEach(region => hubIds.push(...region.hubIds));

    hubIds.forEach(hubId => {
      const hub = allHubs.find(item => item.id.toString() === hubId);
      if (hub) {
        newHubs.push(hub);
      }
    });
    return newHubs;
  };

  const getHubOptions = (items: Hub[]): Option[] => {
    return items
      .map(hub => ({
        label: hub.name ? hub.name.fi || "" : "",
        value: hub.id.toString()
      }))
      .sort(sortOptions);
  };

  const allFacilities: Facility[] =
    facilitiesData && facilitiesData.facilities
      ? facilitiesData.facilities.facilities
      : [];

  const getFilteredFacilities = (
    regionIds: string[],
    hubIds: string[]
  ): Facility[] => {
    if (!regionIds.length && !hubIds.length) {
      return allFacilities;
    }

    const newFacilities: Facility[] = [];
    const facilityIds: number[] = [];
    const hubItems = hubIds.length
      ? hubIds
          .map(hubId => allHubs.find(item => item.id.toString() === hubId))
          .filter(hub => !!hub)
      : getFilteredHubs(regionIds);

    hubItems.forEach(hub => {
      facilityIds.push(...(hub ? hub.facilityIds : []));
    });

    facilityIds.forEach(facilityId => {
      const facility = allFacilities.find(
        item => item && item.id === facilityId
      );
      if (facility) {
        newFacilities.push(facility);
      }
    });

    return newFacilities;
  };

  const getFacilityOptions = (items: Facility[]): Option[] => {
    return items
      .map((facility: any) => ({
        label: facility.name.fi,
        value: facility.id.toString()
      }))
      .sort(sortOptions);
  };

  let filteredHubs: Hub[] = getFilteredHubs([]);
  let hubOptions: Option[] = getHubOptions(filteredHubs);
  let filteredFacilities: Facility[] = getFilteredFacilities([], []);
  let facilityOptions: Option[] = getFacilityOptions(filteredFacilities);

  const addWaitClass = () => {
    document.documentElement.classList.add("wait");
  };

  const removeWaitClass = () => {
    document.documentElement.classList.remove("wait");
  };

  const getReportFilename = (type: string, params: any): string => {
    let name = t(`${type}Label`);

    if (
      type === REPORT_TYPE.FACILITY_USAGE ||
      type === REPORT_TYPE.MAX_UTILIZATION ||
      type === REPORT_TYPE.REQUEST_LOG
    ) {
      const startDate = params.startDate;
      name += `_${startDate[0]}${("0" + startDate[1]).slice(-2)}${(
        "0" + startDate[2]
      ).slice(-2)}`;

      if (!/^\s*$/.test(params.endDate)) {
        const endDate = params.endDate;
        name += `_${endDate[0]}${("0" + endDate[1]).slice(-2)}${(
          "0" + endDate[2]
        ).slice(-2)}`;
      }
    } else if (type === REPORT_TYPE.HUBS_AND_FACILITIES) {
      const date = new Date();
      name += `_${date.getFullYear()}${("0" + (date.getMonth() + 1)).slice(
        -2
      )}${("0" + date.getDate()).slice(-2)}`;
    }

    name += ".xlsx";
    name = name.replace(/ /g, "_");

    return name;
  };

  React.useEffect(() => {
    if (facilityUsageReportError) {
      removeWaitClass();
      openLoginModal();
    }
  }, [facilityUsageReportError]);

  React.useEffect(() => {
    if (hubsAndFacilitiesReportError) {
      removeWaitClass();
      openLoginModal();
    }
  }, [hubsAndFacilitiesReportError]);

  React.useEffect(() => {
    if (maxUtilizationReportError) {
      removeWaitClass();
      openLoginModal();
    }
  }, [maxUtilizationReportError]);

  React.useEffect(() => {
    if (requestLogReportError) {
      removeWaitClass();
      openLoginModal();
    }
  }, [requestLogReportError]);

  return (
    <>
      <Breadcrumb name={t("reports")} canGoback={false} />
      <div className="container content">
        <div className="wdReportsView">
          <Formik
            initialValues={{
              capacityTypes: ["CAR"],
              endDate: now,
              facilities: [],
              hubs: [],
              interval: "60",
              operators: [],
              startDate: new Date(year, month, 1),
              regions: [],
              reportType: "facilityUsage",
              requestLogInterval: "DAY",
              usages: []
            }}
            onSubmit={async ({
              capacityTypes,
              endDate,
              facilities,
              hubs,
              interval,
              operators,
              regions,
              reportType,
              requestLogInterval,
              startDate,
              usages
            }) => {
              addWaitClass();

              switch (reportType) {
                case REPORT_TYPE.FACILITY_USAGE: {
                  const facilityUsageVariables = {
                    capacityTypes,
                    endDate: dateToArray(endDate),
                    facilities,
                    hubs,
                    interval: Number(interval),
                    operators,
                    regions,
                    startDate: dateToArray(startDate),
                    usages
                  };

                  const facilityUsageData: any = await downloadFacilityUsageReport(
                    {
                      variables: facilityUsageVariables
                    }
                  );

                  const facilityUsageResponse =
                    facilityUsageData &&
                    facilityUsageData.data &&
                    facilityUsageData.data.facilityUsageReport;

                  if (facilityUsageResponse) {
                    const filename = getReportFilename(
                      reportType,
                      facilityUsageVariables
                    );
                    saveAs(
                      new Blob([decode(facilityUsageResponse)], {
                        type: XLSXContentType
                      }),
                      filename
                    );
                  }
                  break;
                }
                case REPORT_TYPE.HUBS_AND_FACILITIES: {
                  const hubsAndFacilitiesData: any = await downloadHubsAndFacilitiesReport();

                  const hubsAndFacilitiesResponse =
                    hubsAndFacilitiesData &&
                    hubsAndFacilitiesData.data &&
                    hubsAndFacilitiesData.data.hubsAndFacilitiesReport;

                  if (hubsAndFacilitiesResponse) {
                    const filename = getReportFilename(reportType, {});
                    saveAs(
                      new Blob([decode(hubsAndFacilitiesResponse)], {
                        type: XLSXContentType
                      }),
                      filename
                    );
                  }
                  break;
                }
                case REPORT_TYPE.MAX_UTILIZATION: {
                  const maxUtilizationVariables = {
                    capacityTypes,
                    endDate: dateToArray(endDate),
                    facilities,
                    hubs,
                    operators,
                    regions,
                    startDate: dateToArray(startDate),
                    usages
                  };

                  const maxUtilizationData: any = await downloadMaxUtilizationReport(
                    {
                      variables: maxUtilizationVariables
                    }
                  );

                  const maxUtilizationResponse =
                    maxUtilizationData &&
                    maxUtilizationData.data &&
                    maxUtilizationData.data.maxUtilizationReport;

                  if (maxUtilizationResponse) {
                    const filename = getReportFilename(
                      reportType,
                      maxUtilizationVariables
                    );
                    saveAs(
                      new Blob([decode(maxUtilizationResponse)], {
                        type: XLSXContentType
                      }),
                      filename
                    );
                  }
                  break;
                }
                case REPORT_TYPE.REQUEST_LOG: {
                  const requestLogVariables = {
                    endDate: dateToArray(endDate),
                    requestLogInterval,
                    startDate: dateToArray(startDate)
                  };

                  const requestLogData: any = await downloadRequestLogReport({
                    variables: requestLogVariables
                  });

                  const requestLogResponse =
                    requestLogData &&
                    requestLogData.data &&
                    requestLogData.data.requestLogReport;

                  if (requestLogResponse) {
                    const filename = getReportFilename(
                      reportType,
                      requestLogVariables
                    );
                    saveAs(
                      new Blob([decode(requestLogResponse)], {
                        type: XLSXContentType
                      }),
                      filename
                    );
                  }
                  break;
                }
              }

              removeWaitClass();
            }}
          >
            {({ setFieldValue, values }) => {
              const { reportType } = values;

              const showField = (controlName: string): boolean => {
                return Fields[reportType].indexOf(controlName) !== -1;
              };

              const handleFieldChange = (name: string, value: any) => {
                switch (name) {
                  case "endDate":
                    if (value && value < values.startDate) {
                      setFieldValue("startDate", value);
                    }
                    setFieldValue(name, value);
                    break;
                  case "hubs": {
                    filteredFacilities = getFilteredFacilities(
                      values.regions,
                      value
                    );
                    facilityOptions = getFacilityOptions(filteredFacilities);
                    const newFacilities = values.facilities.filter(
                      facility =>
                        facilityOptions.findIndex(
                          (item: any) => item.value === facility
                        ) !== -1
                    );

                    setFieldValue("facilities", newFacilities);
                    setFieldValue("hubs", value);
                    break;
                  }
                  case "regions": {
                    filteredHubs = getFilteredHubs(value);
                    hubOptions = getHubOptions(filteredHubs);
                    const newHubs = values.hubs.filter(
                      hub =>
                        hubOptions.findIndex(
                          (item: any) => item.value === hub
                        ) !== -1
                    );
                    filteredFacilities = getFilteredFacilities(value, newHubs);
                    facilityOptions = getFacilityOptions(filteredFacilities);
                    const newFacilities = values.facilities.filter(
                      facility =>
                        facilityOptions.findIndex(
                          (item: any) => item.value === facility
                        ) !== -1
                    );

                    setFieldValue("hubs", newHubs);
                    setFieldValue("facilities", newFacilities);
                    setFieldValue("regions", value);
                    break;
                  }
                  case "startDate":
                    if (value && value > values.endDate) {
                      setFieldValue("endDate", value);
                    }
                    setFieldValue(name, value);
                    break;
                }
              };

              return (
                <Form>
                  <Info />
                  <div className="row">
                    <RadioButtonField
                      checked={values.reportType === REPORT_TYPE.FACILITY_USAGE}
                      name="reportType"
                      label={t("facilityUsageLabel")}
                      title={t("facilityUsageTitle")}
                      value="facilityUsage"
                    />
                    <br />
                    <RadioButtonField
                      checked={
                        values.reportType === REPORT_TYPE.MAX_UTILIZATION
                      }
                      name="reportType"
                      label={t("maxUtilizationLabel")}
                      title={t("maxUtilizationTitle")}
                      value="maxUtilization"
                    />
                    <br />
                    <RadioButtonField
                      checked={
                        values.reportType === REPORT_TYPE.HUBS_AND_FACILITIES
                      }
                      name="reportType"
                      label={t("hubsAndFacilitiesLabel")}
                      title={t("hubsAndFacilitiesTitle")}
                      value="hubsAndFacilities"
                    />
                    <br />
                    <RadioButtonField
                      checked={values.reportType === REPORT_TYPE.REQUEST_LOG}
                      name="reportType"
                      label={t("requestLogLabel")}
                      title={t("requestLogTitle")}
                      value="requestLog"
                    />
                  </div>

                  <div className="row smallMarginBottom">
                    <h3 className="wdReportHeading">
                      {t(`${reportType}Label`)}
                    </h3>
                    <div className="row">
                      <p>{t(`${reportType}Info`)}</p>
                    </div>
                  </div>

                  {showField(FIELDS.RANGE) && (
                    <>
                      <div className="row">
                        <label>{t("labelRange")} *</label>
                      </div>
                      <div className="row">
                        <div className="column2 first-column">
                          <div className="form-group">
                            <DatePickerField
                              className="wdStartDate"
                              isClearable={false}
                              maxDate={new Date()}
                              name="startDate"
                              onChange={handleFieldChange}
                              value={values.startDate}
                            />
                            {" - "}
                            <DatePickerField
                              className="wdEndDate"
                              isClearable={false}
                              maxDate={new Date()}
                              name="endDate"
                              onChange={handleFieldChange}
                              value={values.endDate}
                            />
                          </div>
                        </div>
                      </div>
                    </>
                  )}

                  {showField(FIELDS.INTERVAL) && (
                    <>
                      <div className="row">
                        <label>{t("labelInterval")}</label>
                      </div>
                      <div className="row">
                        <div className="column3 first-column">
                          <div className="form-group wdInterval">
                            <SelectField
                              className="dateInput wdInterval"
                              isClearacle={false}
                              name="interval"
                              onChange={setFieldValue}
                              options={intervalOptions}
                              value={values.interval}
                            />
                          </div>
                        </div>
                      </div>
                    </>
                  )}

                  {showField(FIELDS.REGUEST_LOG_INTERVAL) && (
                    <>
                      <div className="row">
                        <label>{t("labelRequestLogInterval")}</label>
                      </div>
                      <div className="row">
                        <div className="column3 first-column">
                          <div className="form-group wdInterval">
                            <SelectField
                              className="dateInput wdInterval"
                              isClearacle={false}
                              name="requestLogInterval"
                              onChange={setFieldValue}
                              options={requestLogIntervalOptions}
                              value={values.requestLogInterval}
                            />
                          </div>
                        </div>
                      </div>
                    </>
                  )}

                  {showField(FIELDS.OPERATOR) && (
                    <>
                      <div className="row">
                        <label>{t("labelOperator")}</label>
                      </div>
                      <div className="row">
                        <div className="column3 first-column">
                          <div className="form-group wdOperators">
                            <SelectField
                              className="wdOperators"
                              isClearacle={false}
                              isMulti={true}
                              name="operators"
                              onChange={setFieldValue}
                              options={operatorOptions}
                              placeholder={t("placeholderOperator")}
                              value={values.operators}
                            />
                          </div>
                        </div>
                      </div>
                    </>
                  )}

                  {showField(FIELDS.USAGES) && (
                    <>
                      <div className="row">
                        <label>{t("labelUsage")}</label>
                      </div>
                      <div className="row">
                        <div className="column3 first-column">
                          <div className="form-group wdUsages">
                            <SelectField
                              className="wdUsages"
                              isClearacle={false}
                              isMulti={true}
                              name="usages"
                              onChange={setFieldValue}
                              options={usageOptions}
                              placeholder={t("placeholderUsage")}
                              value={values.usages}
                            />
                          </div>
                        </div>
                      </div>
                    </>
                  )}

                  {showField(FIELDS.CAPACITY_TYPES) && (
                    <>
                      <div className="row">
                        <label>{t("labelCapacity")}</label>
                      </div>
                      <div className="row">
                        <div className="column3 first-column">
                          <div className="form-group wdCapacityTypes">
                            <SelectField
                              className="wdCapacityTypes"
                              isClearacle={false}
                              isMulti={true}
                              name="capacityTypes"
                              onChange={setFieldValue}
                              options={capacityTypeOptions}
                              placeholder={t("placeholderCapacity")}
                              value={values.capacityTypes}
                            />
                          </div>
                        </div>
                      </div>
                    </>
                  )}

                  {showField(FIELDS.REGIONS) && (
                    <>
                      <div className="row">
                        <label>{t("labelRegion")}</label>
                      </div>
                      <div className="row">
                        <div className="column3 first-column">
                          <div className="form-group wdRegions">
                            <SelectField
                              className="wdRegions"
                              isClearacle={false}
                              isMulti={true}
                              name="regions"
                              onChange={handleFieldChange}
                              options={regionOptions}
                              placeholder={t("placeholderRegion")}
                              value={values.regions}
                            />
                          </div>
                        </div>
                      </div>
                    </>
                  )}

                  {showField(FIELDS.HUBS) && (
                    <>
                      <div className="row">
                        <label>{t("labelHub")}</label>
                      </div>
                      <div className="row">
                        <div className="column3 first-column">
                          <div className="form-group wdHubs">
                            <SelectField
                              className="wdHubs"
                              isClearacle={false}
                              isMulti={true}
                              name="hubs"
                              onChange={handleFieldChange}
                              options={hubOptions}
                              placeholder={t("placeholderHub")}
                              value={values.hubs}
                            />
                          </div>
                        </div>
                      </div>
                    </>
                  )}

                  {showField(FIELDS.FACILITIES) && (
                    <>
                      <div className="row">
                        <label>{t("labelFacility")}</label>
                      </div>
                      <div className="row">
                        <div className="column3 first-column">
                          <div className="form-group wdFacilities">
                            <SelectField
                              className="wdFacilities"
                              isClearacle={false}
                              isMulti={true}
                              name="facilities"
                              onChange={setFieldValue}
                              options={facilityOptions}
                              placeholder={t("placeholderFacility")}
                              value={values.facilities}
                            />
                          </div>
                        </div>
                      </div>
                    </>
                  )}

                  <div className="row smallMargin">
                    <p>{t("labelNotice")}</p>
                    <button type="submit" className="btn btn-s">
                      {t("buttonLoad")}
                    </button>
                  </div>
                </Form>
              );
            }}
          </Formik>
        </div>
      </div>
    </>
  );
};

export default Reports;
