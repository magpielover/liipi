import * as React from "react";
import { useTranslation } from "react-i18next";
import {
  useCapacityTypeListQuery,
  useFacilityListQuery,
  useHubListQuery,
  useOperatorListQuery,
  useRegionsWithHubsQuery,
  useUsageListQuery
} from "../../generated/graphql";
import Reports from "./Reports";

const ReportsPage = () => {
  const { t } = useTranslation("commons");
  const {
    data: capacityTypesData,
    error: capacityTypesError,
    loading: loadingCapacityTypes
  } = useCapacityTypeListQuery();
  const {
    data: facilitiesData,
    error: facilitiesErrors,
    loading: loadingFacilities
  } = useFacilityListQuery();
  const {
    data: hubsData,
    error: hubsErrors,
    loading: loadingHubs
  } = useHubListQuery();
  const {
    data: operatorsData,
    error: operatorsError,
    loading: loadingOperators
  } = useOperatorListQuery();
  const {
    data: regionsData,
    error: regionsError,
    loading: loadingRegions
  } = useRegionsWithHubsQuery();
  const {
    data: usagesData,
    error: usagesError,
    loading: loadingUsages
  } = useUsageListQuery();

  const loading =
    loadingCapacityTypes ||
    loadingFacilities ||
    loadingHubs ||
    loadingOperators ||
    loadingRegions ||
    loadingUsages;
  const error =
    !!capacityTypesError ||
    !!facilitiesErrors ||
    !!hubsErrors ||
    !!operatorsError ||
    !!regionsError ||
    !!usagesError;

  if (loading) {
    return <div>{t("loading")}</div>;
  }

  if (error) {
    return <div />;
  }

  return (
    <Reports
      capacityTypesData={capacityTypesData}
      facilitiesData={facilitiesData}
      hubsData={hubsData}
      operatorsData={operatorsData}
      regionsData={regionsData}
      usagesData={usagesData}
    />
  );
};

export default ReportsPage;
