import gql from "graphql-tag";

export const QUERY_REPORT = gql`
  mutation FacilityUsageReport(
    $capacityTypes: [String!]
    $endDate: [Int!]
    $facilities: [Int!]
    $hubs: [Int!]
    $interval: Int
    $operators: [Int!]
    $regions: [Int!]
    $startDate: [Int!]
    $usages: [Int!]
  ) {
    facilityUsageReport(
      capacityTypes: $capacityTypes
      endDate: $endDate
      facilities: $facilities
      hubs: $hubs
      interval: $interval
      operators: $operators
      regions: $regions
      startDate: $startDate
      usages: $usages
    )
  }

  mutation HubsAndFacilitiesReport {
    hubsAndFacilitiesReport
  }

  mutation MaxUtilizationReport(
    $capacityTypes: [String!]
    $endDate: [Int!]
    $facilities: [Int!]
    $hubs: [Int!]
    $operators: [Int!]
    $regions: [Int!]
    $startDate: [Int!]
    $usages: [Int!]
  ) {
    maxUtilizationReport(
      capacityTypes: $capacityTypes
      endDate: $endDate
      facilities: $facilities
      hubs: $hubs
      operators: $operators
      regions: $regions
      startDate: $startDate
      usages: $usages
    )
  }

  mutation RequestLogReport(
    $endDate: [Int!]
    $requestLogInterval: String!
    $startDate: [Int!]
  ) {
    requestLogReport(
      endDate: $endDate
      requestLogInterval: $requestLogInterval
      startDate: $startDate
    )
  }
`;
