import * as React from "react";
import useAuth from "../../util/useAuth";
import { useLoginModalContext } from "../Commons/LoginModal";
import ReportsPage from "./ReportsPage";

const ReportsContainer = ({}) => {
  const [showReportsPage, setShowReportsPage] = React.useState(false);
  const { isTokenExpired, isUserLoggedIn, loginState } = useAuth();
  const { openLoginModal } = useLoginModalContext();

  React.useEffect(() => {
    if (!isTokenExpired()) {
      setShowReportsPage(true);
    }
  }, [loginState]);
  React.useEffect(() => {
    if (isTokenExpired() || !isUserLoggedIn()) {
      openLoginModal();
    }
  }, []);

  if (!showReportsPage) {
    return null;
  }

  return <ReportsPage />;
};

export default ReportsContainer;
