import * as React from "react";
import { useTranslation } from "react-i18next";

const Info: React.FC<any> = () => {
  const { t } = useTranslation("reports");

  return (
    <div className="row smallMarginBottom">
      <p>{t("infoLead")}</p>
      <ul>
        <li>{t("infoBullet1")}</li>
        <li>{t("infoBullet2")}</li>
      </ul>
    </div>
  );
};

export default Info;
