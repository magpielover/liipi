import gql from "graphql-tag";

export const QUERY_OPERATOR_LIST = gql`
  query OperatorList {
    operators {
      results {
        id
        name {
          fi
          en
          sv
        }
      }
      hasMore
    }
  }
`;
