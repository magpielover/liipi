import * as React from "react";
import { useTranslation } from "react-i18next";
import {
  OperatorListQuery,
  useOperatorListQuery
} from "../../generated/graphql";
import OperatorList from "./OperatorList";
interface Props {
  ids: number[];
  data: OperatorListQuery;
}
const OperatorListContainer = (props: Props) => {
  const { t } = useTranslation("commons");
  const { data, error, loading } = useOperatorListQuery({
    variables: { ids: props.ids }
  });

  if (loading) {
    return <div>{t("loading")}</div>;
  }

  if (error || !data) {
    return <div />;
  }

  return (
    <React.Fragment>
      <OperatorList ids={props.ids} data={data} />
    </React.Fragment>
  );
};
export default OperatorListContainer;
