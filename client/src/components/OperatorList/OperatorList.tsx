import React, { useState } from "react";
import { useTranslation } from "react-i18next";
import Modal from "react-modal";
import { OperatorListQuery } from "../../generated/graphql";
import Breadcrumb from "../Commons/Breadcrumb";
import Info from "./Info";
import OperatorModal from "./OperatorModal";

interface Props {
  data: OperatorListQuery;
  ids: number[];
}

const className = "OperatorList";
Modal.setAppElement("#root");

const OperatorList: React.FC<Props> = ({ data }) => {
  const initialState = {
    showModal: false,
    operatorÎd: 0
  };
  const [state, setState] = useState(initialState);
  const { t } = useTranslation("operatorList");
  const handleOpenModal = (operatorÎd: any) => {
    setState({ showModal: true, operatorÎd });
  };

  const handleCloseModal = (operatorÎd: any) => {
    setState({ showModal: false, operatorÎd });
  };
  return (
    <React.Fragment>
      <Breadcrumb name={t("operators")} canGoback={false} />
      <div className="container content">
        <Info />
        <div className="panel panel-default wdOperatorsView">
          <table className="table table-bordered table-condensed">
            <thead>
              <tr>
                <th>{t("labelNimi")}</th>
              </tr>
            </thead>
            <tbody>
              {!!data.operators &&
                !!data.operators.results &&
                data.operators.results.map(
                  (operator: any, i: any) =>
                    !!operator && (
                      <tr key={operator.id} className={`${className}__item`}>
                        <td>
                          <button
                            className="link-button"
                            onClick={() => handleOpenModal(operator.id)}
                          >
                            {operator.name.fi}{" "}
                          </button>
                        </td>
                      </tr>
                    )
                )}
            </tbody>
          </table>
        </div>
        {state.showModal ? (
          <OperatorModal
            isOpen={state.showModal}
            operatorId={state.operatorÎd}
            onClose={handleCloseModal}
          />
        ) : null}
      </div>
    </React.Fragment>
  );
};
export default OperatorList;
