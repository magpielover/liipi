import React, { useState } from "react";
import { useTranslation } from "react-i18next";
import { Operator, useOperatorDetailsQuery } from "../../generated/graphql";
import { getLocalizedName } from "../../util/helpers";
import Modal from "../Commons/Modal";
import ModalBody from "../Commons/ModalBody";
import ModalContent from "../Commons/ModalContent";
import ModalFooter from "../Commons/ModalFooter";
import ModalHeader from "../Commons/ModalHeader";
import "./styles.css";
interface Props {
  operatorId: number;
  isOpen: boolean;
  onClose: any;
}

const OperatorModal: React.FC<Props> = (props: Props) => {
  const initialState = {
    error: false,
    operatorSelected: false
  };
  const [state, setState] = useState(initialState);
  const { t } = useTranslation(["operatorList", "commons"]);
  const { data, error, loading } = useOperatorDetailsQuery({
    variables: { id: props.operatorId.toString() }
  });

  if (loading) {
    return <div>{t("commons:loading")}</div>;
  }

  if (error || !data || !data.operatorDetails) {
    return <div />;
  }
  const operatorDetails: Operator = data.operatorDetails;

  const handleCloseModal = () => {
    props.onClose();
  };

  return (
    <React.Fragment>
      <Modal isOpen={props.isOpen}>
        <ModalContent>
          <div id="operatorEditModal">
            <form
              name="editOperatorForm"
              className="form-horizontal"
              role="form"
            >
              <ModalHeader title={t("editProfile")} />
              <ModalBody>
                {state.error ? <div>{t("error")}</div> : <div />}

                <div className="row">
                  <label>{t("labelNimi")} *</label>
                </div>
                <div className="row">
                  <div className="column3 first-column">
                    <div className="form-group">
                      <div className="input-group">
                        <input
                          name="nameFi"
                          type="text"
                          className="form-control"
                          value={getLocalizedName(operatorDetails.name, "fi")}
                        />
                        <span className="input-group-addon ng-binding">
                          fi&nbsp;
                        </span>
                      </div>
                    </div>
                  </div>
                  <div className="column3">
                    <div className="form-group">
                      <div className="input-group">
                        <input
                          name="nameSv"
                          type="text"
                          className="form-control"
                          value={getLocalizedName(operatorDetails.name, "sv")}
                        />
                        <span className="input-group-addon ng-binding">sv</span>
                      </div>
                    </div>
                  </div>
                  <div className="column3 last-column">
                    <div className="form-group">
                      <div className="input-group">
                        <input
                          name="nameEn"
                          type="text"
                          className="form-control"
                          value={getLocalizedName(operatorDetails.name, "en")}
                        />
                        <span className="input-group-addon ng-binding">en</span>
                      </div>
                    </div>
                  </div>
                </div>
              </ModalBody>
              <ModalFooter>
                <div className="row">
                  <div className="last-column">
                    <button
                      type="button"
                      id="wdOperatorCancel"
                      className="btn btn-xs-secondary ng-binding"
                      onClick={handleCloseModal}
                    >
                      {t("cancel")}
                    </button>
                    <button
                      type="submit"
                      id="wdUserModalOk"
                      className="btn btn-xs link-button"
                    >
                      Valmis
                    </button>
                  </div>
                </div>
              </ModalFooter>
            </form>
          </div>
        </ModalContent>
      </Modal>
    </React.Fragment>
  );
};
export default OperatorModal;
