import * as React from "react";

interface Props {
  title?: string;
}

const ModalHeader: React.FC<Props> = ({ title }) => {
  return (
    <div className="modal-header">
      <h3 className="modal-title">{title}</h3>
    </div>
  );
};
export default ModalHeader;
