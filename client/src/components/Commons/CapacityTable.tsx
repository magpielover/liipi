import * as React from "react";
import { useTranslation } from "react-i18next";
import { Capacity } from "../../generated/graphql";
import { translateValue } from "../../util/translateUtils";

interface Props {
  data: Capacity;
}

const CapacityTable: React.FC<Props> = ({ data }) => {
  const { t } = useTranslation("commons");

  return (
    <table className="table table-bordered table-striped table-condensed wdCapacityTable">
      <thead>
        <tr>
          <th>
            <span>{t("labelCapacityType")}</span>
          </th>
          <th>
            <span>{t("labelBuiltCapacity")}</span>
          </th>
        </tr>
      </thead>
      <tbody>
        {Object.keys(data).map(key => (
          <tr key={key}>
            <td>{translateValue("capacityType", key, t)}</td>
            <td>{(data as any)[key]}</td>
          </tr>
        ))}
      </tbody>
    </table>
  );
};

export default CapacityTable;
