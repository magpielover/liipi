import * as React from "react";
import renderer from "react-test-renderer";
import Navbar from "./Navbar";

test("Navbar matches snapshot", () => {
  const component = renderer.create(<Navbar tab="hubs" />);
  const tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});

export {};
