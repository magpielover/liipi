import * as React from "react";
import renderer from "react-test-renderer";
import Breadcrumb from "./Breadcrumb";

test("Breadcrumb matches snapshot", () => {
  const component = renderer.create(<Breadcrumb name="foo" />);
  const tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});

export {};
