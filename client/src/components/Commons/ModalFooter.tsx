import * as React from "react";

interface Props {
  children: any;
}

const ModalFooter: React.FC<Props> = ({ children }) => {
  return <div className="modal-footer">{children}</div>;
};

export default ModalFooter;
