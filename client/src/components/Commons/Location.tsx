import { GeoJsonObject } from "geojson";
import L from "leaflet";
import * as React from "react";
import { useTranslation } from "react-i18next";
import { GeoJSON, Map, TileLayer } from "react-leaflet";
import { Link } from "react-router-dom";
import PortModal from "../FacilityProfile/PortModal";

export type LatLngTuple = [number, number];
interface Props {
  bounds: LatLngTuple[] | undefined;
  center: LatLngTuple | undefined;
  geojson: GeoJsonObject;
  hubs: object[] | undefined;
  zoom: number;
}

const defaultCenter: any = [60.173324, 24.941025];

const coordsToLatLng: any = (coords: any) => [coords[1], [coords[0]]];

const Location: React.FC<Props> = ({ bounds, center, geojson, hubs, zoom }) => {
  const [isModalOpen, setIsModalOpen] = React.useState(false);
  const [port, setPort] = React.useState(null);

  const { t } = useTranslation("commons");

  const handleClickPort = (e: any, properties: any) => {
    setPort(properties);
    setIsModalOpen(true);
  };

  const handleCloseModal = () => {
    setIsModalOpen(false);
  };

  return (
    <div>
      <PortModal isOpen={isModalOpen} onClose={handleCloseModal} port={port} />
      <div className="row">
        <h3>{t("locationPosition")}</h3>
        {Array.isArray(hubs) && !!hubs.length && (
          <>
            <h4>{t("locationBelongsToArea")}</h4>
            <div className="row smallMarginBottom">
              {hubs.map((hub: any, i: any) => (
                <p key={i}>
                  <Link to={`/hub/${hub.id}`}>{hub.name.fi}</Link>
                </p>
              ))}
            </div>
          </>
        )}
      </div>
      <link
        rel="stylesheet"
        href="https://unpkg.com/leaflet@1.5.1/dist/leaflet.css"
      />
      <Map
        bounds={bounds}
        center={center || defaultCenter}
        maxZoom={19}
        zoom={zoom}
        zoomAnimation={false}
      >
        <TileLayer
          attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
          url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
        />
        <GeoJSON
          data={geojson}
          coordsToLatLng={coordsToLatLng}
          onEachFeature={(feature: any, layer: any) => {
            if (feature.hasOwnProperty("properties")) {
              layer.on({
                click: (e: any) => handleClickPort(e, feature.properties)
              });
            }
          }}
          pointToLayer={(feature, latlng) => {
            const markerOptions = {
              radius: 8,
              fillColor: "#4DC7FF",
              weight: 0,
              fillOpacity: 1
            };
            if (feature.hasOwnProperty("properties")) {
              const properties = feature.properties;
              const iconUrl =
                (properties.entry ? "/entry-" : "/noentry-") +
                (properties.exit ? "exit-" : "noexit-") +
                (properties.pedestrian ? "pedestrian-" : "nopedestrian-") +
                (properties.bicycle ? "bicycle.gif" : "nobicycle.gif");
              const icon = L.icon({
                iconUrl,
                iconSize: [40, 40],
                iconAnchor: [20, 40],
                popupAnchor: [0, 0]
              });
              return L.marker(latlng, { icon });
            }
            return L.circleMarker(latlng, markerOptions);
          }}
        />
      </Map>
    </div>
  );
};

export default Location;
