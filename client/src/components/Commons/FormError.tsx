import React from "react";
import "./styles.css";

interface Props {
  errorMessage: string;
}
const FormError: React.FC<Props> = ({ errorMessage }) => {
  return (
    <React.Fragment>
      <div className="row errorRow">
        <div className="wdViolation">
          <span className="wdViolationMessage">
            <span>{errorMessage}</span>
          </span>
        </div>
      </div>
    </React.Fragment>
  );
};
export default FormError;
