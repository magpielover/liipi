import { Field } from "formik";
import * as React from "react";

interface Props {
  checked: boolean;
  label: string;
  name: string;
  title: string;
  value: string;
}

const RadioButton: React.FC<Props> = ({
  checked,
  label,
  name,
  title,
  value
}) => {
  return (
    <label className="radiobutton-label" title={title}>
      <Field checked={checked} type="radio" name={name} value={value} />
      {` ${label}`}
    </label>
  );
};

export default RadioButton;
