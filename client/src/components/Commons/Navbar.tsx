import React, { MouseEvent, useState } from "react";
import { useTranslation } from "react-i18next";
import { RouteComponentProps, withRouter } from "react-router";
import { NavLink } from "react-router-dom";
import useAuth from "../../util/useAuth";
import { useLoginModalContext } from "./LoginModal";
import "./styles.css";

const TABS = {
  CONTACTS: "/contacts",
  HUBS: "/hubs",
  OPERATORS: "/operators",
  REPORTS: "/reports",
  USERS: "/users"
};

const Navbar: React.FC<RouteComponentProps> = ({ history }) => {
  const {
    deleteLoginState,
    isTokenExpired,
    isUserLoggedIn,
    loginState
  } = useAuth();
  const [nextTab, setNextTab] = useState();
  const loggedIn = isUserLoggedIn();
  const { openLoginModal } = useLoginModalContext();

  const { t } = useTranslation("navbar");

  const handleLogout = () => {
    deleteLoginState();
  };

  const handleNavLinkClick = (event: MouseEvent, tab: string) => {
    if (tab === TABS.REPORTS || tab === TABS.USERS) {
      const tokenExpired = isTokenExpired();
      if (tokenExpired) {
        event.preventDefault();
        openLoginModal();
      }
    }

    setNextTab(tab);
  };

  React.useEffect(() => {
    if (loginState) {
      history.push(nextTab);
    } else {
      history.push(TABS.HUBS);
    }
  }, [loginState]);

  return (
    <React.Fragment>
      <div className="page-header navbar-static-top">
        <div className="container">
          <div className="row">
            <div className="logo-column first-column">
              <img
                className="img-responsive logo"
                src="https://p.hsl.fi/assets/hsl-liipi-logo2.png"
                alt="HSL Liityntäpysäköinti"
              />
            </div>
            <div className="navi-col">
              <ul className="nav nav-tabs">
                <li>
                  <NavLink
                    to="/hubs"
                    activeClassName="active"
                    onClick={event => {
                      handleNavLinkClick(event, TABS.HUBS);
                    }}
                  >
                    {t("facilities")}
                  </NavLink>
                </li>
                <li>
                  <NavLink
                    to="/contacts"
                    activeClassName="active"
                    onClick={event => {
                      handleNavLinkClick(event, TABS.CONTACTS);
                    }}
                  >
                    {t("contacts")}
                  </NavLink>
                </li>
                <li>
                  <NavLink
                    to="/operators"
                    activeClassName="active"
                    onClick={event => {
                      handleNavLinkClick(event, TABS.OPERATORS);
                    }}
                  >
                    {t("operators")}
                  </NavLink>
                </li>
                <li>
                  <NavLink
                    to="/users"
                    activeClassName="active"
                    onClick={event => {
                      handleNavLinkClick(event, TABS.USERS);
                    }}
                  >
                    {t("users")}
                  </NavLink>
                </li>
                <li>
                  <NavLink
                    to="/reports"
                    activeClassName="active"
                    onClick={event => {
                      handleNavLinkClick(event, TABS.REPORTS);
                    }}
                  >
                    {t("reports")}
                  </NavLink>
                </li>
              </ul>
            </div>
            <div className="logout last-column">
              <div className="pull-right">
                <NavLink
                  to="/about"
                  className="link-btn"
                  activeClassName="active link-btn"
                >
                  {t("tietoPalvelusta")}
                </NavLink>

                <NavLink
                  to="/about-en"
                  className="link-btn"
                  activeClassName="active link-btn"
                >
                  In English
                </NavLink>
                {!loggedIn ? (
                  <button
                    id="openLoginPrompt"
                    type="button"
                    className="link-btn "
                    onClick={openLoginModal}
                  >
                    {t("login")}
                  </button>
                ) : (
                  <button
                    id="logout"
                    type="button"
                    className="link-btn "
                    onClick={handleLogout}
                  >
                    {t("logout")}
                  </button>
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};
export default withRouter(Navbar);
