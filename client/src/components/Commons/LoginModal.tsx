import { Field, Form, Formik } from "formik";
import React, { useEffect } from "react";
import { useTranslation } from "react-i18next";
import { useLoginMutation } from "../../generated/graphql";
import useAuth from "../../util/useAuth";
import Modal from "./Modal";
import ModalBody from "./ModalBody";
import ModalContent from "./ModalContent";
import ModalFooter from "./ModalFooter";
import ModalHeader from "./ModalHeader";
import "./styles.css";

interface LoginModalContext {
  closeLoginModal: () => void;
  isLoginModalOpen: boolean;
  openLoginModal: () => void;
}

export const LoginModalContext = React.createContext<LoginModalContext>({
  closeLoginModal: () => null,
  isLoginModalOpen: false,
  openLoginModal: () => null
});

export const LoginModalProvider: React.FC<{ children: React.ReactNode }> = ({
  children
}) => {
  const [isLoginModalOpen, setIsModalOpen] = React.useState(false);

  const closeLoginModal = () => {
    setIsModalOpen(false);
  };

  const openLoginModal = () => {
    setIsModalOpen(true);
  };

  return (
    <LoginModalContext.Provider
      value={{
        closeLoginModal,
        isLoginModalOpen,
        openLoginModal
      }}
    >
      {children}
    </LoginModalContext.Provider>
  );
};

export const useLoginModalContext = (): LoginModalContext =>
  React.useContext(LoginModalContext);

interface Props {
  isOpen: boolean;
  onClose: any;
}

const LoginModal: React.FC<Props> = (props: Props) => {
  const [login, { data: loginData, error }] = useLoginMutation();
  const { t } = useTranslation("login");
  const { setLoginState } = useAuth();

  const handleCloseModal = () => {
    props.onClose();
  };

  useEffect(() => {
    if (!!loginData && !!loginData.login) {
      setLoginState(loginData.login);
      handleCloseModal();
    }
  });

  const handleSubmit = (values: any) => {
    login({ variables: values });
  };

  return (
    <React.Fragment>
      <Modal
        isOpen={props.isOpen}
        contentLabel="Modal"
        className="modal-dialog"
      >
        <ModalContent>
          <Formik
            initialValues={{ password: "", username: "" }}
            onSubmit={(values: any) => {
              handleSubmit(values);
            }}
          >
            <Form className="form-horizontal" role="form">
              <ModalHeader title={t("login")} />
              <ModalBody>
                {error ? (
                  <div className="row errorRow" ng-show="loginError">
                    <div className="wdViolation">
                      <span className="wdViolationMessage ">
                        {t("credentialsError")}
                      </span>
                    </div>
                  </div>
                ) : null}

                <div className="row">
                  <div className="column3 first-column">
                    <div className="form-group">
                      <label>{t("username")}</label>
                      <div>
                        <Field
                          name="username"
                          type="text"
                          className="form-control"
                        />
                      </div>
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="column3 first-column">
                    <div className="form-group">
                      <label>{t("password")}</label>
                      <div>
                        <Field
                          name="password"
                          type="password"
                          className="form-control"
                        />
                      </div>
                    </div>
                  </div>
                </div>
              </ModalBody>
              <ModalFooter>
                <div className="row">
                  <div className="last-column">
                    <button
                      type="button"
                      className="btn btn-xs-secondary wdCancel "
                      onClick={handleCloseModal}
                    >
                      {t("cancel")}
                    </button>
                    <button type="submit" className="btn btn-xs ">
                      {t("ready")}
                    </button>
                  </div>
                </div>
              </ModalFooter>
            </Form>
          </Formik>
        </ModalContent>
      </Modal>
    </React.Fragment>
  );
};
export default LoginModal;
