import gql from "graphql-tag";

export const MUTATION_LOGIN = gql`
  mutation Login($username: String, $password: String) {
    login(password: $password, username: $username) {
      operatorId
      passwordExpireInDays
      permissions
      role
      token
      userId
      username
    }
  }
`;
