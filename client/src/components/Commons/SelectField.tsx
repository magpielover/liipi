import * as React from "react";
import { useTranslation } from "react-i18next";
import Select from "react-select";

export interface Option {
  label: string;
  value: string;
}

type Value = string | number | null;

interface Props {
  className?: string;
  isClearacle?: boolean;
  isMulti?: boolean;
  name: string;
  onChange: (name: string, value: string | string[] | null) => void;
  options: Option[];
  placeholder?: string;
  value: Value | Value[];
}

const SelectField: React.FC<Props> = ({
  className,
  isClearacle = true,
  isMulti = false,
  name,
  onChange,
  options,
  placeholder,
  value
}) => {
  const handleChange = (val: any) => {
    if (isMulti) {
      const items: string[] = Array.isArray(val)
        ? val.map(item => (item ? item.value : null))
        : [];

      onChange(name, items);
    } else {
      onChange(name, val ? val.value : null);
    }
  };

  const { t } = useTranslation("commons");

  const getValue = (val: Value | Value[]) => {
    if (isMulti) {
      return Array.isArray(val)
        ? val.map(item => options.find(option => option.value === item))
        : [];
    }
    return value ? options.find(option => option.value === value) : null;
  };

  return (
    <Select
      className={className}
      options={options}
      isClearable={isClearacle}
      isMulti={isMulti}
      onChange={handleChange}
      placeholder={placeholder || t("placeholder")}
      value={getValue(value)}
    />
  );
};

export default SelectField;
