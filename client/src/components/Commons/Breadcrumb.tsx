import * as React from "react";
import { useTranslation } from "react-i18next";
import { Link } from "react-router-dom";

interface Props {
  name: any;
  canGoback?: boolean;
  canAddUser?: boolean;
  onClickNewUser?: () => void;
}
const Breadcrumb: React.FC<Props> = (props: Props) => {
  const { t } = useTranslation(["commons", "userList"]);

  return (
    <>
      <div className="row navi-above">
        <div className="container">
          <div className="secondary-navi-column first-column">
            <h2>
              <span className="wdFacilityNameFi">{props.name}</span>
            </h2>
          </div>
          {props.canGoback ? (
            <React.Fragment>
              <div className="secondary-navi-column navi-column-responsive text-right last-column">
                <Link className="btn btn-s btn-responsive" to="/hubs">
                  {t("breadcrumbBack")}
                </Link>
              </div>
            </React.Fragment>
          ) : null}
          {props.canAddUser ? (
            <React.Fragment>
              <div className="secondary-navi-column navi-column-responsive text-right last-column">
                <button
                  className="btn btn-s btn-responsive link-button"
                  role="button"
                  ui-sref="hub-list"
                  onClick={props.onClickNewUser}
                >
                  {t("userList:addNewUser")}
                </button>
              </div>
            </React.Fragment>
          ) : null}
        </div>
      </div>
    </>
  );
};

export default Breadcrumb;
