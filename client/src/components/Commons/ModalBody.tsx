import * as React from "react";

interface Props {
  children: any;
}

export const ModalBody: React.FC<Props> = ({ children }) => {
  return <div className="modal-body">{children}</div>;
};

export default ModalBody;
