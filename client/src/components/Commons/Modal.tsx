import React from "react";
import ReactModal from "react-modal";
import { classNames } from "react-select/src/utils";

interface Props {
  children: any;
  contentLabel?: string;
  isOpen: boolean;
  bottom?: string;
  top?: string;
  className?: string;
}
ReactModal.setAppElement("#root");

const Modal: React.FC<Props> = ({
  children,
  contentLabel,
  isOpen,
  bottom,
  top,
  className
}) => {
  return (
    <ReactModal
      isOpen={isOpen}
      contentLabel={contentLabel}
      bodyOpenClassName="modal-open"
      className={className || ""}
      style={{
        overlay: {
          zIndex: 1000,
          backgroundColor: "rgba(0, 0, 0, 0.5)"
        },
        content: {
          width: "943px",
          maxWidth: "calc(100% - 40px)",
          margin: "auto",
          padding: "0",
          top: top || "20px",
          left: "20px",
          right: "20px",
          bottom: bottom || "unset",
          overflowX: "hidden",
          overflowY: "auto"
        }
      }}
    >
      {children}
    </ReactModal>
  );
};

export default Modal;
