import * as React from "react";

interface Props {
  children: any;
}

const ModalContent: React.FC<Props> = ({ children }) => {
  return <div className="modal-content">{children}</div>;
};

export default ModalContent;
