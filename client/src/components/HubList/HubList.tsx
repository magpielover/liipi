import { flatten, forEach, map, sortBy } from "lodash";
import * as React from "react";
import { useTranslation } from "react-i18next";
import {
  Facility,
  FacilityListQuery,
  Hub as HubType,
  HubListQuery
} from "../../generated/graphql";
import Breadcrumb from "../Commons/Breadcrumb";
import FacilityList from "../FacilityList/FacilityList";
import Hub from "./Hub";
import Info from "./Info";
import "./styles.css";

interface HubWithFacilities {
  attachedFacilities: Facility[];
  hub: HubType;
}
interface Props {
  facilitiesData: FacilityListQuery;
  hubsData: HubListQuery;
}
const HubList: React.FC<Props> = ({ facilitiesData, hubsData }) => {
  const { t } = useTranslation("hubList");
  const [search, setSearch] = React.useState("");
  const facilities =
    facilitiesData && facilitiesData.facilities
      ? facilitiesData.facilities.facilities
      : [];
  const hubs = hubsData ? hubsData.hubs.hubs : [];

  const getAttachedFacilitiesOfHub = React.useCallback(
    (hub: HubType): Facility[] => {
      const facilityIds = hub.facilityIds;
      return facilities.filter((facility: any) => {
        return facilityIds.indexOf(facility.id) !== -1;
      });
    },
    [facilities]
  );

  const unattachedFacilityIds = React.useMemo(() => {
    const facilityIds = sortBy(flatten(map(hubs, "facilityIds")));
    return map(facilities, "id").filter(id => {
      return facilityIds.indexOf(id) === -1;
    });
  }, [facilities, hubs]);

  const unattachedFacilities = React.useMemo(() => {
    return facilities.filter(facility => {
      return unattachedFacilityIds.indexOf(facility.id) !== -1;
    });
  }, [unattachedFacilityIds, facilities]);

  const hubsWithFacilities: HubWithFacilities[] = React.useMemo(() => {
    return hubs.map(hub => {
      const attachedFacilities = getAttachedFacilitiesOfHub(hub);
      return {
        attachedFacilities,
        hub
      };
    });
  }, [hubs, getAttachedFacilitiesOfHub]);

  const getFilteredFacilities = React.useCallback(
    (items: Facility[]): Facility[] => {
      return items.filter(facility => {
        const name = facility.name ? facility.name.fi || "" : "";
        return name.toLowerCase().includes(search.toLowerCase());
      });
    },
    [search]
  );

  const filteredUnattachedFacilities = React.useMemo(() => {
    return getFilteredFacilities(unattachedFacilities);
  }, [getFilteredFacilities, unattachedFacilities]);

  const handleSearchChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setSearch(event.target.value);
  };

  const shouldShowHub = React.useCallback(
    (name: string, filteredFacilities: Facility[]): boolean => {
      return (
        name.toLowerCase().includes(search.toLowerCase()) ||
        !!filteredFacilities.length
      );
    },
    [search]
  );

  const noResults = React.useMemo(() => {
    if (filteredUnattachedFacilities.length) {
      return false;
    }
    let showNoResultsMessage = true;

    forEach(hubsWithFacilities, item => {
      const hub = item.hub;
      const name = hub.name ? hub.name.fi || "" : "";
      const attachedFacilities = item.attachedFacilities;
      const filteredFacilities = getFilteredFacilities(attachedFacilities);
      const showHub = shouldShowHub(name, filteredFacilities);

      if (showHub) {
        showNoResultsMessage = false;
        return true;
      }
    });

    return showNoResultsMessage;
  }, [
    filteredUnattachedFacilities,
    getFilteredFacilities,
    hubsWithFacilities,
    shouldShowHub
  ]);

  const className = "HubList";

  return (
    <>
      <Breadcrumb name={t("hubs")} canGoback={false} />
      <div className={"container content"}>
        <div className={className}>
          <Info />
          <div className="row">
            <div className="column2 first-column">
              <input
                className="form-control"
                type="text"
                onChange={handleSearchChange}
                placeholder={t("placeholderNoHubsOrFacilities")}
                value={search}
              />
            </div>
          </div>

          {noResults ? (
            <div className="row smallMargin">
              <p>{t("messageNoHubsOrFacilities")}</p>
            </div>
          ) : (
            <div className="panel panel-default smallMargin">
              <table className="table table-bordered table-condensed">
                <tbody>
                  <FacilityList facilities={filteredUnattachedFacilities} />
                  {hubsWithFacilities.map(item => {
                    const shouldShowHubWithFacilities = (
                      hubWithFacilities: HubWithFacilities
                    ): boolean => {
                      const hub = hubWithFacilities.hub;
                      const name = hub.name ? hub.name.fi || "" : "";
                      const filteredFacilities = getFilteredFacilities(
                        hubWithFacilities.attachedFacilities
                      );
                      return shouldShowHub(name, filteredFacilities);
                    };

                    return (
                      shouldShowHubWithFacilities(item) && (
                        <Hub
                          key={item.hub.id}
                          facilities={getFilteredFacilities(
                            item.attachedFacilities
                          )}
                          hub={item.hub}
                        />
                      )
                    );
                  })}
                </tbody>
              </table>
            </div>
          )}
        </div>
      </div>
    </>
  );
};

export default HubList;
