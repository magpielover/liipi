import gql from "graphql-tag";

export const QUERY_HUB_LIST = gql`
  query HubList {
    hubs {
      totalCount
      hubs {
        id
        name {
          fi
        }
        facilityIds
      }
      hasMore
    }
  }
`;
