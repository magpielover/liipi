import * as React from "react";
import { Facility, Hub as HubType } from "../../generated/graphql";
import FacilityList from "../FacilityList/FacilityList";

interface Props {
  facilities: Facility[];
  hub: HubType;
}

const Hub: React.FC<Props> = ({ facilities, hub }) => (
  <React.Fragment>
    <tr className="hubRow">
      <th className="wdNameFi hubRow" colSpan={4}>
        <a href={`/hub/${hub.id}`}>{hub && hub.name ? hub.name.fi : ""}</a>
      </th>
    </tr>

    <FacilityList facilities={facilities} />
  </React.Fragment>
);

export default Hub;
