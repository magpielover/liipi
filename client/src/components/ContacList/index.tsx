import * as React from "react";
import { useTranslation } from "react-i18next";
import Modal from "react-modal";
import { ContactListQuery, useContactListQuery } from "../../generated/graphql";
import ContactList from "./ContactList";
import "./styles.css";
interface Props {
  ids: number[];
  data: ContactListQuery;
}

// Make sure to bind modal to your appElement (http://reactcommunity.org/react-modal/accessibility/)
Modal.setAppElement("#root");

const ContactListContainer = (props: Props) => {
  const { t } = useTranslation("commons");
  const { data, error, loading } = useContactListQuery();

  if (loading) {
    return <div>{t("loading")}</div>;
  }

  if (error || !data) {
    return <div />;
  }

  return (
    <React.Fragment>
      <ContactList ids={props.ids} contactListItems={data} />
    </React.Fragment>
  );
};
export default ContactListContainer;
