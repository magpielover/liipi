import React, { useState } from "react";
import { useTranslation } from "react-i18next";
import Modal from "react-modal";
import {
  ContactListQuery,
  useOperatorNamesQuery
} from "../../generated/graphql";
import Breadcrumb from "../Commons/Breadcrumb";
import ContactModal from "./ContactModal";
import Info from "./Info";
interface Props {
  contactListItems: ContactListQuery;
  ids: number[];
}

Modal.setAppElement("#root");

const ContactList: React.FC<Props> = ({ contactListItems }) => {
  const [showModal, setShowModal] = useState(false);
  const [contactId, setContactId] = useState(0);

  const { data, loading, error } = useOperatorNamesQuery();
  const allOperatorNames =
    data && data.operators && data.operators.results
      ? data.operators.results.map(item => ({
          label: item && item.name && item.name.fi ? item.name.fi : "",
          value: item && item.name && item.name.fi ? item.name.fi : ""
        }))
      : [];

  const handleOpenModal = (id: any) => {
    setShowModal(true);
    setContactId(id);
  };
  const handleCloseModal = () => {
    setShowModal(false);
  };
  const { t } = useTranslation("contactList");
  const className = "ContactList";

  return (
    <React.Fragment>
      <Breadcrumb name={t("contacts")} canGoback={false} />
      <div className="container content">
        <Info />
        <div className="panel panel-default wdOperatorsView">
          <table className="table table-bordered table-condensed">
            <thead>
              <tr>
                <th>{t("labelName")}</th>
                <th>{t("labelPhone")}</th>
                <th>{t("labelEmail")}</th>
              </tr>
            </thead>
            <tbody>
              {!!contactListItems.contacts &&
                !!contactListItems.contacts.results &&
                contactListItems.contacts.results.map(
                  (contact: any, i: any) =>
                    !!contact && (
                      <tr key={contact.id} className={`${className}__item`}>
                        <td>
                          <button
                            className="link-button"
                            onClick={() => handleOpenModal(contact.id)}
                          >
                            {contact.name.fi}{" "}
                          </button>
                        </td>
                        <td>
                          <span>{contact.phone}</span>{" "}
                        </td>
                        <td>
                          <span>{contact.email}</span>
                        </td>
                      </tr>
                    )
                )}
            </tbody>
          </table>
        </div>
        {showModal ? (
          <ContactModal
            allOperatorNames={allOperatorNames}
            isOpen={showModal}
            contactId={contactId}
            onClose={handleCloseModal}
          />
        ) : null}
      </div>
    </React.Fragment>
  );
};
export default ContactList;
