import React, { useState } from "react";
import { useTranslation } from "react-i18next";
import Select from "react-select";
import { Contact, useContactDetailsQuery } from "../../generated/graphql";
import { getLocalizedName } from "../../util/helpers";
import Modal from "../Commons/Modal";
import ModalBody from "../Commons/ModalBody";
import ModalContent from "../Commons/ModalContent";
import ModalFooter from "../Commons/ModalFooter";
import ModalHeader from "../Commons/ModalHeader";

import "./styles.css";
interface OperatorOption {
  label: string;
  value: string;
}
interface Props {
  contactId: number;
  isOpen: boolean;
  onClose: any;
  allOperatorNames: OperatorOption[];
}

const ContactModal: React.FC<Props> = (props: Props) => {
  const initialState = {
    error: false
  };
  const [state] = useState(initialState);
  const { t } = useTranslation(["contactList", "commons"]);
  const { data, error, loading } = useContactDetailsQuery({
    variables: { id: props.contactId.toString() }
  });

  if (loading) {
    return <div>{t("commons:loading")}</div>;
  }

  if (error || !data || !data.contactDetails) {
    return <div />;
  }
  const contactDetails: Contact = data.contactDetails;

  const handleCloseModal = () => {
    props.onClose();
  };

  return (
    <React.Fragment>
      <Modal isOpen={props.isOpen} bottom="20px">
        <ModalContent>
          <div id="contactEditModal">
            <form
              name="editContactForm"
              className="form-horizontal"
              role="form"
            >
              <ModalHeader title={t("editContact")} />
              <ModalBody>
                {state.error ? <div>{t("error")}</div> : <div />}

                <div className="row">
                  <label>{t("visibilityToOperators")}</label>
                </div>
                <div className="row">
                  <div className="column3 first-column">
                    <div className="form-group">
                      <div
                        className="input-group operator"
                        style={{ width: "100%", zIndex: 20 }}
                      >
                        <Select
                          options={props.allOperatorNames}
                          isClearable={true}
                        />
                        <span className="input-group-btn">
                          <button
                            type="button"
                            className="btn btn-default createOperator"
                          >
                            +
                          </button>
                        </span>
                      </div>
                    </div>
                  </div>
                </div>

                <div className="row">
                  <label>{t("labelName")} *</label>
                </div>
                <div className="row">
                  <div className="column3 first-column">
                    <div className="form-group">
                      <div className="input-group">
                        <input
                          name="nameFi"
                          type="text"
                          className="form-control"
                          defaultValue={getLocalizedName(
                            contactDetails.name,
                            "fi"
                          )}
                        />
                        <span className="input-group-addon">fi&nbsp;</span>
                      </div>
                    </div>
                  </div>
                  <div className="column3">
                    <div className="form-group">
                      <div className="input-group">
                        <input
                          name="nameSv"
                          type="text"
                          className="form-control"
                          defaultValue={getLocalizedName(
                            contactDetails.name,
                            "sv"
                          )}
                        />
                        <span className="input-group-addon">sv</span>
                      </div>
                    </div>
                  </div>
                  <div className="column3 last-column">
                    <div className="form-group">
                      <div className="input-group">
                        <input
                          name="nameEn"
                          type="text"
                          className="form-control"
                          defaultValue={getLocalizedName(
                            contactDetails.name,
                            "en"
                          )}
                        />
                        <span className="input-group-addon">en</span>
                      </div>
                    </div>
                  </div>
                </div>

                <div className="row">
                  <div className="column3 first-column">
                    <div className="form-group">
                      <label>{t("labelPhone")} (*)</label>
                      <div>
                        <input
                          name="phone"
                          type="input"
                          className="form-control"
                          defaultValue={contactDetails.phone || ""}
                        />
                      </div>
                      <div className="row">
                        <p> {t("fillPhoneOrEmail")}</p>
                      </div>
                    </div>
                  </div>
                </div>

                <div className="row">
                  <div className="column3 first-column">
                    <div className="form-group">
                      <label> {t("email")} (*)</label>
                      <div>
                        <input
                          name="email"
                          type="input"
                          className="form-control"
                          defaultValue={contactDetails.email || ""}
                        />
                      </div>
                    </div>
                  </div>
                </div>

                <h4>{t("address")} </h4>
                <div className="row">
                  <label>{t("streetAddress")} </label>
                </div>
                <div className="row">
                  <div className="column3 first-column">
                    <div className="form-group">
                      <div className="input-group">
                        <input
                          name="streetAddressFi"
                          type="text"
                          className="form-control "
                          defaultValue={getLocalizedName(
                            contactDetails.address
                              ? contactDetails.address.streetAddress
                              : null,
                            "fi"
                          )}
                        />
                        <span className="input-group-addon">fi&nbsp;</span>
                      </div>
                    </div>
                  </div>
                  <div className="column3">
                    <div className="form-group">
                      <div className="input-group">
                        <input
                          name="streetAddressSv"
                          type="text"
                          className="form-control "
                          defaultValue={getLocalizedName(
                            contactDetails.address
                              ? contactDetails.address.streetAddress
                              : undefined,
                            "sv"
                          )}
                        />
                        <span className="input-group-addon">sv</span>
                      </div>
                    </div>
                  </div>
                  <div className="column3 last-column">
                    <div className="form-group">
                      <div className="input-group">
                        <input
                          name="streetAddressEn"
                          type="text"
                          className="form-control "
                          defaultValue={getLocalizedName(
                            contactDetails.address
                              ? contactDetails.address.streetAddress
                              : null,
                            "en"
                          )}
                        />
                        <span className="input-group-addon">en</span>
                      </div>
                    </div>
                  </div>
                </div>

                <div className="row">
                  <div className="column3 first-column">
                    <div className="form-group">
                      <label>{t("postalCode")}</label>
                      <div>
                        <input
                          name="postalCode"
                          type="input"
                          defaultValue={
                            contactDetails.address &&
                            contactDetails.address.postalCode
                              ? contactDetails.address.postalCode
                              : ""
                          }
                          className="form-control "
                          empty-to-null=""
                        />
                      </div>
                    </div>
                  </div>
                </div>

                <div className="row">
                  <label>{t("city")}</label>
                </div>
                <div className="row">
                  <div className="column3 first-column">
                    <div className="form-group">
                      <div className="input-group">
                        <input
                          name="cityFi"
                          type="text"
                          className="form-control "
                          defaultValue={getLocalizedName(
                            contactDetails.address
                              ? contactDetails.address.city
                              : null,
                            "fi"
                          )}
                        />
                        <span className="input-group-addon">fi&nbsp;</span>
                      </div>
                    </div>
                  </div>
                  <div className="column3">
                    <div className="form-group">
                      <div className="input-group">
                        <input
                          name="citySv"
                          type="text"
                          className="form-control "
                          defaultValue={getLocalizedName(
                            contactDetails.address
                              ? contactDetails.address.city
                              : null,
                            "sv"
                          )}
                        />
                        <span className="input-group-addon">sv</span>
                      </div>
                    </div>
                  </div>
                  <div className="column3 last-column">
                    <div className="form-group">
                      <div className="input-group">
                        <input
                          type="text"
                          className="form-control "
                          defaultValue={getLocalizedName(
                            contactDetails.address
                              ? contactDetails.address.city
                              : null,
                            "en"
                          )}
                        />
                        <span className="input-group-addon">en</span>
                      </div>
                    </div>
                  </div>
                </div>

                <h4>{t("additionalInfo")}</h4>
                <div className="row">
                  <label>{t("Customer Service Opening Hours")}</label>
                </div>
                <div className="row">
                  <div className="column3 first-column">
                    <div className="form-group">
                      <div className="input-group">
                        <textarea
                          className="form-control"
                          defaultValue={getLocalizedName(
                            contactDetails.openingHours
                              ? contactDetails.openingHours
                              : null,
                            "fi"
                          )}
                        />
                        <span className="input-group-addon">fi&nbsp;</span>
                      </div>
                    </div>
                  </div>
                  <div className="column3">
                    <div className="form-group">
                      <div className="input-group">
                        <textarea
                          name="openingHoursSv"
                          className="form-control "
                          defaultValue={getLocalizedName(
                            contactDetails.openingHours
                              ? contactDetails.openingHours
                              : null,
                            "sv"
                          )}
                        />
                        <span className="input-group-addon">sv</span>
                      </div>
                    </div>
                  </div>
                  <div className="column3 last-column">
                    <div className="form-group">
                      <div className="input-group">
                        <textarea
                          name="openingHoursEn"
                          className="form-control"
                          defaultValue={getLocalizedName(
                            contactDetails.openingHours
                              ? contactDetails.openingHours
                              : null,
                            "en"
                          )}
                        />
                        <span className="input-group-addon">en</span>
                      </div>
                    </div>
                  </div>
                </div>

                <div className="row">
                  <label>{t("details")}</label>
                </div>
                <div className="row">
                  <div className="column3 first-column">
                    <div className="form-group">
                      <div className="input-group">
                        <textarea
                          className="form-control "
                          defaultValue={getLocalizedName(
                            contactDetails.info || null,
                            "fi"
                          )}
                        />
                        <span className="input-group-addon">fi&nbsp;</span>
                      </div>
                    </div>
                  </div>
                  <div className="column3">
                    <div className="form-group">
                      <div className="input-group">
                        <textarea
                          className="form-control "
                          defaultValue={getLocalizedName(
                            contactDetails.info || null,
                            "sv"
                          )}
                        />
                        <span className="input-group-addon">sv</span>
                      </div>
                    </div>
                  </div>
                  <div className="column3 last-column">
                    <div className="form-group">
                      <div className="input-group">
                        <textarea
                          className="form-control "
                          defaultValue={getLocalizedName(
                            contactDetails.info || null,
                            "en"
                          )}
                        />
                        <span className="input-group-addon">en</span>
                      </div>
                    </div>
                  </div>
                </div>
              </ModalBody>
              <ModalFooter>
                <div className="row">
                  <div className="last-column">
                    <button
                      type="button"
                      id="wdContactCancel"
                      className="btn btn-xs-secondary"
                      onClick={handleCloseModal}
                    >
                      {t("cancel")}
                    </button>
                  </div>
                </div>
              </ModalFooter>
            </form>
          </div>
        </ModalContent>
      </Modal>
    </React.Fragment>
  );
};
export default ContactModal;
