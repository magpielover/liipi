import gql from "graphql-tag";

export const QUERY_CONTACT_LIST = gql`
  query ContactList($ids: String) {
    contacts(ids: $ids) {
      results {
        id
        name {
          fi
          en
          sv
        }
        operatorId
        phone
        email
        address {
          streetAddress {
            fi
            en
            sv
          }
          postalCode
          city {
            fi
            en
            sv
          }
        }
        openingHours {
          fi
          en
          sv
        }
        info {
          fi
          en
          sv
        }
        hasMore
      }
    }
  }
`;
