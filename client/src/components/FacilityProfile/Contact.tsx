import * as React from "react";
import { useTranslation } from "react-i18next";
import { useContactDetailsQuery } from "../../generated/graphql";

interface Props {
  id: any;
  title: string;
}

const Contact: React.FC<Props> = (props: Props) => {
  const { t } = useTranslation("commons");
  const { data, error, loading } = useContactDetailsQuery({
    skip: !props.id,
    variables: { id: props.id.toString() }
  });

  if (loading) {
    return <div>{t("loading")}</div>;
  }

  if (error || !data) {
    return <div>{t("error")}</div>;
  }

  const contactDetails = data.contactDetails;
  return (
    <>
      <div>
        <h4>{props.title}</h4>
        <div className="row">
          <div className="column3 first-column">
            <p ng-show="value.fi">
              <span className="lang-fi ">
                {contactDetails.name ? contactDetails.name.fi : "-"}
              </span>{" "}
              (fi)
            </p>
          </div>
          <div className="column3">
            <p>
              <span className="lang-sv">
                {contactDetails.name ? contactDetails.name.sv : "-"}
              </span>{" "}
              (sv)
            </p>
          </div>
          <div className="column3 last-column">
            <p>
              <span className="lang-en ">
                {contactDetails.name ? contactDetails.name.en : "-"}
              </span>{" "}
              (en)
            </p>
          </div>
        </div>

        {contactDetails.phone && (
          <div className="row ng-scope">
            <div className="column3 first-column">
              <p>{contactDetails.phone}</p>
            </div>
          </div>
        )}

        {contactDetails.email && (
          <div className="row">
            <div className="column3 first-column">
              <p>{contactDetails.email}</p>
            </div>
          </div>
        )}
      </div>
    </>
  );
};

export default Contact;
