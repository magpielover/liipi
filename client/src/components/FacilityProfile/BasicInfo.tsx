import * as React from "react";
import { useTranslation } from "react-i18next";
import { FacilityDetails, FacilityProfileQuery } from "../../generated/graphql";
import { translateValue } from "../../util/translateUtils";
import Operator from "./Operator";
interface Props {
  facilityDetails: FacilityDetails;
}

const FacilityBasicInfo: React.FC<Props> = ({ facilityDetails }) => {
  const { t } = useTranslation("facilityProfile");

  const showAliases = () => {
    return !!facilityDetails.aliases.length;
  };

  const showStatusDescription = () => {
    return !!facilityDetails.statusDescription;
  };

  return (
    <div>
      <div className="row">
        <h3 className="h3-first ">{t("titleBasicInfo")}</h3>
        <h4>{t("labelName")}</h4>
      </div>

      <div className="row facilityName">
        <div>
          <div className="column3 first-column">
            <p>
              <span className="lang-fi ">{facilityDetails.name.fi || "-"}</span>{" "}
              (fi)
            </p>
          </div>
          <div className="column3">
            <p>
              <span>{facilityDetails.name.sv || "-"}</span> (sv)
            </p>
          </div>
          <div className="column3 last-column">
            <p>
              <span className="lang-en">{facilityDetails.name.en || "-"}</span>{" "}
              (en)
            </p>
          </div>
        </div>
      </div>

      {showAliases() && (
        <div className="row">
          <div className="column3 first-column wdAliasesBlock">
            <h4>{t("titleAliases")}</h4>
            <p className="wdAliases">{facilityDetails.aliases.join(", ")}</p>
          </div>
        </div>
      )}

      <Operator id={facilityDetails.operatorId} />
      <div className="row">
        <h4>{t("labelStatus")}</h4>
      </div>

      <div className="row">
        {facilityDetails.status
          ? translateValue("status", facilityDetails.status, t)
          : ""}
      </div>
      {showStatusDescription() && (
        <>
          <div className="row">
            <h4>{t("labelStatusAdditionalInfo")}</h4>
          </div>
          <div className="row">
            <div className="column3 first-column">
              <p>
                <span className="lang-fi ">
                  {facilityDetails.statusDescription
                    ? facilityDetails.statusDescription.fi
                    : ""}
                </span>{" "}
                (fi)
              </p>
            </div>
            <div className="column3">
              <p>
                <span className="lang-sv ">
                  {facilityDetails.statusDescription
                    ? facilityDetails.statusDescription.sv
                    : ""}
                </span>{" "}
                (sv)
              </p>
            </div>
            <div className="column3 last-column">
              <p>
                <span className="lang-en ">
                  {facilityDetails.statusDescription
                    ? facilityDetails.statusDescription.en
                    : ""}
                </span>{" "}
                (en)
              </p>
            </div>
          </div>
        </>
      )}
    </div>
  );
};

export default FacilityBasicInfo;
