import * as React from "react";
import { useTranslation } from "react-i18next";
import { FacilityDetails, Utilization } from "../../generated/graphql";
import BuiltCapacity from "../Commons/BuiltCapacity";
import PricingAndServices from "./PricingAndServices";
import UnavailableCapacityTable from "./UnavailableCapacityTable";
import UtilizationTable from "./UtilizationTable";

interface Props {
  facilityDetails: FacilityDetails;
  utilizationItems: Utilization[];
}

const Capacity: React.FC<Props> = ({ facilityDetails, utilizationItems }) => {
  const { t } = useTranslation(["facilityProfile", "commons"]);
  const facilities: any = [facilityDetails];

  return (
    <div id="capacities">
      <h3 className="h3-view">
        <span>{t("titleCapacity")}</span>
      </h3>
      {!!utilizationItems.length && (
        <UtilizationTable
          facilities={facilities}
          utilizationItems={utilizationItems}
        />
      )}

      <h4>{t("titleBuiltCapacity")}</h4>
      <div className="row">
        <div className="column2 first-column">
          <div className="panel panel-default">
            <BuiltCapacity facilities={facilities} />
          </div>
        </div>
      </div>

      <div>
        <div className="row smallMargin">
          <h4>
            <span>{t("titleUnavailable")}</span>
          </h4>
        </div>
        <div className="row">
          <div className="column2 first-column">
            <div className="panel panel-default">
              <UnavailableCapacityTable
                unavailableCapacities={facilityDetails.unavailableCapacities}
              />
            </div>
          </div>
        </div>
      </div>

      <PricingAndServices facilityDetails={facilityDetails} />
    </div>
  );
};

export default Capacity;
