import * as React from "react";
import { useTranslation } from "react-i18next";
import { UnavailableCapacity } from "../../generated/graphql";
import { translateValue } from "../../util/translateUtils";

interface Props {
  unavailableCapacities: UnavailableCapacity[];
}

const UnavailableCapacityTable: React.FC<Props> = ({
  unavailableCapacities
}) => {
  const { t } = useTranslation("commons");

  if (!unavailableCapacities || !unavailableCapacities.length) {
    return null;
  }

  return (
    <table
      id="unavailable-capacities"
      className="table table-bordered table-striped table-condensed"
    >
      <thead>
        <tr>
          <th>
            <span>{t("labelCapacityType")}</span>
          </th>
          <th>
            <span>{t("labelUsage")}</span>
          </th>
          <th>
            <span>{t("labelUnavailable")}</span>
          </th>
        </tr>
      </thead>
      <tbody>
        {unavailableCapacities.map((unavailableCapacity, i: any) => {
          const shouldShowCapacityType = () => {
            if (i === 0) {
              return true;
            } else if (
              unavailableCapacities[i - 1].capacityType ===
              unavailableCapacity.capacityType
            ) {
              return false;
            } else {
              return true;
            }
          };

          return (
            <tr key={i}>
              <td>
                {shouldShowCapacityType() &&
                  translateValue(
                    "capacityType",
                    unavailableCapacity.capacityType,
                    t
                  )}
              </td>
              <td>{translateValue("usage", unavailableCapacity.usage, t)}</td>
              <td>{unavailableCapacity.capacity}</td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
};

export default UnavailableCapacityTable;
