import * as React from "react";
import { useTranslation } from "react-i18next";
import {
  useFacilityProfileQuery,
  useFacilityUtilizationQuery
} from "../../generated/graphql";
import FacilityProfile from "./FacilityProfile";

interface Params {
  id: string;
  url: string;
  isExact: boolean;
}
interface Match {
  params: Params;
}

interface Props {
  match: Match;
}

const FacilityProfileContainer = ({ match }: Props) => {
  const { t } = useTranslation("commons");
  const { data, error, loading } = useFacilityProfileQuery({
    variables: { id: match.params.id }
  });
  const {
    data: utilizationData,
    loading: loadingUtilization
  } = useFacilityUtilizationQuery({
    variables: { id: match.params.id }
  });

  if (loading || loadingUtilization) {
    return <div>{t("loading")}</div>;
  }

  if (error || !data) {
    return <div />;
  }

  return <FacilityProfile data={data} utilizationData={utilizationData} />;
};

export default FacilityProfileContainer;
