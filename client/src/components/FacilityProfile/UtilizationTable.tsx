import * as React from "react";
import { useTranslation } from "react-i18next";
import { Capacity, Facility, Utilization } from "../../generated/graphql";
import { formatDate } from "../../util/dateUtils";
import { translateValue } from "../../util/translateUtils";

interface Props {
  facilities: Facility[];
  utilizationItems: Utilization[];
}

const UtilizationTable: React.FC<Props> = ({
  facilities,
  utilizationItems
}) => {
  const { t } = useTranslation(["facilityProfile", "commons"]);
  if (!utilizationItems.length) {
    return null;
  }

  const totalCapacities: any = facilities.reduce<Capacity>(
    (previousValue: any, currentValue: Facility) => {
      if (!currentValue || !currentValue.builtCapacity) {
        return previousValue;
      }
      const builtCapacity: any = currentValue.builtCapacity;
      Object.keys(builtCapacity).forEach(key => {
        if (builtCapacity[key]) {
          previousValue[key] = previousValue[key]
            ? previousValue[key] + builtCapacity[key]
            : builtCapacity[key];
        }
      });
      return previousValue;
    },
    {}
  );

  return (
    <div className="wdPredictions">
      <h4>
        <span>{t("titleAvailable")}</span>
      </h4>
      <div className="row">
        <p>
          <span>
            {t("messageAvailableAndForecast", {
              time: formatDate(new Date(), "HH.mm")
            })}
          </span>
        </p>
      </div>
      <div className="row">
        <div className="column first-column">
          <div className="panel panel-default row">
            <table
              id="utilization"
              className="table table-bordered table-striped table-condensed wdPredictionsTable"
            >
              <thead>
                <tr>
                  <th id="facilities-view-pricing-capacityType">
                    <span>{t("labelCapacityType")}</span>
                  </th>
                  <th id="facilities-view-pricing-usage">
                    <span>{t("labelUsage")}</span>
                  </th>
                  <th id="facilities-utilization-capacity-build">
                    <span>{t("labelBuiltCapacity")}</span>
                  </th>
                  <th>
                    <span>{t("labelCurrent")}</span>
                  </th>
                </tr>
              </thead>
              <tbody>
                {utilizationItems.map((item, index) => {
                  return (
                    <tr className="wdPredictionRow" key={index}>
                      <td className="wdCapacityType">
                        <span>
                          {translateValue(
                            "commons:capacityType",
                            item.capacityType,
                            t
                          )}
                        </span>
                      </td>
                      <td className="wdUsage">
                        <span>
                          {translateValue("commons:usage", item.usage, t)}
                        </span>
                      </td>
                      <td>{totalCapacities[item.capacityType || ""]}</td>
                      <td>
                        <span>{item.spacesAvailable}</span>{" "}
                        {item.timestamp && (
                          <small>
                            ({formatDate(item.timestamp, "dd.MM.yyyy HH:mm")})
                          </small>
                        )}
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  );
};

export default UtilizationTable;
