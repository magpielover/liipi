import React from "react";
import { useTranslation } from "react-i18next";
import Modal from "../Commons/Modal";
import ModalBody from "../Commons/ModalBody";
import ModalContent from "../Commons/ModalContent";
import ModalFooter from "../Commons/ModalFooter";
import ModalHeader from "../Commons/ModalHeader";
import "./styles.css";

interface Props {
  isOpen: boolean;
  onClose: any;
  port: any | null;
}

const PortModal: React.FC<Props> = ({ isOpen, onClose, port }) => {
  const { t } = useTranslation("facilityProfile");
  const streetAddress =
    port && port.address ? port.address.streetAddress : null;
  const info = port ? port.info : null;

  const handleClose = () => {
    onClose();
  };

  const hasAddress = () => {
    return (
      streetAddress &&
      (streetAddress.fi || streetAddress.sv || streetAddress.en)
    );
  };

  const hasInfo = () => {
    return info && (info.fi || info.sv || info.en);
  };

  return (
    <Modal isOpen={isOpen} contentLabel="Sisään-/uloskäynti">
      <ModalContent>
        <ModalHeader title="Sisään-/uloskäynti" />
        <ModalBody>
          <div className="row">
            <div className="column3 first-column">
              {port && !!port.entry && (
                <div className="portEntry">{t("portEntry")}</div>
              )}
              {port && !!port.exit && (
                <div className="portExit">{t("portExit")}</div>
              )}
              {port && !!port.pedestrian && (
                <div className="portPedestrian">{t("portPedestrian")}</div>
              )}
              {port && !!port.bicycle && (
                <div className="portBicycle">{t("portBicycle")}</div>
              )}
            </div>
          </div>
          {hasAddress() && (
            <>
              <div className="row">
                <h4>{t("titlePortAddress")}</h4>
              </div>
              <div className="row streetAddress">
                <div className="column3 first-column">
                  <p>
                    <span className="lang-fi">{streetAddress.fi}</span> (fi)
                  </p>
                </div>
                <div className="column3">
                  <p>
                    <span className="lang-sv">{streetAddress.sv}</span> (sv)
                  </p>
                </div>
                <div className="column3 last-column">
                  <p>
                    <span className="lang-en">{streetAddress.en}</span> (en)
                  </p>
                </div>
              </div>
            </>
          )}
          {hasInfo() && (
            <>
              <div className="row">
                <h4>Lisätietoa</h4>
              </div>
              <div className="row portInfo">
                <div className="column3 first-column">
                  <p>
                    <span className="lang-fi">{info.fi}</span> (fi)
                  </p>
                </div>
                <div className="column3">
                  <p>
                    <span className="lang-sv">{info.sv}</span> (sv)
                  </p>
                </div>
                <div className="column3 last-column">
                  <p>
                    <span className="lang-en">{info.en}</span> (en)
                  </p>
                </div>
              </div>
            </>
          )}
        </ModalBody>
        <ModalFooter>
          <button type="button" className="btn btn-xs" onClick={handleClose}>
            {t("buttonPortOk")}
          </button>
        </ModalFooter>
      </ModalContent>
    </Modal>
  );
};
export default PortModal;
