import * as React from "react";
import { useTranslation } from "react-i18next";
import { FacilityDetails, OpeningHour } from "../../generated/graphql";

type DayType = "BUSINESS_DAY" | "SATURDAY" | "SUNDAY";

interface Props {
  facilityDetails: FacilityDetails;
}

const OpeningHours: React.FC<Props> = ({ facilityDetails }) => {
  const { t } = useTranslation("facilityProfile");

  const translateOpenNow = (openingHours: OpeningHour, t: Function) =>
    openingHours.openNow ? t("labelOpen") : t("labelClosed");

  const translateOpenTimes = (
    openingHours: OpeningHour,
    dayType: DayType,
    t: Function
  ) => {
    const openTimes = openingHours.byDayType
      ? openingHours.byDayType[dayType]
      : null;
    return openTimes
      ? openTimes.from + "-" + openTimes.until
      : t("labelClosed");
  };

  const showOpeningHoursInfo = () => {
    return !!facilityDetails.openingHours.info;
  };

  const showOpeningHoursUrl = () => {
    return !!facilityDetails.openingHours.url;
  };

  const showOpeningHoursAdditionalInfo = () => {
    return showOpeningHoursInfo() || showOpeningHoursUrl();
  };

  return (
    <div>
      <div>
        <div className="row">
          <h3>{t("titleOpeningHours")}</h3>
        </div>
        <div>
          <div className="row">
            <div className="column3 first-column">
              <div className="panel panel-default row">
                <table
                  id="opening-hours"
                  className="table table-bordered table-striped table-condensed"
                >
                  <tbody>
                    <tr>
                      <td className="day-type">{t("labelBusinessDay")}</td>
                      <td className="from-until">
                        <span>
                          {facilityDetails.openingHours
                            ? translateOpenTimes(
                                facilityDetails.openingHours,
                                "BUSINESS_DAY",
                                t
                              )
                            : ""}
                        </span>
                      </td>
                    </tr>
                    <tr>
                      <td className="day-type">{t("labelSaturday")}</td>
                      <td className="from-until">
                        <span>
                          {facilityDetails.openingHours
                            ? translateOpenTimes(
                                facilityDetails.openingHours,
                                "SATURDAY",
                                t
                              )
                            : ""}
                        </span>
                      </td>
                    </tr>
                    <tr>
                      <td className="day-type">{t("labelSunday")}</td>
                      <td className="from-until">
                        <span>
                          {facilityDetails.openingHours
                            ? translateOpenTimes(
                                facilityDetails.openingHours,
                                "SUNDAY",
                                t
                              )
                            : ""}
                        </span>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <div className="row">
            <h4>{t("titleCurrentSituation")}</h4>
          </div>
          <div className="row">
            {facilityDetails.openingHours
              ? translateOpenNow(facilityDetails.openingHours, t)
              : ""}
          </div>
        </div>
        {showOpeningHoursAdditionalInfo() && (
          <>
            <div className="row">
              <h4>{t("titleAdditionalInfo")}</h4>
            </div>
            {showOpeningHoursInfo() && (
              <div className="row">
                <div className="column3 first-column">
                  <p>
                    <span className="lang-fi">
                      {facilityDetails.openingHours.info
                        ? facilityDetails.openingHours.info.fi
                        : "-"}
                    </span>{" "}
                    (fi)
                  </p>
                </div>
                <div className="column3">
                  <p>
                    <span className="lang-sv">
                      {facilityDetails.openingHours.info
                        ? facilityDetails.openingHours.info.sv
                        : "-"}
                    </span>{" "}
                    (sv)
                  </p>
                </div>
                <div className="column3 last-column">
                  <p>
                    <span className="lang-en">
                      {facilityDetails.openingHours.info
                        ? facilityDetails.openingHours.info.en
                        : "-"}
                    </span>{" "}
                    (en)
                  </p>
                </div>
              </div>
            )}

            {showOpeningHoursUrl() && (
              <div className="row">
                <div className="column3 first-column">
                  <p>
                    <span className="lang-fi">
                      {!!facilityDetails.openingHours.url &&
                        !!facilityDetails.openingHours.url.fi && (
                          <a
                            target="_blank"
                            href={facilityDetails.openingHours.url.fi}
                          >
                            {facilityDetails.openingHours.url.fi}
                          </a>
                        )}
                    </span>{" "}
                    (fi)
                  </p>
                </div>
                <div className="column3">
                  <p>
                    <span className="lang-sv">
                      {!!facilityDetails.openingHours.url &&
                        !!facilityDetails.openingHours.url.sv && (
                          <a
                            target="_blank"
                            href={facilityDetails.openingHours.url.sv}
                          >
                            {facilityDetails.openingHours.url.sv}
                          </a>
                        )}
                    </span>{" "}
                    (sv)
                  </p>
                </div>
                <div className="column3 last-column">
                  <p>
                    <span className="lang-en">
                      {!!facilityDetails.openingHours.url &&
                        !!facilityDetails.openingHours.url.en && (
                          <a
                            target="_blank"
                            href={facilityDetails.openingHours.url.en}
                          >
                            {facilityDetails.openingHours.url.en}
                          </a>
                        )}
                    </span>{" "}
                    (en)
                  </p>
                </div>
              </div>
            )}
          </>
        )}
      </div>
    </div>
  );
};

export default OpeningHours;
