import * as React from "react";
import { useTranslation } from "react-i18next";
import { RouteComponentProps, withRouter } from "react-router";
import {
  FacilityDetails,
  FacilityProfileQuery,
  FacilityUtilizationQuery,
  useHubsByFacilityQuery
} from "../../generated/graphql";
import { getCenter } from "../../util/mapUtils";
import { translateValue } from "../../util/translateUtils";
import Breadcrumb from "../Commons/Breadcrumb";
import Location from "../Commons/Location";
import BasicInfo from "./BasicInfo";
import Capacity from "./Capacity";
import Contact from "./Contact";
import OpeningHours from "./OpeningHours";
import "./styles.css";

interface Params {
  id: string;
}
interface Props extends RouteComponentProps<Params> {
  data: FacilityProfileQuery;
  utilizationData: FacilityUtilizationQuery | undefined;
}

const CONTACT_TYPE = {
  EMERGENCY: "emergency",
  OPERATOR: "operator",
  SERVICE: "service"
};

const getContactId = (
  facilityDetails: FacilityDetails,
  field: string
): number | null | undefined => {
  const contacts = facilityDetails.contacts;

  switch (field) {
    case CONTACT_TYPE.EMERGENCY:
      return contacts.emergency;
    case CONTACT_TYPE.OPERATOR:
      return contacts.operator;
    case CONTACT_TYPE.SERVICE:
      return contacts.service;
  }
};

const getFeatures: any = (facility: any) => {
  return facility
    ? [
        {
          type: "Feature",
          geometry: {
            coordinates: facility.location.coordinates,
            type: facility.location.type
          }
        }
      ]
    : [];
};

const getPortFeatures: any = (ports: any) => {
  return Array.isArray(ports)
    ? ports.map(port => ({
        type: "Feature",
        geometry: {
          coordinates: port.location.coordinates,
          type: port.location.type
        },
        properties: {
          entry: port.entry,
          exit: port.exit,
          pedestrian: port.pedestrian,
          bicycle: port.bicycle,
          address: port.address,
          info: port.info
        }
      }))
    : [];
};

const FacilityProfile: React.FC<Props> = ({ data, match, utilizationData }) => {
  const { t } = useTranslation("facilityProfile");
  const facilityDetails = data.facilityDetails;
  const utilizationItems = utilizationData
    ? utilizationData.facilityUtilization
    : [];

  const facilityName = facilityDetails.name ? facilityDetails.name.fi : "-";
  const {
    params: { id: facilityId }
  } = match;
  const { data: hubsData } = useHubsByFacilityQuery({
    variables: { facilityIds: [Number(facilityId)] }
  });
  const hubs: any = !!hubsData && !!hubsData.hubs ? hubsData.hubs.hubs : [];

  const bbox: any =
    data.facilityDetails && data.facilityDetails.location
      ? data.facilityDetails.location.bbox
      : undefined;
  const center = getCenter(bbox);

  const features = getFeatures(facilityDetails);
  const ports: any =
    data.facilityDetails && data.facilityDetails.ports
      ? data.facilityDetails.ports
      : undefined;
  features.push(...getPortFeatures(ports));

  const geojson: any = {
    type: "FeatureCollection",
    features
  };

  const zoom = 17;

  if (!data) {
    return <div>No facility available</div>;
  }
  const className = "container content";
  return (
    <React.Fragment>
      <Breadcrumb name={facilityName} canGoback={true} />
      <div className={className}>
        <div className="row wdFacilityView">
          <BasicInfo facilityDetails={facilityDetails} />
          <Location
            bounds={undefined}
            center={center}
            geojson={geojson}
            hubs={hubs}
            zoom={zoom}
          />
          <OpeningHours facilityDetails={facilityDetails} />

          <Capacity facilityDetails={facilityDetails} utilizationItems={utilizationItems} />

          <div className="row contacts">
            <h3 className="h3-view">{t("titleContacts")}</h3>
            {[
              CONTACT_TYPE.EMERGENCY,
              CONTACT_TYPE.OPERATOR,
              CONTACT_TYPE.SERVICE
            ].map((contactType, index) => {
              const id = getContactId(facilityDetails, contactType);
              return id ? (
                <Contact
                  key={index}
                  title={translateValue("commons:contactType", contactType, t)}
                  id={id}
                />
              ) : null;
            })}
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};

export default withRouter(FacilityProfile);
