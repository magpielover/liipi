import * as React from "react";
import { useTranslation } from "react-i18next";
import { FacilityDetails } from "../../generated/graphql";
import { translateList, translateValue } from "../../util/translateUtils";

interface Props {
  facilityDetails: FacilityDetails;
}

const PricingAndServices: React.FC<Props> = ({ facilityDetails }) => {
  const { t } = useTranslation(["facilityProfile", "commons"]);

  if (!facilityDetails.pricing || !facilityDetails.pricing.length) {
    return null;
  }

  const showPaymentMethods = () => {
    return !!facilityDetails.paymentInfo.paymentMethods.length;
  };

  const showPaymentInfoDetails = () => {
    return !!facilityDetails.paymentInfo.detail;
  };

  const showPaymentInfoUrls = () => {
    return !!facilityDetails.paymentInfo.url;
  };

  const showPaymentInfoAdditionalInfo = () => {
    return showPaymentInfoDetails() || showPaymentInfoUrls();
  };

  return (
    <>
      <div>
        <div className="row contacts">
          <h3 className="h3-view ">
            <span>{t("titlePricingAndServices")}</span>
          </h3>
        </div>
        <div className="row">
          <p>
            {translateValue(
              "commons:pricingMethod",
              facilityDetails.pricingMethod,
              t
            )}
          </p>
        </div>
        <div className="row">
          <div className="column first-column">
            <div className="panel panel-default row">
              <table
                id="pricing"
                className="table table-bordered table-striped table-condensed"
              >
                <thead>
                  <tr>
                    <th>
                      <span>{t("labelCapacity")}</span>
                    </th>
                    <th>
                      <span>{t("labelOpeningHours")}</span>
                    </th>
                    <th>
                      <span>{t("labelPrice")}</span>
                    </th>
                  </tr>
                  <tr>
                    <th>
                      <span>{t("labelCapacityType")}</span>
                    </th>
                    <th>
                      <span>{t("labelUsage")}</span>
                    </th>
                    <th>
                      <span>{t("labelMaxCapacity")}</span>
                    </th>
                    <th>
                      <span>{t("labelDayType")}</span>
                    </th>
                    <th>
                      <span>{t("label24H")}</span>
                    </th>
                    <th>
                      <span>{t("labelFrom")}</span>
                    </th>
                    <th>
                      <span>{t("labelUntil")}</span>
                    </th>
                    <th>
                      <span>{t("labelFree")}</span>
                    </th>
                    <th>
                      <span>{t("labelPriceFi")}</span>
                    </th>
                    <th>
                      <span>{t("labelPriceSv")}</span>
                    </th>
                    <th>
                      <span>{t("labelPriceEn")}</span>
                    </th>
                  </tr>
                </thead>
                <tbody>
                  {!!facilityDetails.pricing &&
                    facilityDetails.pricing.map((price, i: any) => {
                      const shouldShowCapacityType = () => {
                        if (!i) {
                          return true;
                        } else if (
                          price.capacityType ===
                            facilityDetails.pricing[i - 1].capacityType &&
                          price.usage === facilityDetails.pricing[i - 1].usage
                        ) {
                          return false;
                        } else {
                          return true;
                        }
                      };

                      const is24h = () => {
                        return (
                          Number(price.time.from) === 0 &&
                          Number(price.time.until) === 24
                        );
                      };
                      return (
                        <tr key={i}>
                          <td>
                            <div>
                              {shouldShowCapacityType() &&
                                translateValue(
                                  "commons:capacityType",
                                  price.capacityType,
                                  t
                                )}
                            </div>
                          </td>
                          <td>
                            <div>
                              {shouldShowCapacityType() &&
                                translateValue("commons:usage", price.usage, t)}
                            </div>
                          </td>
                          <td className="text-right ">{price.maxCapacity}</td>

                          <td>
                            {translateValue(
                              "commons:dayType",
                              price.dayType,
                              t
                            )}
                          </td>
                          <td>
                            <div className="centeredCell">
                              {is24h() ? "✓" : ""}
                            </div>
                          </td>
                          <td>
                            <span>{is24h() ? "" : price.time.from}</span>
                          </td>
                          <td>
                            <span>{is24h() ? "" : price.time.until}</span>
                          </td>

                          <td>
                            <div className="centeredCell">✓</div>
                          </td>
                          <td className="tableText ">
                            <span>{price.price && price.price.fi}</span>
                          </td>
                          <td className="tableText ">
                            <span>{price.price && price.price.sv}</span>
                          </td>
                          <td className="tableText ">
                            <span>{price.price && price.price.en}</span>
                          </td>
                        </tr>
                      );
                    })}
                </tbody>
              </table>
            </div>
          </div>
        </div>

        {showPaymentMethods() && (
          <div className="wdPaymentInfo">
            <h4>Maksutavat</h4>
            <div className="row wdPaymentMethodNames">
              {translateList(
                "commons:paymentMethod",
                facilityDetails.paymentInfo.paymentMethods,
                t
              )}
            </div>
          </div>
        )}

        {showPaymentInfoAdditionalInfo() && (
          <div className="wdPaymentInfoDetails">
            <h4>Lisätiedot</h4>
            {showPaymentInfoDetails() && (
              <div className="row">
                <div className="column3 first-column">
                  <p>
                    <span className="lang-fi">
                      {facilityDetails.paymentInfo.detail &&
                        facilityDetails.paymentInfo.detail.fi}
                    </span>{" "}
                    (fi)
                  </p>
                </div>
                <div className="column3">
                  <p>
                    <span className="lang-sv">
                      {facilityDetails.paymentInfo.detail &&
                        facilityDetails.paymentInfo.detail.sv}
                    </span>{" "}
                    (sv)
                  </p>
                </div>
                <div className="column3 last-column">
                  <p>
                    <span className="lang-en">
                      {facilityDetails.paymentInfo.detail &&
                        facilityDetails.paymentInfo.detail.en}
                    </span>{" "}
                    (en)
                  </p>
                </div>
              </div>
            )}
            {showPaymentInfoUrls() && (
              <div className="row">
                <div className="column3 first-column">
                  <p>
                    <span className="lang-fi">
                      {facilityDetails.paymentInfo.url &&
                        facilityDetails.paymentInfo.url.fi && (
                          <a
                            target="_blank"
                            href={facilityDetails.paymentInfo.url.fi}
                          >
                            {facilityDetails.paymentInfo.url.fi}
                          </a>
                        )}
                    </span>{" "}
                    (fi)
                  </p>
                </div>
                <div className="column3">
                  <p>
                    <span className="lang-sv">
                      {facilityDetails.paymentInfo.url &&
                        facilityDetails.paymentInfo.url.sv && (
                          <a
                            target="_blank"
                            href={facilityDetails.paymentInfo.url.sv}
                          >
                            {facilityDetails.paymentInfo.url.sv}
                          </a>
                        )}
                    </span>{" "}
                    (sv)
                  </p>
                </div>
                <div className="column3 last-column">
                  <p>
                    <span className="lang-en">
                      {facilityDetails.paymentInfo.url &&
                        facilityDetails.paymentInfo.url.en && (
                          <a
                            target="_blank"
                            href={facilityDetails.paymentInfo.url.en}
                          >
                            {facilityDetails.paymentInfo.url.en}
                          </a>
                        )}
                    </span>{" "}
                    (en)
                  </p>
                </div>
              </div>
            )}
          </div>
        )}
      </div>
    </>
  );
};

export default PricingAndServices;
