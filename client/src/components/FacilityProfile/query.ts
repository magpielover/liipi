import gql from "graphql-tag";

export const QUERY_FACILITY_PROFILE = gql`
  query FacilityProfile($id: ID!) {
    facilityDetails(id: $id) {
      id
      name {
        fi
        sv
        en
      }
      location {
        crs {
          type
          properties {
            name
          }
        }
        bbox
        type
        coordinates
      }
      aliases
      services
      status
      statusDescription {
        fi
        sv
        en
      }
      operatorId
      builtCapacity {
        CAR
        BICYCLE
        ELECTRIC_CAR
        BICYCLE_SECURE_SPACE
        DISABLED
        MOTORCYCLE
        BICYCLE
      }
      unavailableCapacities {
        capacityType
        usage
        capacity
      }
      ports {
        location {
          crs {
            type
            properties {
              name
            }
          }
          type
          coordinates
        }
        entry
        exit
        pedestrian
        bicycle
        address {
          streetAddress {
            fi
            sv
            en
          }
          postalCode
          city {
            fi
            sv
            en
          }
        }
        info {
          fi
          sv
          en
        }
      }
      pricingMethod
      pricing {
        usage
        capacityType
        maxCapacity
        dayType
        time {
          from
          until
        }
        price {
          fi
          sv
          en
        }
      }
      paymentInfo {
        detail {
          fi
          sv
          en
        }
        paymentMethods
        url {
          fi
          sv
          en
        }
      }
      contacts {
        emergency
        operator
        service
      }
      openingHours {
        openNow
        byDayType {
          BUSINESS_DAY {
            from
            until
          }
          SUNDAY {
            from
            until
          }
          SATURDAY {
            from
            until
          }
        }
        info {
          fi
          sv
          en
        }
        url {
          fi
          sv
          en
        }
      }
    }
  }
  query OperatorDetails($id: ID!) {
    operatorDetails(id: $id) {
      id
      name {
        fi
        sv
        en
      }
    }
  }
  query Contacts($ids: String) {
    contacts(ids: $ids) {
      results {
        id
        name {
          fi
          sv
          en
        }
        operatorId
        phone
        email
        address {
          streetAddress {
            fi
            en
            sv
          }
          postalCode
          city {
            fi
            en
            sv
          }
        }
        openingHours {
          fi
          en
          sv
        }
        info {
          fi
          sv
          en
        }
      }
    }
  }
  query ContactDetails($id: ID!) {
    contactDetails(id: $id) {
      id
      name {
        fi
        sv
        en
      }
      operatorId
      phone
      email
      address {
        streetAddress {
          fi
          en
          sv
        }
        postalCode
        city {
          fi
          en
          sv
        }
      }
      openingHours {
        fi
        en
        sv
      }
      info {
        fi
        sv
        en
      }
    }
  }
  query HubsByFacility($facilityIds: [Int]) {
    hubs(facilityIds: $facilityIds) {
      totalCount
      hubs {
        id
        name {
          fi
        }
      }
    }
  }

  query FacilityUtilization($id: ID!) {
    facilityUtilization(id: $id) {
      facilityId
      capacityType
      usage
      timestamp
      spacesAvailable
      capacity
      openNow
    }
  }
`;
