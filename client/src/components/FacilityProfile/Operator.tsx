import * as React from "react";
import { useTranslation } from "react-i18next";
import { useOperatorDetailsQuery } from "../../generated/graphql";

interface Props {
  id: number;
}

const Operator: React.FC<Props> = (props: Props) => {
  const { t } = useTranslation(["facilityProfile", "commons"]);

  const { data, error, loading } = useOperatorDetailsQuery({
    variables: { id: props.id.toString() }
  });

  if (loading) {
    return <div>{t("commons:loading")}</div>;
  }

  if (error) {
    return <div>{t("commons:error")}</div>;
  }

  if (!data) {
    return <div>No operator</div>;
  }
  const operatorDetails = data.operatorDetails;
  
  return (
    <div>
      <div className="row">
        <h4>{t("titleOperator")}</h4>
      </div>
      <div className="row operatorName">
        <div className="column3 first-column">
          <p>
            <span className="lang-fi">{operatorDetails.name.fi || "-"}</span>{" "}
            (fi)
          </p>
        </div>
        <div className="column3">
          <p>
            <span className="lang-sv">{operatorDetails.name.sv || "-"}</span>{" "}
            (sv)
          </p>
        </div>
        <div className="column3 last-column">
          <p ng-show="value.en">
            <span className="lang-en">{operatorDetails.name.en || "-"}</span>{" "}
            (en)
          </p>
        </div>
      </div>
    </div>
  );
};

export default Operator;
