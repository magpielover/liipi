import React from "react";

import { Redirect, Route, Switch } from "react-router-dom";
import "./App.css";
import About from "./components/About/About";
import AboutEn from "./components/About/AboutEn";
import LoginModal, {
  useLoginModalContext
} from "./components/Commons/LoginModal";
import Navbar from "./components/Commons/Navbar";
import ContactListContainer from "./components/ContacList";
import FacilityProfile from "./components/FacilityProfile";
import HubList from "./components/HubList";
import HubProfile from "./components/HubProfile";
import OperatorListContainer from "./components/OperatorList";
import ReportsContainer from "./components/Reports";
import UserListContainer from "./components/UserList";

const App = () => {
  const { closeLoginModal, isLoginModalOpen } = useLoginModalContext();

  return (
    <div id="wrap" className="App">
      <Navbar />
      {isLoginModalOpen ? (
        <LoginModal isOpen={isLoginModalOpen} onClose={closeLoginModal} />
      ) : null}
      <Switch>
        <Route path="/hubs" exact={true} component={HubList} />
        <Route path="/facility/:id" component={FacilityProfile} />
        <Route path="/hub/:id" component={HubProfile} />
        <Route path="/operators" component={OperatorListContainer} />
        <Route path="/contacts" component={ContactListContainer} />
        <Route path="/about" component={About} />
        <Route path="/about-en" component={AboutEn} />
        <Route path="/users" component={UserListContainer} />
        <Route path="/reports" component={ReportsContainer} />
        <Redirect path="/" to="/hubs" />
      </Switch>
    </div>
  );
};

export default App;
