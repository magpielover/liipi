import "jest-localstorage-mock";

import Enzyme from "enzyme";
import Adapter from "enzyme-adapter-react-16";

import "./i18n";

Enzyme.configure({ adapter: new Adapter() });
