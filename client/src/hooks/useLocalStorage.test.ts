import useLocalStorage from "./useLocalStorage";

describe("useLocalStorage hook", () => {
  it("sets value", () => {
    const { setLocalStorageItem } = useLocalStorage("somePrefix:");
    setLocalStorageItem("someKey", "someValue");
    expect(localStorage.getItem("somePrefix:someKey")).toBe("someValue");
  });

  it("gets value", () => {
    const { getLocalStorageItem } = useLocalStorage("somePrefix:");
    localStorage.setItem("somePrefix:someKey", "someValue");
    expect(getLocalStorageItem("someKey")).toBe("someValue");
  });

  it("removes value", () => {
    const { removeLocalStorageItem } = useLocalStorage("somePrefix:");
    localStorage.setItem("somePrefix:someKey", "someValue");
    removeLocalStorageItem("someKey");
    expect(localStorage.getItem("somePrefix:someKey")).toBeNull;
  });

  it("clears values", () => {
    const { clearLocalStorage } = useLocalStorage("somePrefix:");
    localStorage.setItem("somePrefix:someKey", "someValue");
    localStorage.setItem("somePrefix:someOtherKey", "someOtherValue");
    localStorage.setItem("someOtherPrefix:yetAnotherKey", "yetAnotherValue");
    clearLocalStorage();
    expect(localStorage.getItem("somePrefix:someKey")).toBeNull;
    expect(localStorage.getItem("somePrefix:someOtherKey")).toBeNull;
    expect(localStorage.getItem("someOtherPrefix:yetAnotherKey")).toBe(
      "yetAnotherValue"
    );
  });
});
