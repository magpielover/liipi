export default function useLocalStorage(prefix: string) {
  const storedKeys: string[] = [];

  const getLocalStorageItem = (key: string): string | null =>
    localStorage.getItem(prefix + key);

  const setLocalStorageItem = (key: string, value: string): void => {
    storedKeys.push(key);
    return localStorage.setItem(prefix + key, value);
  };

  const removeLocalStorageItem = (key: string): void => {
    localStorage.removeItem(prefix + key);
    storedKeys.filter(k => k !== key);
  };

  const clearLocalStorage = (): void => {
    storedKeys.forEach(removeLocalStorageItem);
  };

  return {
    getLocalStorageItem,
    setLocalStorageItem,
    removeLocalStorageItem,
    clearLocalStorage
  };
}
