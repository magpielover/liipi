import gql from 'graphql-tag';
import * as React from 'react';
import * as ReactApollo from 'react-apollo';
import * as ReactApolloHooks from 'react-apollo-hooks';
export type Maybe<T> = T | null;
export type Omit<T, K extends keyof T> = Pick<T, Exclude<keyof T, K>>;
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string,
  String: string,
  Boolean: boolean,
  Int: number,
  Float: number,
};

export type Address = {
  __typename?: 'Address',
  streetAddress?: Maybe<LocalizedObject>,
  postalCode?: Maybe<Scalars['String']>,
  city?: Maybe<LocalizedObject>,
};

export type Capacity = {
  __typename?: 'Capacity',
  CAR?: Maybe<Scalars['Int']>,
  ELECTRIC_CAR?: Maybe<Scalars['Int']>,
  BICYCLE_SECURE_SPACE?: Maybe<Scalars['Int']>,
  DISABLED?: Maybe<Scalars['Int']>,
  MOTORCYCLE?: Maybe<Scalars['Int']>,
  BICYCLE?: Maybe<Scalars['Int']>,
};

export type Contact = {
  __typename?: 'Contact',
  id: Scalars['Int'],
  name: LocalizedObject,
  operatorId?: Maybe<Scalars['Int']>,
  phone?: Maybe<Scalars['String']>,
  email?: Maybe<Scalars['String']>,
  address?: Maybe<Address>,
  openingHours?: Maybe<LocalizedObject>,
  info?: Maybe<LocalizedObject>,
  hasMore?: Maybe<Scalars['Boolean']>,
};

export type ContactOptions = {
  __typename?: 'ContactOptions',
  emergency?: Maybe<Scalars['Int']>,
  operator?: Maybe<Scalars['Int']>,
  service?: Maybe<Scalars['Int']>,
};

export type ContactResponse = {
  __typename?: 'ContactResponse',
  results: Array<Contact>,
};

export type Crs = {
  __typename?: 'Crs',
  type?: Maybe<Scalars['String']>,
  properties?: Maybe<Name>,
  bbox?: Maybe<Array<Maybe<Scalars['Float']>>>,
};

export type Day = {
  __typename?: 'Day',
  BUSINESS_DAY?: Maybe<FromUntil>,
  SATURDAY?: Maybe<FromUntil>,
  SUNDAY?: Maybe<FromUntil>,
};

export type FacilitiesGeoJsonResponse = {
  __typename?: 'FacilitiesGeoJSONResponse',
  hasMore?: Maybe<Scalars['Boolean']>,
  features?: Maybe<Array<Maybe<FacilityGeoJson>>>,
  type?: Maybe<Scalars['String']>,
};

export type FacilitiesSummary = {
  __typename?: 'FacilitiesSummary',
  facilityCount?: Maybe<Scalars['Int']>,
  capacities?: Maybe<Capacity>,
};

export type Facility = {
  __typename?: 'Facility',
  id: Scalars['Int'],
  name?: Maybe<LocalizedObject>,
  status?: Maybe<Scalars['String']>,
  landings?: Maybe<Scalars['Int']>,
  type?: Maybe<Scalars['String']>,
  reuse_count?: Maybe<Scalars['Int']>,
  operatorId?: Maybe<Scalars['Int']>,
  builtCapacity?: Maybe<Capacity>,
  usages?: Maybe<Array<Maybe<Scalars['String']>>>,
  location?: Maybe<Location>,
};

export type FacilityDetails = {
  __typename?: 'FacilityDetails',
  id: Scalars['Int'],
  name: LocalizedObject,
  location?: Maybe<Location>,
  operatorId: Scalars['Int'],
  status?: Maybe<Scalars['String']>,
  pricingMethod: Scalars['String'],
  statusDescription?: Maybe<LocalizedObject>,
  builtCapacity?: Maybe<Capacity>,
  usages?: Maybe<Array<Maybe<Scalars['String']>>>,
  pricing: Array<Pricing>,
  unavailableCapacities: Array<UnavailableCapacity>,
  aliases: Array<Scalars['String']>,
  ports: Array<Port>,
  services: Array<Scalars['String']>,
  contacts: ContactOptions,
  paymentInfo: PaymentInfo,
  openingHours: OpeningHour,
};

export type FacilityGeoJson = {
  __typename?: 'FacilityGeoJSON',
  id?: Maybe<Scalars['Int']>,
  geometry?: Maybe<Geometry>,
  properties?: Maybe<Properties>,
  type?: Maybe<Scalars['String']>,
};

export type FacilityResponse = {
  __typename?: 'FacilityResponse',
  cursor?: Maybe<Scalars['String']>,
  totalCount?: Maybe<Scalars['Int']>,
  hasMore: Scalars['Boolean'],
  facilities: Array<Facility>,
};

export type FromUntil = {
  __typename?: 'FromUntil',
  from?: Maybe<Scalars['String']>,
  until?: Maybe<Scalars['String']>,
};

export type Geometry = {
  __typename?: 'Geometry',
  crs?: Maybe<Crs>,
  bbox?: Maybe<Array<Maybe<Scalars['Float']>>>,
  type?: Maybe<Scalars['String']>,
};

export type Hub = {
  __typename?: 'Hub',
  id: Scalars['Int'],
  name?: Maybe<LocalizedObject>,
  location?: Maybe<Location>,
  facilityIds: Array<Scalars['Int']>,
  address?: Maybe<Address>,
  hasMore?: Maybe<Scalars['Boolean']>,
};

export type HubDetails = {
  __typename?: 'HubDetails',
  id?: Maybe<Scalars['Int']>,
  name?: Maybe<LocalizedObject>,
  location?: Maybe<HubLocation>,
  facilityIds: Array<Scalars['Int']>,
  address?: Maybe<Address>,
};

export type HubGeoJson = {
  __typename?: 'HubGeoJSON',
  id?: Maybe<Scalars['Int']>,
  geometry?: Maybe<Geometry>,
  properties?: Maybe<Properties>,
  type?: Maybe<Scalars['String']>,
};

export type HubLocation = {
  __typename?: 'HubLocation',
  crs?: Maybe<Crs>,
  bbox?: Maybe<Array<Maybe<Scalars['Float']>>>,
  type?: Maybe<Scalars['String']>,
  coordinates?: Maybe<Array<Maybe<Scalars['Float']>>>,
};

export type HubResponse = {
  __typename?: 'HubResponse',
  cursor?: Maybe<Scalars['String']>,
  totalCount?: Maybe<Scalars['Int']>,
  hasMore: Scalars['Boolean'],
  hubs: Array<Hub>,
};

export type HubsGeoJsonResponse = {
  __typename?: 'HubsGeoJSONResponse',
  hasMore?: Maybe<Scalars['Boolean']>,
  features?: Maybe<Array<Maybe<HubGeoJson>>>,
  type?: Maybe<Scalars['String']>,
};

export type HubsSummary = {
  __typename?: 'HubsSummary',
  hubCount?: Maybe<Scalars['Int']>,
  capacities?: Maybe<Capacity>,
};

export type LocalizedObject = {
  __typename?: 'LocalizedObject',
  fi?: Maybe<Scalars['String']>,
  sv?: Maybe<Scalars['String']>,
  en?: Maybe<Scalars['String']>,
};

export type Location = {
  __typename?: 'Location',
  crs?: Maybe<Crs>,
  bbox?: Maybe<Array<Maybe<Scalars['Float']>>>,
  type?: Maybe<Scalars['String']>,
  coordinates?: Maybe<Array<Maybe<Array<Maybe<Array<Maybe<Scalars['Float']>>>>>>>,
};

export type Login = {
  __typename?: 'Login',
  operatorId?: Maybe<Scalars['String']>,
  passwordExpireInDays: Scalars['Int'],
  permissions: Array<Maybe<Scalars['String']>>,
  role: Scalars['String'],
  token: Scalars['String'],
  userId: Scalars['Int'],
  username: Scalars['String'],
};

export type Mutation = {
  __typename?: 'Mutation',
  _empty?: Maybe<Scalars['String']>,
  updateUtilization?: Maybe<Array<Maybe<UtilizationMutationResponse>>>,
  login?: Maybe<Login>,
  facilityUsageReport?: Maybe<Scalars['String']>,
  maxUtilizationReport?: Maybe<Scalars['String']>,
  hubsAndFacilitiesReport?: Maybe<Scalars['String']>,
  requestLogReport?: Maybe<Scalars['String']>,
  createUser?: Maybe<User>,
  changePassword?: Maybe<Scalars['String']>,
  updateToken?: Maybe<Scalars['String']>,
  deleteUser?: Maybe<Scalars['String']>,
};


export type MutationUpdateUtilizationArgs = {
  facilityId: Scalars['ID'],
  newUtilizations?: Maybe<Array<Maybe<UtilizationInput>>>
};


export type MutationLoginArgs = {
  password?: Maybe<Scalars['String']>,
  username?: Maybe<Scalars['String']>
};


export type MutationFacilityUsageReportArgs = {
  capacityTypes?: Maybe<Array<Scalars['String']>>,
  endDate?: Maybe<Array<Scalars['Int']>>,
  facilities?: Maybe<Array<Scalars['Int']>>,
  hubs?: Maybe<Array<Scalars['Int']>>,
  interval?: Maybe<Scalars['Int']>,
  operators?: Maybe<Array<Scalars['Int']>>,
  regions?: Maybe<Array<Scalars['Int']>>,
  startDate?: Maybe<Array<Scalars['Int']>>,
  usages?: Maybe<Array<Scalars['Int']>>
};


export type MutationMaxUtilizationReportArgs = {
  capacityTypes?: Maybe<Array<Scalars['String']>>,
  endDate?: Maybe<Array<Scalars['Int']>>,
  facilities?: Maybe<Array<Scalars['Int']>>,
  hubs?: Maybe<Array<Scalars['Int']>>,
  operators?: Maybe<Array<Scalars['Int']>>,
  regions?: Maybe<Array<Scalars['Int']>>,
  startDate?: Maybe<Array<Scalars['Int']>>,
  usages?: Maybe<Array<Scalars['Int']>>
};


export type MutationRequestLogReportArgs = {
  endDate?: Maybe<Array<Scalars['Int']>>,
  requestLogInterval: Scalars['String'],
  startDate?: Maybe<Array<Scalars['Int']>>
};


export type MutationCreateUserArgs = {
  user?: Maybe<UserInput>
};


export type MutationChangePasswordArgs = {
  id?: Maybe<Scalars['Int']>,
  newPassword?: Maybe<Scalars['String']>
};


export type MutationUpdateTokenArgs = {
  id?: Maybe<Scalars['Int']>
};


export type MutationDeleteUserArgs = {
  id: Scalars['Int']
};

export type Name = {
  __typename?: 'Name',
  name?: Maybe<Scalars['String']>,
};

export type OpeningHour = {
  __typename?: 'OpeningHour',
  openNow?: Maybe<Scalars['Boolean']>,
  byDayType?: Maybe<Day>,
  info?: Maybe<LocalizedObject>,
  url?: Maybe<LocalizedObject>,
};

export type Operator = {
  __typename?: 'Operator',
  id: Scalars['ID'],
  name: LocalizedObject,
};

export type OperatorResponse = {
  __typename?: 'OperatorResponse',
  hasMore?: Maybe<Scalars['Boolean']>,
  results: Array<Operator>,
};

export type PaymentInfo = {
  __typename?: 'PaymentInfo',
  detail?: Maybe<LocalizedObject>,
  url?: Maybe<LocalizedObject>,
  paymentMethods: Array<Scalars['String']>,
};

export type Port = {
  __typename?: 'Port',
  location?: Maybe<PortLocation>,
  entry?: Maybe<Scalars['Boolean']>,
  exit?: Maybe<Scalars['Boolean']>,
  pedestrian?: Maybe<Scalars['Boolean']>,
  bicycle?: Maybe<Scalars['Boolean']>,
  address?: Maybe<Address>,
  info?: Maybe<LocalizedObject>,
};

export type PortLocation = {
  __typename?: 'PortLocation',
  crs?: Maybe<Crs>,
  bbox?: Maybe<Array<Maybe<Scalars['Float']>>>,
  type?: Maybe<Scalars['String']>,
  coordinates: Array<Maybe<Scalars['Float']>>,
};

export type Prediction = {
  __typename?: 'Prediction',
  capacityType?: Maybe<Scalars['String']>,
  usage?: Maybe<Scalars['String']>,
  timestamp?: Maybe<Scalars['String']>,
  spacesAvailable?: Maybe<Scalars['Int']>,
  facilityId?: Maybe<Scalars['Int']>,
  hubId?: Maybe<Scalars['Int']>,
};

export type Pricing = {
  __typename?: 'Pricing',
  usage: Scalars['String'],
  capacityType: Scalars['String'],
  maxCapacity: Scalars['Int'],
  dayType: Scalars['String'],
  time: PricingTimeRange,
  price?: Maybe<LocalizedObject>,
};

export type PricingTimeRange = {
  __typename?: 'PricingTimeRange',
  from: Scalars['String'],
  until: Scalars['String'],
};

export type Properties = {
  __typename?: 'Properties',
  name?: Maybe<LocalizedObject>,
  status?: Maybe<Scalars['String']>,
  operatorId?: Maybe<Scalars['Int']>,
  builtCapacity?: Maybe<Capacity>,
  usages?: Maybe<Array<Maybe<Scalars['String']>>>,
  facilityIds?: Maybe<Array<Maybe<Scalars['Int']>>>,
};

export type Query = {
  __typename?: 'Query',
  _empty?: Maybe<Scalars['String']>,
  facilities: FacilityResponse,
  facilityDetails: FacilityDetails,
  facilitiesSummary?: Maybe<FacilitiesSummary>,
  facilityUtilization: Array<Utilization>,
  facilityPrediction?: Maybe<Array<Maybe<Prediction>>>,
  facilitiesGeoJSON?: Maybe<FacilitiesGeoJsonResponse>,
  facilityGeoJSON?: Maybe<FacilityGeoJson>,
  utilizations?: Maybe<Array<Maybe<Utilization>>>,
  hubs: HubResponse,
  hubDetails: HubDetails,
  hubsSmmary?: Maybe<HubsSummary>,
  hubUtilization?: Maybe<Array<Maybe<Utilization>>>,
  hubPrediction?: Maybe<Array<Maybe<Prediction>>>,
  hubsGeoJSON?: Maybe<HubsGeoJsonResponse>,
  hubGeoJSON?: Maybe<HubGeoJson>,
  operators: OperatorResponse,
  operatorDetails: Operator,
  contacts: ContactResponse,
  contactDetails: Contact,
  capacityTypes?: Maybe<Array<Scalars['String']>>,
  dayTypes?: Maybe<Array<Maybe<Scalars['String']>>>,
  facilityStatuses?: Maybe<Array<Maybe<Scalars['String']>>>,
  pricingMethods?: Maybe<Array<Maybe<Scalars['String']>>>,
  regions: Array<Maybe<Region>>,
  regionsWithHubs: Array<RegionWithHubs>,
  services?: Maybe<Array<Maybe<Scalars['String']>>>,
  usages?: Maybe<Array<Scalars['String']>>,
  paymentMethods?: Maybe<Array<Maybe<Scalars['String']>>>,
  users?: Maybe<Array<Maybe<User>>>,
};


export type QueryFacilitiesArgs = {
  after?: Maybe<Scalars['String']>,
  pageSize?: Maybe<Scalars['Int']>,
  sortBy?: Maybe<Scalars['String']>,
  sortDir?: Maybe<Scalars['String']>,
  statuses?: Maybe<Array<Maybe<Scalars['String']>>>,
  ids?: Maybe<Array<Maybe<Scalars['Int']>>>,
  geometry?: Maybe<Scalars['String']>,
  maxDistance?: Maybe<Scalars['Float']>
};


export type QueryFacilityDetailsArgs = {
  id: Scalars['ID'],
  geojson?: Maybe<Scalars['Boolean']>
};


export type QueryFacilitiesSummaryArgs = {
  summary?: Maybe<Scalars['Boolean']>
};


export type QueryFacilityUtilizationArgs = {
  id: Scalars['ID']
};


export type QueryFacilityPredictionArgs = {
  id: Scalars['ID'],
  after?: Maybe<Scalars['Int']>,
  at?: Maybe<Scalars['String']>
};


export type QueryFacilityGeoJsonArgs = {
  id: Scalars['ID']
};


export type QueryHubsArgs = {
  after?: Maybe<Scalars['String']>,
  pageSize?: Maybe<Scalars['Int']>,
  sortBy?: Maybe<Scalars['String']>,
  sortDir?: Maybe<Scalars['String']>,
  statuses?: Maybe<Array<Maybe<Scalars['String']>>>,
  ids?: Maybe<Array<Maybe<Scalars['Int']>>>,
  facilityIds?: Maybe<Array<Maybe<Scalars['Int']>>>,
  geometry?: Maybe<Scalars['String']>,
  maxDistance?: Maybe<Scalars['Float']>
};


export type QueryHubDetailsArgs = {
  id: Scalars['ID'],
  geojson?: Maybe<Scalars['Boolean']>
};


export type QueryHubsSmmaryArgs = {
  summary?: Maybe<Scalars['Boolean']>
};


export type QueryHubUtilizationArgs = {
  id: Scalars['ID']
};


export type QueryHubPredictionArgs = {
  id: Scalars['ID'],
  after?: Maybe<Scalars['Int']>,
  at?: Maybe<Scalars['String']>
};


export type QueryHubGeoJsonArgs = {
  id: Scalars['ID']
};


export type QueryOperatorDetailsArgs = {
  id: Scalars['ID']
};


export type QueryContactsArgs = {
  ids?: Maybe<Scalars['String']>
};


export type QueryContactDetailsArgs = {
  id: Scalars['ID']
};

export type Region = {
  __typename?: 'Region',
  id: Scalars['ID'],
  name?: Maybe<LocalizedObject>,
};

export type RegionWithHubs = {
  __typename?: 'RegionWithHubs',
  id: Scalars['ID'],
  name?: Maybe<LocalizedObject>,
  hubIds: Array<Scalars['ID']>,
};

export type Subscription = {
  __typename?: 'Subscription',
  _empty?: Maybe<Scalars['String']>,
};

export type UnavailableCapacity = {
  __typename?: 'UnavailableCapacity',
  capacityType: Scalars['String'],
  usage: Scalars['String'],
  capacity: Scalars['Int'],
};

export type User = {
  __typename?: 'User',
  id?: Maybe<Scalars['Int']>,
  username?: Maybe<Scalars['String']>,
  role?: Maybe<Scalars['String']>,
  operatorId?: Maybe<Scalars['Int']>,
};

export type UserInput = {
  role?: Maybe<Scalars['String']>,
  username?: Maybe<Scalars['String']>,
  password?: Maybe<Scalars['String']>,
  operatorId?: Maybe<Scalars['Int']>,
};

export type Utilization = {
  __typename?: 'Utilization',
  facilityId: Scalars['Int'],
  capacityType: Scalars['String'],
  usage: Scalars['String'],
  timestamp?: Maybe<Scalars['String']>,
  spacesAvailable?: Maybe<Scalars['Int']>,
  capacity?: Maybe<Scalars['Int']>,
  openNow?: Maybe<Scalars['Boolean']>,
};

export type UtilizationInput = {
  capacityType?: Maybe<Scalars['String']>,
  usage?: Maybe<Scalars['String']>,
  timestamp?: Maybe<Scalars['String']>,
  spacesAvailable?: Maybe<Scalars['Int']>,
  capacity?: Maybe<Scalars['Int']>,
};

export type UtilizationMutationResponse = {
  __typename?: 'UtilizationMutationResponse',
  facilityId?: Maybe<Scalars['Int']>,
  capacityType?: Maybe<Scalars['String']>,
  usage?: Maybe<Scalars['String']>,
  timestamp?: Maybe<Scalars['String']>,
  spacesAvailable?: Maybe<Scalars['Int']>,
  capacity?: Maybe<Scalars['Int']>,
};
export type CapacityTypeListQueryVariables = {};


export type CapacityTypeListQuery = ({ __typename?: 'Query' } & Pick<Query, 'capacityTypes'>);

export type LoginMutationVariables = {
  username?: Maybe<Scalars['String']>,
  password?: Maybe<Scalars['String']>
};


export type LoginMutation = ({ __typename?: 'Mutation' } & { login: Maybe<({ __typename?: 'Login' } & Pick<Login, 'operatorId' | 'passwordExpireInDays' | 'permissions' | 'role' | 'token' | 'userId' | 'username'>)> });

export type ContactListQueryVariables = {
  ids?: Maybe<Scalars['String']>
};


export type ContactListQuery = ({ __typename?: 'Query' } & { contacts: ({ __typename?: 'ContactResponse' } & { results: Array<({ __typename?: 'Contact' } & Pick<Contact, 'id' | 'operatorId' | 'phone' | 'email' | 'hasMore'> & { name: ({ __typename?: 'LocalizedObject' } & Pick<LocalizedObject, 'fi' | 'en' | 'sv'>), address: Maybe<({ __typename?: 'Address' } & Pick<Address, 'postalCode'> & { streetAddress: Maybe<({ __typename?: 'LocalizedObject' } & Pick<LocalizedObject, 'fi' | 'en' | 'sv'>)>, city: Maybe<({ __typename?: 'LocalizedObject' } & Pick<LocalizedObject, 'fi' | 'en' | 'sv'>)> })>, openingHours: Maybe<({ __typename?: 'LocalizedObject' } & Pick<LocalizedObject, 'fi' | 'en' | 'sv'>)>, info: Maybe<({ __typename?: 'LocalizedObject' } & Pick<LocalizedObject, 'fi' | 'en' | 'sv'>)> })> }) });

export type FacilityListQueryVariables = {
  ids?: Maybe<Array<Maybe<Scalars['Int']>>>
};


export type FacilityListQuery = ({ __typename?: 'Query' } & { facilities: ({ __typename?: 'FacilityResponse' } & Pick<FacilityResponse, 'totalCount' | 'hasMore'> & { facilities: Array<({ __typename?: 'Facility' } & Pick<Facility, 'id' | 'status' | 'usages'> & { name: Maybe<({ __typename?: 'LocalizedObject' } & Pick<LocalizedObject, 'fi'>)>, location: Maybe<({ __typename?: 'Location' } & Pick<Location, 'bbox' | 'type' | 'coordinates'> & { crs: Maybe<({ __typename?: 'Crs' } & Pick<Crs, 'type'> & { properties: Maybe<({ __typename?: 'Name' } & Pick<Name, 'name'>)> })> })>, builtCapacity: Maybe<({ __typename?: 'Capacity' } & Pick<Capacity, 'CAR' | 'ELECTRIC_CAR' | 'BICYCLE_SECURE_SPACE' | 'DISABLED' | 'MOTORCYCLE' | 'BICYCLE'>)> })> }) });

export type FacilityProfileQueryVariables = {
  id: Scalars['ID']
};


export type FacilityProfileQuery = ({ __typename?: 'Query' } & { facilityDetails: ({ __typename?: 'FacilityDetails' } & Pick<FacilityDetails, 'id' | 'aliases' | 'services' | 'status' | 'operatorId' | 'pricingMethod'> & { name: ({ __typename?: 'LocalizedObject' } & Pick<LocalizedObject, 'fi' | 'sv' | 'en'>), location: Maybe<({ __typename?: 'Location' } & Pick<Location, 'bbox' | 'type' | 'coordinates'> & { crs: Maybe<({ __typename?: 'Crs' } & Pick<Crs, 'type'> & { properties: Maybe<({ __typename?: 'Name' } & Pick<Name, 'name'>)> })> })>, statusDescription: Maybe<({ __typename?: 'LocalizedObject' } & Pick<LocalizedObject, 'fi' | 'sv' | 'en'>)>, builtCapacity: Maybe<({ __typename?: 'Capacity' } & Pick<Capacity, 'CAR' | 'BICYCLE' | 'ELECTRIC_CAR' | 'BICYCLE_SECURE_SPACE' | 'DISABLED' | 'MOTORCYCLE' | 'BICYCLE'>)>, unavailableCapacities: Array<({ __typename?: 'UnavailableCapacity' } & Pick<UnavailableCapacity, 'capacityType' | 'usage' | 'capacity'>)>, ports: Array<({ __typename?: 'Port' } & Pick<Port, 'entry' | 'exit' | 'pedestrian' | 'bicycle'> & { location: Maybe<({ __typename?: 'PortLocation' } & Pick<PortLocation, 'type' | 'coordinates'> & { crs: Maybe<({ __typename?: 'Crs' } & Pick<Crs, 'type'> & { properties: Maybe<({ __typename?: 'Name' } & Pick<Name, 'name'>)> })> })>, address: Maybe<({ __typename?: 'Address' } & Pick<Address, 'postalCode'> & { streetAddress: Maybe<({ __typename?: 'LocalizedObject' } & Pick<LocalizedObject, 'fi' | 'sv' | 'en'>)>, city: Maybe<({ __typename?: 'LocalizedObject' } & Pick<LocalizedObject, 'fi' | 'sv' | 'en'>)> })>, info: Maybe<({ __typename?: 'LocalizedObject' } & Pick<LocalizedObject, 'fi' | 'sv' | 'en'>)> })>, pricing: Array<({ __typename?: 'Pricing' } & Pick<Pricing, 'usage' | 'capacityType' | 'maxCapacity' | 'dayType'> & { time: ({ __typename?: 'PricingTimeRange' } & Pick<PricingTimeRange, 'from' | 'until'>), price: Maybe<({ __typename?: 'LocalizedObject' } & Pick<LocalizedObject, 'fi' | 'sv' | 'en'>)> })>, paymentInfo: ({ __typename?: 'PaymentInfo' } & Pick<PaymentInfo, 'paymentMethods'> & { detail: Maybe<({ __typename?: 'LocalizedObject' } & Pick<LocalizedObject, 'fi' | 'sv' | 'en'>)>, url: Maybe<({ __typename?: 'LocalizedObject' } & Pick<LocalizedObject, 'fi' | 'sv' | 'en'>)> }), contacts: ({ __typename?: 'ContactOptions' } & Pick<ContactOptions, 'emergency' | 'operator' | 'service'>), openingHours: ({ __typename?: 'OpeningHour' } & Pick<OpeningHour, 'openNow'> & { byDayType: Maybe<({ __typename?: 'Day' } & { BUSINESS_DAY: Maybe<({ __typename?: 'FromUntil' } & Pick<FromUntil, 'from' | 'until'>)>, SUNDAY: Maybe<({ __typename?: 'FromUntil' } & Pick<FromUntil, 'from' | 'until'>)>, SATURDAY: Maybe<({ __typename?: 'FromUntil' } & Pick<FromUntil, 'from' | 'until'>)> })>, info: Maybe<({ __typename?: 'LocalizedObject' } & Pick<LocalizedObject, 'fi' | 'sv' | 'en'>)>, url: Maybe<({ __typename?: 'LocalizedObject' } & Pick<LocalizedObject, 'fi' | 'sv' | 'en'>)> }) }) });

export type OperatorDetailsQueryVariables = {
  id: Scalars['ID']
};


export type OperatorDetailsQuery = ({ __typename?: 'Query' } & { operatorDetails: ({ __typename?: 'Operator' } & Pick<Operator, 'id'> & { name: ({ __typename?: 'LocalizedObject' } & Pick<LocalizedObject, 'fi' | 'sv' | 'en'>) }) });

export type ContactsQueryVariables = {
  ids?: Maybe<Scalars['String']>
};


export type ContactsQuery = ({ __typename?: 'Query' } & { contacts: ({ __typename?: 'ContactResponse' } & { results: Array<({ __typename?: 'Contact' } & Pick<Contact, 'id' | 'operatorId' | 'phone' | 'email'> & { name: ({ __typename?: 'LocalizedObject' } & Pick<LocalizedObject, 'fi' | 'sv' | 'en'>), address: Maybe<({ __typename?: 'Address' } & Pick<Address, 'postalCode'> & { streetAddress: Maybe<({ __typename?: 'LocalizedObject' } & Pick<LocalizedObject, 'fi' | 'en' | 'sv'>)>, city: Maybe<({ __typename?: 'LocalizedObject' } & Pick<LocalizedObject, 'fi' | 'en' | 'sv'>)> })>, openingHours: Maybe<({ __typename?: 'LocalizedObject' } & Pick<LocalizedObject, 'fi' | 'en' | 'sv'>)>, info: Maybe<({ __typename?: 'LocalizedObject' } & Pick<LocalizedObject, 'fi' | 'sv' | 'en'>)> })> }) });

export type ContactDetailsQueryVariables = {
  id: Scalars['ID']
};


export type ContactDetailsQuery = ({ __typename?: 'Query' } & { contactDetails: ({ __typename?: 'Contact' } & Pick<Contact, 'id' | 'operatorId' | 'phone' | 'email'> & { name: ({ __typename?: 'LocalizedObject' } & Pick<LocalizedObject, 'fi' | 'sv' | 'en'>), address: Maybe<({ __typename?: 'Address' } & Pick<Address, 'postalCode'> & { streetAddress: Maybe<({ __typename?: 'LocalizedObject' } & Pick<LocalizedObject, 'fi' | 'en' | 'sv'>)>, city: Maybe<({ __typename?: 'LocalizedObject' } & Pick<LocalizedObject, 'fi' | 'en' | 'sv'>)> })>, openingHours: Maybe<({ __typename?: 'LocalizedObject' } & Pick<LocalizedObject, 'fi' | 'en' | 'sv'>)>, info: Maybe<({ __typename?: 'LocalizedObject' } & Pick<LocalizedObject, 'fi' | 'sv' | 'en'>)> }) });

export type HubsByFacilityQueryVariables = {
  facilityIds?: Maybe<Array<Maybe<Scalars['Int']>>>
};


export type HubsByFacilityQuery = ({ __typename?: 'Query' } & { hubs: ({ __typename?: 'HubResponse' } & Pick<HubResponse, 'totalCount'> & { hubs: Array<({ __typename?: 'Hub' } & Pick<Hub, 'id'> & { name: Maybe<({ __typename?: 'LocalizedObject' } & Pick<LocalizedObject, 'fi'>)> })> }) });

export type FacilityUtilizationQueryVariables = {
  id: Scalars['ID']
};


export type FacilityUtilizationQuery = ({ __typename?: 'Query' } & { facilityUtilization: Array<({ __typename?: 'Utilization' } & Pick<Utilization, 'facilityId' | 'capacityType' | 'usage' | 'timestamp' | 'spacesAvailable' | 'capacity' | 'openNow'>)> });

export type HubListQueryVariables = {};


export type HubListQuery = ({ __typename?: 'Query' } & { hubs: ({ __typename?: 'HubResponse' } & Pick<HubResponse, 'totalCount' | 'hasMore'> & { hubs: Array<({ __typename?: 'Hub' } & Pick<Hub, 'id' | 'facilityIds'> & { name: Maybe<({ __typename?: 'LocalizedObject' } & Pick<LocalizedObject, 'fi'>)> })> }) });

export type HubProfileQueryVariables = {
  id: Scalars['ID']
};


export type HubProfileQuery = ({ __typename?: 'Query' } & { hubDetails: ({ __typename?: 'HubDetails' } & Pick<HubDetails, 'id' | 'facilityIds'> & { name: Maybe<({ __typename?: 'LocalizedObject' } & Pick<LocalizedObject, 'fi' | 'sv' | 'en'>)>, location: Maybe<({ __typename?: 'HubLocation' } & Pick<HubLocation, 'type' | 'coordinates'> & { crs: Maybe<({ __typename?: 'Crs' } & Pick<Crs, 'type'> & { properties: Maybe<({ __typename?: 'Name' } & Pick<Name, 'name'>)> })> })>, address: Maybe<({ __typename?: 'Address' } & Pick<Address, 'postalCode'> & { streetAddress: Maybe<({ __typename?: 'LocalizedObject' } & Pick<LocalizedObject, 'fi' | 'en' | 'sv'>)>, city: Maybe<({ __typename?: 'LocalizedObject' } & Pick<LocalizedObject, 'fi' | 'en' | 'sv'>)> })> }) });

export type OperatorListQueryVariables = {};


export type OperatorListQuery = ({ __typename?: 'Query' } & { operators: ({ __typename?: 'OperatorResponse' } & Pick<OperatorResponse, 'hasMore'> & { results: Array<({ __typename?: 'Operator' } & Pick<Operator, 'id'> & { name: ({ __typename?: 'LocalizedObject' } & Pick<LocalizedObject, 'fi' | 'en' | 'sv'>) })> }) });

export type RegionListQueryVariables = {};


export type RegionListQuery = ({ __typename?: 'Query' } & { regions: Array<Maybe<({ __typename?: 'Region' } & Pick<Region, 'id'> & { name: Maybe<({ __typename?: 'LocalizedObject' } & Pick<LocalizedObject, 'fi' | 'sv' | 'en'>)> })>> });

export type RegionsWithHubsQueryVariables = {};


export type RegionsWithHubsQuery = ({ __typename?: 'Query' } & { regionsWithHubs: Array<({ __typename?: 'RegionWithHubs' } & Pick<RegionWithHubs, 'id' | 'hubIds'> & { name: Maybe<({ __typename?: 'LocalizedObject' } & Pick<LocalizedObject, 'fi' | 'sv' | 'en'>)> })> });

export type FacilityUsageReportMutationVariables = {
  capacityTypes?: Maybe<Array<Scalars['String']>>,
  endDate?: Maybe<Array<Scalars['Int']>>,
  facilities?: Maybe<Array<Scalars['Int']>>,
  hubs?: Maybe<Array<Scalars['Int']>>,
  interval?: Maybe<Scalars['Int']>,
  operators?: Maybe<Array<Scalars['Int']>>,
  regions?: Maybe<Array<Scalars['Int']>>,
  startDate?: Maybe<Array<Scalars['Int']>>,
  usages?: Maybe<Array<Scalars['Int']>>
};


export type FacilityUsageReportMutation = ({ __typename?: 'Mutation' } & Pick<Mutation, 'facilityUsageReport'>);

export type HubsAndFacilitiesReportMutationVariables = {};


export type HubsAndFacilitiesReportMutation = ({ __typename?: 'Mutation' } & Pick<Mutation, 'hubsAndFacilitiesReport'>);

export type MaxUtilizationReportMutationVariables = {
  capacityTypes?: Maybe<Array<Scalars['String']>>,
  endDate?: Maybe<Array<Scalars['Int']>>,
  facilities?: Maybe<Array<Scalars['Int']>>,
  hubs?: Maybe<Array<Scalars['Int']>>,
  operators?: Maybe<Array<Scalars['Int']>>,
  regions?: Maybe<Array<Scalars['Int']>>,
  startDate?: Maybe<Array<Scalars['Int']>>,
  usages?: Maybe<Array<Scalars['Int']>>
};


export type MaxUtilizationReportMutation = ({ __typename?: 'Mutation' } & Pick<Mutation, 'maxUtilizationReport'>);

export type RequestLogReportMutationVariables = {
  endDate?: Maybe<Array<Scalars['Int']>>,
  requestLogInterval: Scalars['String'],
  startDate?: Maybe<Array<Scalars['Int']>>
};


export type RequestLogReportMutation = ({ __typename?: 'Mutation' } & Pick<Mutation, 'requestLogReport'>);

export type UsageListQueryVariables = {};


export type UsageListQuery = ({ __typename?: 'Query' } & Pick<Query, 'usages'>);

export type UserListQueryVariables = {};


export type UserListQuery = ({ __typename?: 'Query' } & { users: Maybe<Array<Maybe<({ __typename?: 'User' } & Pick<User, 'id' | 'username' | 'operatorId' | 'role'>)>>> });

export type OperatorNamesQueryVariables = {};


export type OperatorNamesQuery = ({ __typename?: 'Query' } & { operators: ({ __typename?: 'OperatorResponse' } & { results: Array<({ __typename?: 'Operator' } & Pick<Operator, 'id'> & { name: ({ __typename?: 'LocalizedObject' } & Pick<LocalizedObject, 'fi'>) })> }) });

export type ChangePasswordMutationVariables = {
  id: Scalars['Int'],
  newPassword: Scalars['String']
};


export type ChangePasswordMutation = ({ __typename?: 'Mutation' } & Pick<Mutation, 'changePassword'>);

export type DeleteUserMutationVariables = {
  id: Scalars['Int']
};


export type DeleteUserMutation = ({ __typename?: 'Mutation' } & Pick<Mutation, 'deleteUser'>);

export type CreateUserMutationVariables = {
  user: UserInput
};


export type CreateUserMutation = ({ __typename?: 'Mutation' } & { createUser: Maybe<({ __typename?: 'User' } & Pick<User, 'id' | 'username' | 'role' | 'operatorId'>)> });

export type UpdateTokenMutationVariables = {
  id: Scalars['Int']
};


export type UpdateTokenMutation = ({ __typename?: 'Mutation' } & Pick<Mutation, 'updateToken'>);

export const CapacityTypeListDocument = gql`
    query CapacityTypeList {
  capacityTypes
}
    `;
export type CapacityTypeListComponentProps = Omit<ReactApollo.QueryProps<CapacityTypeListQuery, CapacityTypeListQueryVariables>, 'query'>;

    export const CapacityTypeListComponent = (props: CapacityTypeListComponentProps) => (
      <ReactApollo.Query<CapacityTypeListQuery, CapacityTypeListQueryVariables> query={CapacityTypeListDocument} {...props} />
    );
    
export type CapacityTypeListProps<TChildProps = {}> = Partial<ReactApollo.DataProps<CapacityTypeListQuery, CapacityTypeListQueryVariables>> & TChildProps;
export function withCapacityTypeList<TProps, TChildProps = {}>(operationOptions?: ReactApollo.OperationOption<
  TProps,
  CapacityTypeListQuery,
  CapacityTypeListQueryVariables,
  CapacityTypeListProps<TChildProps>>) {
    return ReactApollo.withQuery<TProps, CapacityTypeListQuery, CapacityTypeListQueryVariables, CapacityTypeListProps<TChildProps>>(CapacityTypeListDocument, {
      alias: 'withCapacityTypeList',
      ...operationOptions
    });
};

    export function useCapacityTypeListQuery(baseOptions?: ReactApolloHooks.QueryHookOptions<CapacityTypeListQueryVariables>) {
      return ReactApolloHooks.useQuery<CapacityTypeListQuery, CapacityTypeListQueryVariables>(CapacityTypeListDocument, baseOptions);
    };
export type CapacityTypeListQueryHookResult = ReturnType<typeof useCapacityTypeListQuery>;
export const LoginDocument = gql`
    mutation Login($username: String, $password: String) {
  login(password: $password, username: $username) {
    operatorId
    passwordExpireInDays
    permissions
    role
    token
    userId
    username
  }
}
    `;
export type LoginMutationFn = ReactApollo.MutationFn<LoginMutation, LoginMutationVariables>;
export type LoginComponentProps = Omit<ReactApollo.MutationProps<LoginMutation, LoginMutationVariables>, 'mutation'>;

    export const LoginComponent = (props: LoginComponentProps) => (
      <ReactApollo.Mutation<LoginMutation, LoginMutationVariables> mutation={LoginDocument} {...props} />
    );
    
export type LoginProps<TChildProps = {}> = Partial<ReactApollo.MutateProps<LoginMutation, LoginMutationVariables>> & TChildProps;
export function withLogin<TProps, TChildProps = {}>(operationOptions?: ReactApollo.OperationOption<
  TProps,
  LoginMutation,
  LoginMutationVariables,
  LoginProps<TChildProps>>) {
    return ReactApollo.withMutation<TProps, LoginMutation, LoginMutationVariables, LoginProps<TChildProps>>(LoginDocument, {
      alias: 'withLogin',
      ...operationOptions
    });
};

    export function useLoginMutation(baseOptions?: ReactApolloHooks.MutationHookOptions<LoginMutation, LoginMutationVariables>) {
      return ReactApolloHooks.useMutation<LoginMutation, LoginMutationVariables>(LoginDocument, baseOptions);
    };
export type LoginMutationHookResult = ReturnType<typeof useLoginMutation>;
export const ContactListDocument = gql`
    query ContactList($ids: String) {
  contacts(ids: $ids) {
    results {
      id
      name {
        fi
        en
        sv
      }
      operatorId
      phone
      email
      address {
        streetAddress {
          fi
          en
          sv
        }
        postalCode
        city {
          fi
          en
          sv
        }
      }
      openingHours {
        fi
        en
        sv
      }
      info {
        fi
        en
        sv
      }
      hasMore
    }
  }
}
    `;
export type ContactListComponentProps = Omit<ReactApollo.QueryProps<ContactListQuery, ContactListQueryVariables>, 'query'>;

    export const ContactListComponent = (props: ContactListComponentProps) => (
      <ReactApollo.Query<ContactListQuery, ContactListQueryVariables> query={ContactListDocument} {...props} />
    );
    
export type ContactListProps<TChildProps = {}> = Partial<ReactApollo.DataProps<ContactListQuery, ContactListQueryVariables>> & TChildProps;
export function withContactList<TProps, TChildProps = {}>(operationOptions?: ReactApollo.OperationOption<
  TProps,
  ContactListQuery,
  ContactListQueryVariables,
  ContactListProps<TChildProps>>) {
    return ReactApollo.withQuery<TProps, ContactListQuery, ContactListQueryVariables, ContactListProps<TChildProps>>(ContactListDocument, {
      alias: 'withContactList',
      ...operationOptions
    });
};

    export function useContactListQuery(baseOptions?: ReactApolloHooks.QueryHookOptions<ContactListQueryVariables>) {
      return ReactApolloHooks.useQuery<ContactListQuery, ContactListQueryVariables>(ContactListDocument, baseOptions);
    };
export type ContactListQueryHookResult = ReturnType<typeof useContactListQuery>;
export const FacilityListDocument = gql`
    query FacilityList($ids: [Int]) {
  facilities(ids: $ids) {
    totalCount
    facilities {
      id
      name {
        fi
      }
      status
      location {
        crs {
          type
          properties {
            name
          }
        }
        bbox
        type
        coordinates
      }
      builtCapacity {
        CAR
        ELECTRIC_CAR
        BICYCLE_SECURE_SPACE
        DISABLED
        MOTORCYCLE
        BICYCLE
      }
      usages
    }
    hasMore
  }
}
    `;
export type FacilityListComponentProps = Omit<ReactApollo.QueryProps<FacilityListQuery, FacilityListQueryVariables>, 'query'>;

    export const FacilityListComponent = (props: FacilityListComponentProps) => (
      <ReactApollo.Query<FacilityListQuery, FacilityListQueryVariables> query={FacilityListDocument} {...props} />
    );
    
export type FacilityListProps<TChildProps = {}> = Partial<ReactApollo.DataProps<FacilityListQuery, FacilityListQueryVariables>> & TChildProps;
export function withFacilityList<TProps, TChildProps = {}>(operationOptions?: ReactApollo.OperationOption<
  TProps,
  FacilityListQuery,
  FacilityListQueryVariables,
  FacilityListProps<TChildProps>>) {
    return ReactApollo.withQuery<TProps, FacilityListQuery, FacilityListQueryVariables, FacilityListProps<TChildProps>>(FacilityListDocument, {
      alias: 'withFacilityList',
      ...operationOptions
    });
};

    export function useFacilityListQuery(baseOptions?: ReactApolloHooks.QueryHookOptions<FacilityListQueryVariables>) {
      return ReactApolloHooks.useQuery<FacilityListQuery, FacilityListQueryVariables>(FacilityListDocument, baseOptions);
    };
export type FacilityListQueryHookResult = ReturnType<typeof useFacilityListQuery>;
export const FacilityProfileDocument = gql`
    query FacilityProfile($id: ID!) {
  facilityDetails(id: $id) {
    id
    name {
      fi
      sv
      en
    }
    location {
      crs {
        type
        properties {
          name
        }
      }
      bbox
      type
      coordinates
    }
    aliases
    services
    status
    statusDescription {
      fi
      sv
      en
    }
    operatorId
    builtCapacity {
      CAR
      BICYCLE
      ELECTRIC_CAR
      BICYCLE_SECURE_SPACE
      DISABLED
      MOTORCYCLE
      BICYCLE
    }
    unavailableCapacities {
      capacityType
      usage
      capacity
    }
    ports {
      location {
        crs {
          type
          properties {
            name
          }
        }
        type
        coordinates
      }
      entry
      exit
      pedestrian
      bicycle
      address {
        streetAddress {
          fi
          sv
          en
        }
        postalCode
        city {
          fi
          sv
          en
        }
      }
      info {
        fi
        sv
        en
      }
    }
    pricingMethod
    pricing {
      usage
      capacityType
      maxCapacity
      dayType
      time {
        from
        until
      }
      price {
        fi
        sv
        en
      }
    }
    paymentInfo {
      detail {
        fi
        sv
        en
      }
      paymentMethods
      url {
        fi
        sv
        en
      }
    }
    contacts {
      emergency
      operator
      service
    }
    openingHours {
      openNow
      byDayType {
        BUSINESS_DAY {
          from
          until
        }
        SUNDAY {
          from
          until
        }
        SATURDAY {
          from
          until
        }
      }
      info {
        fi
        sv
        en
      }
      url {
        fi
        sv
        en
      }
    }
  }
}
    `;
export type FacilityProfileComponentProps = Omit<ReactApollo.QueryProps<FacilityProfileQuery, FacilityProfileQueryVariables>, 'query'> & ({ variables: FacilityProfileQueryVariables; skip?: false; } | { skip: true; });

    export const FacilityProfileComponent = (props: FacilityProfileComponentProps) => (
      <ReactApollo.Query<FacilityProfileQuery, FacilityProfileQueryVariables> query={FacilityProfileDocument} {...props} />
    );
    
export type FacilityProfileProps<TChildProps = {}> = Partial<ReactApollo.DataProps<FacilityProfileQuery, FacilityProfileQueryVariables>> & TChildProps;
export function withFacilityProfile<TProps, TChildProps = {}>(operationOptions?: ReactApollo.OperationOption<
  TProps,
  FacilityProfileQuery,
  FacilityProfileQueryVariables,
  FacilityProfileProps<TChildProps>>) {
    return ReactApollo.withQuery<TProps, FacilityProfileQuery, FacilityProfileQueryVariables, FacilityProfileProps<TChildProps>>(FacilityProfileDocument, {
      alias: 'withFacilityProfile',
      ...operationOptions
    });
};

    export function useFacilityProfileQuery(baseOptions?: ReactApolloHooks.QueryHookOptions<FacilityProfileQueryVariables>) {
      return ReactApolloHooks.useQuery<FacilityProfileQuery, FacilityProfileQueryVariables>(FacilityProfileDocument, baseOptions);
    };
export type FacilityProfileQueryHookResult = ReturnType<typeof useFacilityProfileQuery>;
export const OperatorDetailsDocument = gql`
    query OperatorDetails($id: ID!) {
  operatorDetails(id: $id) {
    id
    name {
      fi
      sv
      en
    }
  }
}
    `;
export type OperatorDetailsComponentProps = Omit<ReactApollo.QueryProps<OperatorDetailsQuery, OperatorDetailsQueryVariables>, 'query'> & ({ variables: OperatorDetailsQueryVariables; skip?: false; } | { skip: true; });

    export const OperatorDetailsComponent = (props: OperatorDetailsComponentProps) => (
      <ReactApollo.Query<OperatorDetailsQuery, OperatorDetailsQueryVariables> query={OperatorDetailsDocument} {...props} />
    );
    
export type OperatorDetailsProps<TChildProps = {}> = Partial<ReactApollo.DataProps<OperatorDetailsQuery, OperatorDetailsQueryVariables>> & TChildProps;
export function withOperatorDetails<TProps, TChildProps = {}>(operationOptions?: ReactApollo.OperationOption<
  TProps,
  OperatorDetailsQuery,
  OperatorDetailsQueryVariables,
  OperatorDetailsProps<TChildProps>>) {
    return ReactApollo.withQuery<TProps, OperatorDetailsQuery, OperatorDetailsQueryVariables, OperatorDetailsProps<TChildProps>>(OperatorDetailsDocument, {
      alias: 'withOperatorDetails',
      ...operationOptions
    });
};

    export function useOperatorDetailsQuery(baseOptions?: ReactApolloHooks.QueryHookOptions<OperatorDetailsQueryVariables>) {
      return ReactApolloHooks.useQuery<OperatorDetailsQuery, OperatorDetailsQueryVariables>(OperatorDetailsDocument, baseOptions);
    };
export type OperatorDetailsQueryHookResult = ReturnType<typeof useOperatorDetailsQuery>;
export const ContactsDocument = gql`
    query Contacts($ids: String) {
  contacts(ids: $ids) {
    results {
      id
      name {
        fi
        sv
        en
      }
      operatorId
      phone
      email
      address {
        streetAddress {
          fi
          en
          sv
        }
        postalCode
        city {
          fi
          en
          sv
        }
      }
      openingHours {
        fi
        en
        sv
      }
      info {
        fi
        sv
        en
      }
    }
  }
}
    `;
export type ContactsComponentProps = Omit<ReactApollo.QueryProps<ContactsQuery, ContactsQueryVariables>, 'query'>;

    export const ContactsComponent = (props: ContactsComponentProps) => (
      <ReactApollo.Query<ContactsQuery, ContactsQueryVariables> query={ContactsDocument} {...props} />
    );
    
export type ContactsProps<TChildProps = {}> = Partial<ReactApollo.DataProps<ContactsQuery, ContactsQueryVariables>> & TChildProps;
export function withContacts<TProps, TChildProps = {}>(operationOptions?: ReactApollo.OperationOption<
  TProps,
  ContactsQuery,
  ContactsQueryVariables,
  ContactsProps<TChildProps>>) {
    return ReactApollo.withQuery<TProps, ContactsQuery, ContactsQueryVariables, ContactsProps<TChildProps>>(ContactsDocument, {
      alias: 'withContacts',
      ...operationOptions
    });
};

    export function useContactsQuery(baseOptions?: ReactApolloHooks.QueryHookOptions<ContactsQueryVariables>) {
      return ReactApolloHooks.useQuery<ContactsQuery, ContactsQueryVariables>(ContactsDocument, baseOptions);
    };
export type ContactsQueryHookResult = ReturnType<typeof useContactsQuery>;
export const ContactDetailsDocument = gql`
    query ContactDetails($id: ID!) {
  contactDetails(id: $id) {
    id
    name {
      fi
      sv
      en
    }
    operatorId
    phone
    email
    address {
      streetAddress {
        fi
        en
        sv
      }
      postalCode
      city {
        fi
        en
        sv
      }
    }
    openingHours {
      fi
      en
      sv
    }
    info {
      fi
      sv
      en
    }
  }
}
    `;
export type ContactDetailsComponentProps = Omit<ReactApollo.QueryProps<ContactDetailsQuery, ContactDetailsQueryVariables>, 'query'> & ({ variables: ContactDetailsQueryVariables; skip?: false; } | { skip: true; });

    export const ContactDetailsComponent = (props: ContactDetailsComponentProps) => (
      <ReactApollo.Query<ContactDetailsQuery, ContactDetailsQueryVariables> query={ContactDetailsDocument} {...props} />
    );
    
export type ContactDetailsProps<TChildProps = {}> = Partial<ReactApollo.DataProps<ContactDetailsQuery, ContactDetailsQueryVariables>> & TChildProps;
export function withContactDetails<TProps, TChildProps = {}>(operationOptions?: ReactApollo.OperationOption<
  TProps,
  ContactDetailsQuery,
  ContactDetailsQueryVariables,
  ContactDetailsProps<TChildProps>>) {
    return ReactApollo.withQuery<TProps, ContactDetailsQuery, ContactDetailsQueryVariables, ContactDetailsProps<TChildProps>>(ContactDetailsDocument, {
      alias: 'withContactDetails',
      ...operationOptions
    });
};

    export function useContactDetailsQuery(baseOptions?: ReactApolloHooks.QueryHookOptions<ContactDetailsQueryVariables>) {
      return ReactApolloHooks.useQuery<ContactDetailsQuery, ContactDetailsQueryVariables>(ContactDetailsDocument, baseOptions);
    };
export type ContactDetailsQueryHookResult = ReturnType<typeof useContactDetailsQuery>;
export const HubsByFacilityDocument = gql`
    query HubsByFacility($facilityIds: [Int]) {
  hubs(facilityIds: $facilityIds) {
    totalCount
    hubs {
      id
      name {
        fi
      }
    }
  }
}
    `;
export type HubsByFacilityComponentProps = Omit<ReactApollo.QueryProps<HubsByFacilityQuery, HubsByFacilityQueryVariables>, 'query'>;

    export const HubsByFacilityComponent = (props: HubsByFacilityComponentProps) => (
      <ReactApollo.Query<HubsByFacilityQuery, HubsByFacilityQueryVariables> query={HubsByFacilityDocument} {...props} />
    );
    
export type HubsByFacilityProps<TChildProps = {}> = Partial<ReactApollo.DataProps<HubsByFacilityQuery, HubsByFacilityQueryVariables>> & TChildProps;
export function withHubsByFacility<TProps, TChildProps = {}>(operationOptions?: ReactApollo.OperationOption<
  TProps,
  HubsByFacilityQuery,
  HubsByFacilityQueryVariables,
  HubsByFacilityProps<TChildProps>>) {
    return ReactApollo.withQuery<TProps, HubsByFacilityQuery, HubsByFacilityQueryVariables, HubsByFacilityProps<TChildProps>>(HubsByFacilityDocument, {
      alias: 'withHubsByFacility',
      ...operationOptions
    });
};

    export function useHubsByFacilityQuery(baseOptions?: ReactApolloHooks.QueryHookOptions<HubsByFacilityQueryVariables>) {
      return ReactApolloHooks.useQuery<HubsByFacilityQuery, HubsByFacilityQueryVariables>(HubsByFacilityDocument, baseOptions);
    };
export type HubsByFacilityQueryHookResult = ReturnType<typeof useHubsByFacilityQuery>;
export const FacilityUtilizationDocument = gql`
    query FacilityUtilization($id: ID!) {
  facilityUtilization(id: $id) {
    facilityId
    capacityType
    usage
    timestamp
    spacesAvailable
    capacity
    openNow
  }
}
    `;
export type FacilityUtilizationComponentProps = Omit<ReactApollo.QueryProps<FacilityUtilizationQuery, FacilityUtilizationQueryVariables>, 'query'> & ({ variables: FacilityUtilizationQueryVariables; skip?: false; } | { skip: true; });

    export const FacilityUtilizationComponent = (props: FacilityUtilizationComponentProps) => (
      <ReactApollo.Query<FacilityUtilizationQuery, FacilityUtilizationQueryVariables> query={FacilityUtilizationDocument} {...props} />
    );
    
export type FacilityUtilizationProps<TChildProps = {}> = Partial<ReactApollo.DataProps<FacilityUtilizationQuery, FacilityUtilizationQueryVariables>> & TChildProps;
export function withFacilityUtilization<TProps, TChildProps = {}>(operationOptions?: ReactApollo.OperationOption<
  TProps,
  FacilityUtilizationQuery,
  FacilityUtilizationQueryVariables,
  FacilityUtilizationProps<TChildProps>>) {
    return ReactApollo.withQuery<TProps, FacilityUtilizationQuery, FacilityUtilizationQueryVariables, FacilityUtilizationProps<TChildProps>>(FacilityUtilizationDocument, {
      alias: 'withFacilityUtilization',
      ...operationOptions
    });
};

    export function useFacilityUtilizationQuery(baseOptions?: ReactApolloHooks.QueryHookOptions<FacilityUtilizationQueryVariables>) {
      return ReactApolloHooks.useQuery<FacilityUtilizationQuery, FacilityUtilizationQueryVariables>(FacilityUtilizationDocument, baseOptions);
    };
export type FacilityUtilizationQueryHookResult = ReturnType<typeof useFacilityUtilizationQuery>;
export const HubListDocument = gql`
    query HubList {
  hubs {
    totalCount
    hubs {
      id
      name {
        fi
      }
      facilityIds
    }
    hasMore
  }
}
    `;
export type HubListComponentProps = Omit<ReactApollo.QueryProps<HubListQuery, HubListQueryVariables>, 'query'>;

    export const HubListComponent = (props: HubListComponentProps) => (
      <ReactApollo.Query<HubListQuery, HubListQueryVariables> query={HubListDocument} {...props} />
    );
    
export type HubListProps<TChildProps = {}> = Partial<ReactApollo.DataProps<HubListQuery, HubListQueryVariables>> & TChildProps;
export function withHubList<TProps, TChildProps = {}>(operationOptions?: ReactApollo.OperationOption<
  TProps,
  HubListQuery,
  HubListQueryVariables,
  HubListProps<TChildProps>>) {
    return ReactApollo.withQuery<TProps, HubListQuery, HubListQueryVariables, HubListProps<TChildProps>>(HubListDocument, {
      alias: 'withHubList',
      ...operationOptions
    });
};

    export function useHubListQuery(baseOptions?: ReactApolloHooks.QueryHookOptions<HubListQueryVariables>) {
      return ReactApolloHooks.useQuery<HubListQuery, HubListQueryVariables>(HubListDocument, baseOptions);
    };
export type HubListQueryHookResult = ReturnType<typeof useHubListQuery>;
export const HubProfileDocument = gql`
    query HubProfile($id: ID!) {
  hubDetails(id: $id) {
    id
    name {
      fi
      sv
      en
    }
    location {
      crs {
        type
        properties {
          name
        }
      }
      type
      coordinates
    }
    address {
      streetAddress {
        fi
        en
        sv
      }
      postalCode
      city {
        fi
        en
        sv
      }
    }
    facilityIds
  }
}
    `;
export type HubProfileComponentProps = Omit<ReactApollo.QueryProps<HubProfileQuery, HubProfileQueryVariables>, 'query'> & ({ variables: HubProfileQueryVariables; skip?: false; } | { skip: true; });

    export const HubProfileComponent = (props: HubProfileComponentProps) => (
      <ReactApollo.Query<HubProfileQuery, HubProfileQueryVariables> query={HubProfileDocument} {...props} />
    );
    
export type HubProfileProps<TChildProps = {}> = Partial<ReactApollo.DataProps<HubProfileQuery, HubProfileQueryVariables>> & TChildProps;
export function withHubProfile<TProps, TChildProps = {}>(operationOptions?: ReactApollo.OperationOption<
  TProps,
  HubProfileQuery,
  HubProfileQueryVariables,
  HubProfileProps<TChildProps>>) {
    return ReactApollo.withQuery<TProps, HubProfileQuery, HubProfileQueryVariables, HubProfileProps<TChildProps>>(HubProfileDocument, {
      alias: 'withHubProfile',
      ...operationOptions
    });
};

    export function useHubProfileQuery(baseOptions?: ReactApolloHooks.QueryHookOptions<HubProfileQueryVariables>) {
      return ReactApolloHooks.useQuery<HubProfileQuery, HubProfileQueryVariables>(HubProfileDocument, baseOptions);
    };
export type HubProfileQueryHookResult = ReturnType<typeof useHubProfileQuery>;
export const OperatorListDocument = gql`
    query OperatorList {
  operators {
    results {
      id
      name {
        fi
        en
        sv
      }
    }
    hasMore
  }
}
    `;
export type OperatorListComponentProps = Omit<ReactApollo.QueryProps<OperatorListQuery, OperatorListQueryVariables>, 'query'>;

    export const OperatorListComponent = (props: OperatorListComponentProps) => (
      <ReactApollo.Query<OperatorListQuery, OperatorListQueryVariables> query={OperatorListDocument} {...props} />
    );
    
export type OperatorListProps<TChildProps = {}> = Partial<ReactApollo.DataProps<OperatorListQuery, OperatorListQueryVariables>> & TChildProps;
export function withOperatorList<TProps, TChildProps = {}>(operationOptions?: ReactApollo.OperationOption<
  TProps,
  OperatorListQuery,
  OperatorListQueryVariables,
  OperatorListProps<TChildProps>>) {
    return ReactApollo.withQuery<TProps, OperatorListQuery, OperatorListQueryVariables, OperatorListProps<TChildProps>>(OperatorListDocument, {
      alias: 'withOperatorList',
      ...operationOptions
    });
};

    export function useOperatorListQuery(baseOptions?: ReactApolloHooks.QueryHookOptions<OperatorListQueryVariables>) {
      return ReactApolloHooks.useQuery<OperatorListQuery, OperatorListQueryVariables>(OperatorListDocument, baseOptions);
    };
export type OperatorListQueryHookResult = ReturnType<typeof useOperatorListQuery>;
export const RegionListDocument = gql`
    query RegionList {
  regions {
    id
    name {
      fi
      sv
      en
    }
  }
}
    `;
export type RegionListComponentProps = Omit<ReactApollo.QueryProps<RegionListQuery, RegionListQueryVariables>, 'query'>;

    export const RegionListComponent = (props: RegionListComponentProps) => (
      <ReactApollo.Query<RegionListQuery, RegionListQueryVariables> query={RegionListDocument} {...props} />
    );
    
export type RegionListProps<TChildProps = {}> = Partial<ReactApollo.DataProps<RegionListQuery, RegionListQueryVariables>> & TChildProps;
export function withRegionList<TProps, TChildProps = {}>(operationOptions?: ReactApollo.OperationOption<
  TProps,
  RegionListQuery,
  RegionListQueryVariables,
  RegionListProps<TChildProps>>) {
    return ReactApollo.withQuery<TProps, RegionListQuery, RegionListQueryVariables, RegionListProps<TChildProps>>(RegionListDocument, {
      alias: 'withRegionList',
      ...operationOptions
    });
};

    export function useRegionListQuery(baseOptions?: ReactApolloHooks.QueryHookOptions<RegionListQueryVariables>) {
      return ReactApolloHooks.useQuery<RegionListQuery, RegionListQueryVariables>(RegionListDocument, baseOptions);
    };
export type RegionListQueryHookResult = ReturnType<typeof useRegionListQuery>;
export const RegionsWithHubsDocument = gql`
    query RegionsWithHubs {
  regionsWithHubs {
    id
    name {
      fi
      sv
      en
    }
    hubIds
  }
}
    `;
export type RegionsWithHubsComponentProps = Omit<ReactApollo.QueryProps<RegionsWithHubsQuery, RegionsWithHubsQueryVariables>, 'query'>;

    export const RegionsWithHubsComponent = (props: RegionsWithHubsComponentProps) => (
      <ReactApollo.Query<RegionsWithHubsQuery, RegionsWithHubsQueryVariables> query={RegionsWithHubsDocument} {...props} />
    );
    
export type RegionsWithHubsProps<TChildProps = {}> = Partial<ReactApollo.DataProps<RegionsWithHubsQuery, RegionsWithHubsQueryVariables>> & TChildProps;
export function withRegionsWithHubs<TProps, TChildProps = {}>(operationOptions?: ReactApollo.OperationOption<
  TProps,
  RegionsWithHubsQuery,
  RegionsWithHubsQueryVariables,
  RegionsWithHubsProps<TChildProps>>) {
    return ReactApollo.withQuery<TProps, RegionsWithHubsQuery, RegionsWithHubsQueryVariables, RegionsWithHubsProps<TChildProps>>(RegionsWithHubsDocument, {
      alias: 'withRegionsWithHubs',
      ...operationOptions
    });
};

    export function useRegionsWithHubsQuery(baseOptions?: ReactApolloHooks.QueryHookOptions<RegionsWithHubsQueryVariables>) {
      return ReactApolloHooks.useQuery<RegionsWithHubsQuery, RegionsWithHubsQueryVariables>(RegionsWithHubsDocument, baseOptions);
    };
export type RegionsWithHubsQueryHookResult = ReturnType<typeof useRegionsWithHubsQuery>;
export const FacilityUsageReportDocument = gql`
    mutation FacilityUsageReport($capacityTypes: [String!], $endDate: [Int!], $facilities: [Int!], $hubs: [Int!], $interval: Int, $operators: [Int!], $regions: [Int!], $startDate: [Int!], $usages: [Int!]) {
  facilityUsageReport(capacityTypes: $capacityTypes, endDate: $endDate, facilities: $facilities, hubs: $hubs, interval: $interval, operators: $operators, regions: $regions, startDate: $startDate, usages: $usages)
}
    `;
export type FacilityUsageReportMutationFn = ReactApollo.MutationFn<FacilityUsageReportMutation, FacilityUsageReportMutationVariables>;
export type FacilityUsageReportComponentProps = Omit<ReactApollo.MutationProps<FacilityUsageReportMutation, FacilityUsageReportMutationVariables>, 'mutation'>;

    export const FacilityUsageReportComponent = (props: FacilityUsageReportComponentProps) => (
      <ReactApollo.Mutation<FacilityUsageReportMutation, FacilityUsageReportMutationVariables> mutation={FacilityUsageReportDocument} {...props} />
    );
    
export type FacilityUsageReportProps<TChildProps = {}> = Partial<ReactApollo.MutateProps<FacilityUsageReportMutation, FacilityUsageReportMutationVariables>> & TChildProps;
export function withFacilityUsageReport<TProps, TChildProps = {}>(operationOptions?: ReactApollo.OperationOption<
  TProps,
  FacilityUsageReportMutation,
  FacilityUsageReportMutationVariables,
  FacilityUsageReportProps<TChildProps>>) {
    return ReactApollo.withMutation<TProps, FacilityUsageReportMutation, FacilityUsageReportMutationVariables, FacilityUsageReportProps<TChildProps>>(FacilityUsageReportDocument, {
      alias: 'withFacilityUsageReport',
      ...operationOptions
    });
};

    export function useFacilityUsageReportMutation(baseOptions?: ReactApolloHooks.MutationHookOptions<FacilityUsageReportMutation, FacilityUsageReportMutationVariables>) {
      return ReactApolloHooks.useMutation<FacilityUsageReportMutation, FacilityUsageReportMutationVariables>(FacilityUsageReportDocument, baseOptions);
    };
export type FacilityUsageReportMutationHookResult = ReturnType<typeof useFacilityUsageReportMutation>;
export const HubsAndFacilitiesReportDocument = gql`
    mutation HubsAndFacilitiesReport {
  hubsAndFacilitiesReport
}
    `;
export type HubsAndFacilitiesReportMutationFn = ReactApollo.MutationFn<HubsAndFacilitiesReportMutation, HubsAndFacilitiesReportMutationVariables>;
export type HubsAndFacilitiesReportComponentProps = Omit<ReactApollo.MutationProps<HubsAndFacilitiesReportMutation, HubsAndFacilitiesReportMutationVariables>, 'mutation'>;

    export const HubsAndFacilitiesReportComponent = (props: HubsAndFacilitiesReportComponentProps) => (
      <ReactApollo.Mutation<HubsAndFacilitiesReportMutation, HubsAndFacilitiesReportMutationVariables> mutation={HubsAndFacilitiesReportDocument} {...props} />
    );
    
export type HubsAndFacilitiesReportProps<TChildProps = {}> = Partial<ReactApollo.MutateProps<HubsAndFacilitiesReportMutation, HubsAndFacilitiesReportMutationVariables>> & TChildProps;
export function withHubsAndFacilitiesReport<TProps, TChildProps = {}>(operationOptions?: ReactApollo.OperationOption<
  TProps,
  HubsAndFacilitiesReportMutation,
  HubsAndFacilitiesReportMutationVariables,
  HubsAndFacilitiesReportProps<TChildProps>>) {
    return ReactApollo.withMutation<TProps, HubsAndFacilitiesReportMutation, HubsAndFacilitiesReportMutationVariables, HubsAndFacilitiesReportProps<TChildProps>>(HubsAndFacilitiesReportDocument, {
      alias: 'withHubsAndFacilitiesReport',
      ...operationOptions
    });
};

    export function useHubsAndFacilitiesReportMutation(baseOptions?: ReactApolloHooks.MutationHookOptions<HubsAndFacilitiesReportMutation, HubsAndFacilitiesReportMutationVariables>) {
      return ReactApolloHooks.useMutation<HubsAndFacilitiesReportMutation, HubsAndFacilitiesReportMutationVariables>(HubsAndFacilitiesReportDocument, baseOptions);
    };
export type HubsAndFacilitiesReportMutationHookResult = ReturnType<typeof useHubsAndFacilitiesReportMutation>;
export const MaxUtilizationReportDocument = gql`
    mutation MaxUtilizationReport($capacityTypes: [String!], $endDate: [Int!], $facilities: [Int!], $hubs: [Int!], $operators: [Int!], $regions: [Int!], $startDate: [Int!], $usages: [Int!]) {
  maxUtilizationReport(capacityTypes: $capacityTypes, endDate: $endDate, facilities: $facilities, hubs: $hubs, operators: $operators, regions: $regions, startDate: $startDate, usages: $usages)
}
    `;
export type MaxUtilizationReportMutationFn = ReactApollo.MutationFn<MaxUtilizationReportMutation, MaxUtilizationReportMutationVariables>;
export type MaxUtilizationReportComponentProps = Omit<ReactApollo.MutationProps<MaxUtilizationReportMutation, MaxUtilizationReportMutationVariables>, 'mutation'>;

    export const MaxUtilizationReportComponent = (props: MaxUtilizationReportComponentProps) => (
      <ReactApollo.Mutation<MaxUtilizationReportMutation, MaxUtilizationReportMutationVariables> mutation={MaxUtilizationReportDocument} {...props} />
    );
    
export type MaxUtilizationReportProps<TChildProps = {}> = Partial<ReactApollo.MutateProps<MaxUtilizationReportMutation, MaxUtilizationReportMutationVariables>> & TChildProps;
export function withMaxUtilizationReport<TProps, TChildProps = {}>(operationOptions?: ReactApollo.OperationOption<
  TProps,
  MaxUtilizationReportMutation,
  MaxUtilizationReportMutationVariables,
  MaxUtilizationReportProps<TChildProps>>) {
    return ReactApollo.withMutation<TProps, MaxUtilizationReportMutation, MaxUtilizationReportMutationVariables, MaxUtilizationReportProps<TChildProps>>(MaxUtilizationReportDocument, {
      alias: 'withMaxUtilizationReport',
      ...operationOptions
    });
};

    export function useMaxUtilizationReportMutation(baseOptions?: ReactApolloHooks.MutationHookOptions<MaxUtilizationReportMutation, MaxUtilizationReportMutationVariables>) {
      return ReactApolloHooks.useMutation<MaxUtilizationReportMutation, MaxUtilizationReportMutationVariables>(MaxUtilizationReportDocument, baseOptions);
    };
export type MaxUtilizationReportMutationHookResult = ReturnType<typeof useMaxUtilizationReportMutation>;
export const RequestLogReportDocument = gql`
    mutation RequestLogReport($endDate: [Int!], $requestLogInterval: String!, $startDate: [Int!]) {
  requestLogReport(endDate: $endDate, requestLogInterval: $requestLogInterval, startDate: $startDate)
}
    `;
export type RequestLogReportMutationFn = ReactApollo.MutationFn<RequestLogReportMutation, RequestLogReportMutationVariables>;
export type RequestLogReportComponentProps = Omit<ReactApollo.MutationProps<RequestLogReportMutation, RequestLogReportMutationVariables>, 'mutation'>;

    export const RequestLogReportComponent = (props: RequestLogReportComponentProps) => (
      <ReactApollo.Mutation<RequestLogReportMutation, RequestLogReportMutationVariables> mutation={RequestLogReportDocument} {...props} />
    );
    
export type RequestLogReportProps<TChildProps = {}> = Partial<ReactApollo.MutateProps<RequestLogReportMutation, RequestLogReportMutationVariables>> & TChildProps;
export function withRequestLogReport<TProps, TChildProps = {}>(operationOptions?: ReactApollo.OperationOption<
  TProps,
  RequestLogReportMutation,
  RequestLogReportMutationVariables,
  RequestLogReportProps<TChildProps>>) {
    return ReactApollo.withMutation<TProps, RequestLogReportMutation, RequestLogReportMutationVariables, RequestLogReportProps<TChildProps>>(RequestLogReportDocument, {
      alias: 'withRequestLogReport',
      ...operationOptions
    });
};

    export function useRequestLogReportMutation(baseOptions?: ReactApolloHooks.MutationHookOptions<RequestLogReportMutation, RequestLogReportMutationVariables>) {
      return ReactApolloHooks.useMutation<RequestLogReportMutation, RequestLogReportMutationVariables>(RequestLogReportDocument, baseOptions);
    };
export type RequestLogReportMutationHookResult = ReturnType<typeof useRequestLogReportMutation>;
export const UsageListDocument = gql`
    query UsageList {
  usages
}
    `;
export type UsageListComponentProps = Omit<ReactApollo.QueryProps<UsageListQuery, UsageListQueryVariables>, 'query'>;

    export const UsageListComponent = (props: UsageListComponentProps) => (
      <ReactApollo.Query<UsageListQuery, UsageListQueryVariables> query={UsageListDocument} {...props} />
    );
    
export type UsageListProps<TChildProps = {}> = Partial<ReactApollo.DataProps<UsageListQuery, UsageListQueryVariables>> & TChildProps;
export function withUsageList<TProps, TChildProps = {}>(operationOptions?: ReactApollo.OperationOption<
  TProps,
  UsageListQuery,
  UsageListQueryVariables,
  UsageListProps<TChildProps>>) {
    return ReactApollo.withQuery<TProps, UsageListQuery, UsageListQueryVariables, UsageListProps<TChildProps>>(UsageListDocument, {
      alias: 'withUsageList',
      ...operationOptions
    });
};

    export function useUsageListQuery(baseOptions?: ReactApolloHooks.QueryHookOptions<UsageListQueryVariables>) {
      return ReactApolloHooks.useQuery<UsageListQuery, UsageListQueryVariables>(UsageListDocument, baseOptions);
    };
export type UsageListQueryHookResult = ReturnType<typeof useUsageListQuery>;
export const UserListDocument = gql`
    query UserList {
  users {
    id
    username
    operatorId
    role
  }
}
    `;
export type UserListComponentProps = Omit<ReactApollo.QueryProps<UserListQuery, UserListQueryVariables>, 'query'>;

    export const UserListComponent = (props: UserListComponentProps) => (
      <ReactApollo.Query<UserListQuery, UserListQueryVariables> query={UserListDocument} {...props} />
    );
    
export type UserListProps<TChildProps = {}> = Partial<ReactApollo.DataProps<UserListQuery, UserListQueryVariables>> & TChildProps;
export function withUserList<TProps, TChildProps = {}>(operationOptions?: ReactApollo.OperationOption<
  TProps,
  UserListQuery,
  UserListQueryVariables,
  UserListProps<TChildProps>>) {
    return ReactApollo.withQuery<TProps, UserListQuery, UserListQueryVariables, UserListProps<TChildProps>>(UserListDocument, {
      alias: 'withUserList',
      ...operationOptions
    });
};

    export function useUserListQuery(baseOptions?: ReactApolloHooks.QueryHookOptions<UserListQueryVariables>) {
      return ReactApolloHooks.useQuery<UserListQuery, UserListQueryVariables>(UserListDocument, baseOptions);
    };
export type UserListQueryHookResult = ReturnType<typeof useUserListQuery>;
export const OperatorNamesDocument = gql`
    query OperatorNames {
  operators {
    results {
      id
      name {
        fi
      }
    }
  }
}
    `;
export type OperatorNamesComponentProps = Omit<ReactApollo.QueryProps<OperatorNamesQuery, OperatorNamesQueryVariables>, 'query'>;

    export const OperatorNamesComponent = (props: OperatorNamesComponentProps) => (
      <ReactApollo.Query<OperatorNamesQuery, OperatorNamesQueryVariables> query={OperatorNamesDocument} {...props} />
    );
    
export type OperatorNamesProps<TChildProps = {}> = Partial<ReactApollo.DataProps<OperatorNamesQuery, OperatorNamesQueryVariables>> & TChildProps;
export function withOperatorNames<TProps, TChildProps = {}>(operationOptions?: ReactApollo.OperationOption<
  TProps,
  OperatorNamesQuery,
  OperatorNamesQueryVariables,
  OperatorNamesProps<TChildProps>>) {
    return ReactApollo.withQuery<TProps, OperatorNamesQuery, OperatorNamesQueryVariables, OperatorNamesProps<TChildProps>>(OperatorNamesDocument, {
      alias: 'withOperatorNames',
      ...operationOptions
    });
};

    export function useOperatorNamesQuery(baseOptions?: ReactApolloHooks.QueryHookOptions<OperatorNamesQueryVariables>) {
      return ReactApolloHooks.useQuery<OperatorNamesQuery, OperatorNamesQueryVariables>(OperatorNamesDocument, baseOptions);
    };
export type OperatorNamesQueryHookResult = ReturnType<typeof useOperatorNamesQuery>;
export const ChangePasswordDocument = gql`
    mutation ChangePassword($id: Int!, $newPassword: String!) {
  changePassword(id: $id, newPassword: $newPassword)
}
    `;
export type ChangePasswordMutationFn = ReactApollo.MutationFn<ChangePasswordMutation, ChangePasswordMutationVariables>;
export type ChangePasswordComponentProps = Omit<ReactApollo.MutationProps<ChangePasswordMutation, ChangePasswordMutationVariables>, 'mutation'>;

    export const ChangePasswordComponent = (props: ChangePasswordComponentProps) => (
      <ReactApollo.Mutation<ChangePasswordMutation, ChangePasswordMutationVariables> mutation={ChangePasswordDocument} {...props} />
    );
    
export type ChangePasswordProps<TChildProps = {}> = Partial<ReactApollo.MutateProps<ChangePasswordMutation, ChangePasswordMutationVariables>> & TChildProps;
export function withChangePassword<TProps, TChildProps = {}>(operationOptions?: ReactApollo.OperationOption<
  TProps,
  ChangePasswordMutation,
  ChangePasswordMutationVariables,
  ChangePasswordProps<TChildProps>>) {
    return ReactApollo.withMutation<TProps, ChangePasswordMutation, ChangePasswordMutationVariables, ChangePasswordProps<TChildProps>>(ChangePasswordDocument, {
      alias: 'withChangePassword',
      ...operationOptions
    });
};

    export function useChangePasswordMutation(baseOptions?: ReactApolloHooks.MutationHookOptions<ChangePasswordMutation, ChangePasswordMutationVariables>) {
      return ReactApolloHooks.useMutation<ChangePasswordMutation, ChangePasswordMutationVariables>(ChangePasswordDocument, baseOptions);
    };
export type ChangePasswordMutationHookResult = ReturnType<typeof useChangePasswordMutation>;
export const DeleteUserDocument = gql`
    mutation DeleteUser($id: Int!) {
  deleteUser(id: $id)
}
    `;
export type DeleteUserMutationFn = ReactApollo.MutationFn<DeleteUserMutation, DeleteUserMutationVariables>;
export type DeleteUserComponentProps = Omit<ReactApollo.MutationProps<DeleteUserMutation, DeleteUserMutationVariables>, 'mutation'>;

    export const DeleteUserComponent = (props: DeleteUserComponentProps) => (
      <ReactApollo.Mutation<DeleteUserMutation, DeleteUserMutationVariables> mutation={DeleteUserDocument} {...props} />
    );
    
export type DeleteUserProps<TChildProps = {}> = Partial<ReactApollo.MutateProps<DeleteUserMutation, DeleteUserMutationVariables>> & TChildProps;
export function withDeleteUser<TProps, TChildProps = {}>(operationOptions?: ReactApollo.OperationOption<
  TProps,
  DeleteUserMutation,
  DeleteUserMutationVariables,
  DeleteUserProps<TChildProps>>) {
    return ReactApollo.withMutation<TProps, DeleteUserMutation, DeleteUserMutationVariables, DeleteUserProps<TChildProps>>(DeleteUserDocument, {
      alias: 'withDeleteUser',
      ...operationOptions
    });
};

    export function useDeleteUserMutation(baseOptions?: ReactApolloHooks.MutationHookOptions<DeleteUserMutation, DeleteUserMutationVariables>) {
      return ReactApolloHooks.useMutation<DeleteUserMutation, DeleteUserMutationVariables>(DeleteUserDocument, baseOptions);
    };
export type DeleteUserMutationHookResult = ReturnType<typeof useDeleteUserMutation>;
export const CreateUserDocument = gql`
    mutation CreateUser($user: UserInput!) {
  createUser(user: $user) {
    id
    username
    role
    operatorId
  }
}
    `;
export type CreateUserMutationFn = ReactApollo.MutationFn<CreateUserMutation, CreateUserMutationVariables>;
export type CreateUserComponentProps = Omit<ReactApollo.MutationProps<CreateUserMutation, CreateUserMutationVariables>, 'mutation'>;

    export const CreateUserComponent = (props: CreateUserComponentProps) => (
      <ReactApollo.Mutation<CreateUserMutation, CreateUserMutationVariables> mutation={CreateUserDocument} {...props} />
    );
    
export type CreateUserProps<TChildProps = {}> = Partial<ReactApollo.MutateProps<CreateUserMutation, CreateUserMutationVariables>> & TChildProps;
export function withCreateUser<TProps, TChildProps = {}>(operationOptions?: ReactApollo.OperationOption<
  TProps,
  CreateUserMutation,
  CreateUserMutationVariables,
  CreateUserProps<TChildProps>>) {
    return ReactApollo.withMutation<TProps, CreateUserMutation, CreateUserMutationVariables, CreateUserProps<TChildProps>>(CreateUserDocument, {
      alias: 'withCreateUser',
      ...operationOptions
    });
};

    export function useCreateUserMutation(baseOptions?: ReactApolloHooks.MutationHookOptions<CreateUserMutation, CreateUserMutationVariables>) {
      return ReactApolloHooks.useMutation<CreateUserMutation, CreateUserMutationVariables>(CreateUserDocument, baseOptions);
    };
export type CreateUserMutationHookResult = ReturnType<typeof useCreateUserMutation>;
export const UpdateTokenDocument = gql`
    mutation UpdateToken($id: Int!) {
  updateToken(id: $id)
}
    `;
export type UpdateTokenMutationFn = ReactApollo.MutationFn<UpdateTokenMutation, UpdateTokenMutationVariables>;
export type UpdateTokenComponentProps = Omit<ReactApollo.MutationProps<UpdateTokenMutation, UpdateTokenMutationVariables>, 'mutation'>;

    export const UpdateTokenComponent = (props: UpdateTokenComponentProps) => (
      <ReactApollo.Mutation<UpdateTokenMutation, UpdateTokenMutationVariables> mutation={UpdateTokenDocument} {...props} />
    );
    
export type UpdateTokenProps<TChildProps = {}> = Partial<ReactApollo.MutateProps<UpdateTokenMutation, UpdateTokenMutationVariables>> & TChildProps;
export function withUpdateToken<TProps, TChildProps = {}>(operationOptions?: ReactApollo.OperationOption<
  TProps,
  UpdateTokenMutation,
  UpdateTokenMutationVariables,
  UpdateTokenProps<TChildProps>>) {
    return ReactApollo.withMutation<TProps, UpdateTokenMutation, UpdateTokenMutationVariables, UpdateTokenProps<TChildProps>>(UpdateTokenDocument, {
      alias: 'withUpdateToken',
      ...operationOptions
    });
};

    export function useUpdateTokenMutation(baseOptions?: ReactApolloHooks.MutationHookOptions<UpdateTokenMutation, UpdateTokenMutationVariables>) {
      return ReactApolloHooks.useMutation<UpdateTokenMutation, UpdateTokenMutationVariables>(UpdateTokenDocument, baseOptions);
    };
export type UpdateTokenMutationHookResult = ReturnType<typeof useUpdateTokenMutation>;