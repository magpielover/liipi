import React from "react";
import { ApolloProvider } from "react-apollo";
import { ApolloProvider as ApolloHooksProvider } from "react-apollo-hooks";
import ReactDOM from "react-dom";
import { BrowserRouter } from "react-router-dom";
import createClient from "./apollo/createClient";
import App from "./App";
import AuthContextProvider from "./components/Commons/AuthContext";
import { LoginModalProvider } from "./components/Commons/LoginModal";
import "./i18n";
import "./index.css";
import "./resources/assets/css/liipi-web-0.0.2-SNAPSHOT.css";
import "./resources/assets/css/select.min.css";
let uri = null;

if (
  process.env.REACT_APP_ENV === "local" ||
  process.env.REACT_APP_ENV === "debug"
) {
  console.log("Starting the app locally...");
  uri = process.env.REACT_APP_LOCAL_API_URL;
} else {
  console.log("Starting the app inside a docker container...");
  uri = process.env.REACT_APP_DOCKER_API_URL || "/api";
}

if (uri) {
  const client = createClient(uri);

  ReactDOM.render(
    <BrowserRouter>
      <ApolloProvider client={client}>
        <ApolloHooksProvider client={client}>
          <AuthContextProvider>
            <LoginModalProvider>
              <App />
            </LoginModalProvider>
          </AuthContextProvider>
        </ApolloHooksProvider>
      </ApolloProvider>
    </BrowserRouter>,

    document.getElementById("root")
  );
}
