export const getBounds: any = (bbox: number[] | undefined) =>
  Array.isArray(bbox) && bbox.length === 4
    ? [[bbox[1], bbox[0]], [bbox[3], bbox[2]]]
    : undefined;

export const getMaxBbox: any = (bboxes: number[][]) => {
  if (!Array.isArray(bboxes) || !bboxes.length) {
    return undefined;
  }

  const eastPoints: any = [];
  const northPoints: any = [];
  const southPoints: any = [];
  const westPoints: any = [];

  bboxes.forEach(bbox => {
    eastPoints.push(bbox[2]);
    northPoints.push(bbox[3]);
    southPoints.push(bbox[1]);
    westPoints.push(bbox[0]);
  });

  return [
    Math.min(...westPoints),
    Math.min(...southPoints),
    Math.max(...eastPoints),
    Math.max(...northPoints)
  ];
};

export const getCenter: any = (bbox: number[] | undefined) =>
  Array.isArray(bbox) && bbox.length === 4
    ? [bbox[3] - (bbox[3] - bbox[1]) / 2, bbox[2] - (bbox[2] - bbox[0]) / 2]
    : undefined;
