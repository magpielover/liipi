import { translateEnum, translateList, translateValue } from "./translateUtils";

const t = (text: string): string => text;

describe("Translation utils", () => {
  it("Translate value", () => {
    expect(translateValue("somePrefix", "SOME_VALUE", t)).toBe(
      "somePrefixSomeValue"
    );
  });

  it("Translates lists", () => {
    expect(
      translateList(
        "somePrefix",
        ["SOME_VALUE", "SOME_OTHER_VALUE", "YET_ANOTHER_VALUE"],
        t
      )
    ).toBe(
      "somePrefixSomeValue, somePrefixSomeOtherValue, somePrefixYetAnotherValue"
    );
  });

  it("Translates enumerables", () => {
    expect(
      translateEnum(
        "somePrefix",
        {
          SOME_VALUE: 1,
          SOME_OTHER_VALUE: 2,
          SOME_EMPTY_VALUE: null,
          YET_ANOTHER_VALUE: 3
        },
        t
      )
    ).toBe(
      "somePrefixSomeValue, somePrefixSomeOtherValue, somePrefixYetAnotherValue"
    );
  });
});

export {};
