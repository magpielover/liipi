import { useContext } from "react";
import { authContext } from "../components/Commons/AuthContext";

export interface LoginState {
  operatorId?: string | undefined | null;
  passwordExpireInDays: number;
  permissions?: any;
  role: string;
  token: string;
  userId: number;
  username: string;
}

interface TokenParts {
  type: string;
  user: number;
  loggedAt: number;
  hmac: string;
}

const useAuth = (): {
  deleteLoginState: () => void;
  isTokenExpired: () => boolean;
  isUserLoggedIn: () => boolean;
  loginState: LoginState | null;
  setLoginState: (loginState: LoginState) => void;
} => {
  const { deleteLoginState, loginState, setLoginState } = useContext(
    authContext
  );

  const isUserLoggedIn = (): boolean => {
    return !!loginState;
  };

  const isTokenExpired = (): boolean => {
    const tokenParts = loginState ? parseToken(loginState.token) : null;
    const loggedAt = tokenParts ? tokenParts.loggedAt : 0;
    // Add 1 hour to logged at timestamp to get expiration time
    const expiresAt = loggedAt + 1000 * 60 * 60;
    return new Date(expiresAt) < new Date();
  };

  const parseToken = (token: string | null): TokenParts | null => {
    const tokenParts = token ? token.split("|") : [];

    return tokenParts.length === 4
      ? {
          type: tokenParts[0],
          user: Number(tokenParts[1]),
          loggedAt: Number(tokenParts[2]),
          hmac: tokenParts[3]
        }
      : null;
  };

  return {
    deleteLoginState,
    isTokenExpired,
    isUserLoggedIn,
    loginState,
    setLoginState
  };
};

export const getStoredLoginState = (): LoginState | null => {
  const loginState = localStorage.getItem("login");
  if (loginState) {
    return JSON.parse(loginState);
  }
  return null;
};

export default useAuth;
