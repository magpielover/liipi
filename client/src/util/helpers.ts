enum Language {
  "fi" = "fi",
  "en" = "en",
  "sv" = "sv"
}
export const getLocalizedName = (obj: any, lang: string) => {
  if (obj && obj.hasOwnProperty(lang)) {
    return obj[lang as Language];
  }
  return "";
};

export const validatePassword = (str: string) => {
  const con = /^.*(?=.{8,})(?=.*[a-zA-Z])(?=.*\d).*$/;
  return con.test(str);
};
